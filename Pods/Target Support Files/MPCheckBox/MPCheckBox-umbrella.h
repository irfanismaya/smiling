#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "MPCheckBox.h"
#import "MPCheckBoxDelegate.h"
#import "MPCheckBoxGroup.h"
#import "MPCheckBoxGroupDelegate.h"
#import "MPTapGestureRecognizerDelegate.h"
#import "MPTapGetureRecognizer.h"

FOUNDATION_EXPORT double MPCheckBoxVersionNumber;
FOUNDATION_EXPORT const unsigned char MPCheckBoxVersionString[];


//  AppDelegate.m
//  Smiling
//  Created by Irfan-Ismaya on 08/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "AppDelegate.h"
#import "AppDependencies.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
@import GoogleMaps;
@import GooglePlaces;

@interface AppDelegate ()

@property (nonatomic, strong) AppDependencies *dependencies;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [GMSServices provideAPIKey:@"AIzaSyC6XDa0nNxAvoq2yF_1U1gXXeYFq_-Cqkw"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyC6XDa0nNxAvoq2yF_1U1gXXeYFq_-Cqkw"];
    [GIDSignIn sharedInstance].delegate = self;
    
    AppDependencies *dependencies = [[AppDependencies alloc] init];
    self.dependencies = dependencies;
    [self.dependencies installRootViewControllerIntoWindow:self.window];

    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *, id> *)options {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSString *givenName = user.profile.givenName;
    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

@end

//
//  AppDepedencies.m
//  Smiling
//
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "AppDependencies.h"
#import "RootWireframe.h"

#import "LoginWireframe.h"
#import "LoginPresenter.h"
#import "LoginManagerAPI.h"
#import "LoginInteractor.h"

#import "TabBarWireframe.h"
#import "TabBarPresenter.h"

@interface AppDependencies ()

@property (nonatomic, strong) LoginWireframe *loginWireframe;

@end

@implementation AppDependencies

- (id)init
{
    if ((self = [super init]))
    {
        [self configureDependencies];
    }
    
    return self;
}

- (void)installRootViewControllerIntoWindow:(UIWindow *)window
{
    //menampilkan halaman login
    [self.loginWireframe presentLoginInterfaceFromWindow:window];
}

- (void)configureDependencies
{
    self.loginWireframe = [[LoginWireframe alloc] init];
}

@end

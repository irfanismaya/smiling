//
//  AppDepedencies.h
//  Smiling
//
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

@interface AppDependencies : NSObject

- (void)installRootViewControllerIntoWindow:(UIWindow *)window;

@end

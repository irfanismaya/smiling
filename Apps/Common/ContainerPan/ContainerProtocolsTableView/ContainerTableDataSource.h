//  ContainerTableDataSource.h
//  Smiling
//  Created by Irfan-Ismaya on 21/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <Foundation/Foundation.h>
#import "ContainerTypes.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContainerTableDataSource : NSObject <UITableViewDataSource>
@property (strong, nonatomic) NSMutableArray *photos;
@property ContainerStyle containerStyle;
@end

NS_ASSUME_NONNULL_END

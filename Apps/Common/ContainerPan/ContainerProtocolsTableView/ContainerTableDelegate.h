//  ContainerTableDelegate.h
//  Smiling
//  Created by Irfan-Ismaya on 21/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ContainerScrollDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContainerTableDelegate : ContainerScrollDelegate <UITableViewDelegate>
@property (strong, nonatomic) void(^blockSelectIndex)(NSInteger);
@end

NS_ASSUME_NONNULL_END

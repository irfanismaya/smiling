//  ContainerTableDelegate.m
//  Smiling
//  Created by Irfan-Ismaya on 21/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ContainerTableDelegate.h"

@implementation ContainerTableDelegate
#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(self.blockSelectIndex) self.blockSelectIndex(indexPath.row);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88;
}
@end

//  ContainerHeaderView.h
//  Smiling
//  Created by Irfan-Ismaya on 21/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ContainerTypes.h"

@interface HeaderLabel : UIView
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIView *separatorLine;
@property (nonatomic, strong) UIView *grip;
@end


@interface HeaderSearch : UIView
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UIView *separatorLine;
@end


@interface HeaderGrib : UIView
@property (nonatomic, strong) UIView *separatorLine;
@property (nonatomic, strong) UIImageView *separatorShadow;
@property (nonatomic, strong) UIView *grip;
@end

NS_ASSUME_NONNULL_BEGIN

@interface ContainerHeaderView : NSObject

+ (HeaderLabel  *)createHeaderLabel;
+ (HeaderSearch *)createHeaderSearch;
+ (HeaderGrib   *)createHeaderGrip;

+ (void)changeColorsHeaderView:(UIView *)headerView forStyle:(ContainerStyle)style;

@end

NS_ASSUME_NONNULL_END

//  ContainerScrollView.m
//  Smiling
//  Created by Irfan-Ismaya on 21/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ContainerScrollView.h"
#import "ContainerDefines.h"

@implementation ContainerScrollView
+ (UITableView *)createTableViewWithProtocols:(id)protocols {
    UITableView *
    table = [[UITableView alloc] initWithFrame:FRAME_SCROLLVIEW style:UITableViewStylePlain];
    table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    table.showsVerticalScrollIndicator = NO;
    table.backgroundColor = CLR_COLOR;
    table.delegate   = protocols;
    table.dataSource = protocols;
    return table;
}
@end

//  ContainerScrollView.h
//  Smiling
//  Created by Irfan-Ismaya on 21/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ContainerTypes.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContainerScrollView : NSObject
+ (UITableView *)createTableViewWithProtocols:(id)protocols;
@end

NS_ASSUME_NONNULL_END

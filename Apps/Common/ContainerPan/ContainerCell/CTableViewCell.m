//  CTableViewCell.m
//  Smiling
//  Created by Irfan-Ismaya on 21/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "CTableViewCell.h"

@implementation CTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end

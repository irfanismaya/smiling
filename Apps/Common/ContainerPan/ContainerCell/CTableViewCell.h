//  CTableViewCell.h
//  Smiling
//  Created by Irfan-Ismaya on 21/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CTableViewCell : UITableViewCell
@property (strong, nonatomic) UILabel *labelTitle;
@property (strong, nonatomic) UILabel *labelSubTitle;
@property (strong, nonatomic) UIView *separatorLine;
@property (strong, nonatomic) UIImageView *imageAvatar;

@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@end

NS_ASSUME_NONNULL_END

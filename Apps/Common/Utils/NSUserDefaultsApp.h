//
//  NSUserDefaultsApp.h
//  Atalian
//
//  Created by Irfan-Ismaya on 05/02/19.
//  Copyright © 2019 Swamedia. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSUserDefaultsApp : NSObject
+ (void)setCheck:(NSString *)check;
+ (NSString*)getCheck;

+ (void)setLogin:(NSString *)login;
+ (NSString*)getLogin;

+ (void)setIntro:(NSString *)intro;
+ (NSString*)getIntro;

+ (void)setToken:(NSString *)token;
+ (NSString*)getToken;

+ (void)setEmail:(NSString *)email;
+ (NSString*)getEmail;

+ (void)setUsername:(NSString *)username;
+ (NSString*)getUsername;

+ (void)setPassword:(NSString *)password;
+ (NSString*)getPassword;

+ (void)setRoleId:(NSString * )roleid;
+ (NSString *)getRoleId;

+ (void)setUserId:(NSString *)userid;
+ (NSString *)getUserId;

+ (void)setCompanyProfileId:(NSString *)companyprofileid;
+ (NSString *)getCompanyProfileId;
    
@end

NS_ASSUME_NONNULL_END

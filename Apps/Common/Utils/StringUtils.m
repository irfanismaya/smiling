//
//  StringUtils.m
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "StringUtils.h"

@implementation StringUtils

+ (NSString*) replaceSpaceFromUrl:(NSString *)originalUrl {
    return [originalUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
}

@end

//  Constants.h
//  Atalian
//  Created by Irfan-Ismaya on 05/02/19.
//  Copyright © 2019 Swamedia. All rights reserved.

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Constants : NSObject
extern NSString * const baseURL;
extern NSString * const login;
extern NSString * const logout;
extern NSString * const forgotpassword;
extern NSString * const inbox;
extern NSString * const inboxcount;
extern NSString * const inboxdetail;
extern NSString * const inboxfollowopen;
extern NSString * const profile;
extern NSString * const editprofile;
extern NSString * const dataprofile;
extern NSString * const changepassword;
extern NSString * const listcondition;
extern NSString * const listpriority;
extern NSString * const listconditioncb;
extern NSString * const listproject;
extern NSString * const listfacilities;
extern NSString * const listteknisi;
extern NSString * const listasset;
extern NSString * const storesupervisor;
extern NSString * const detailmonitoring;
extern NSString * const listmonitoring;
extern NSString * const followuprogress;
extern NSString * const codetransaction;
extern const int reqTimeout;
extern NSString * const primary_color_purple;
@end

NS_ASSUME_NONNULL_END

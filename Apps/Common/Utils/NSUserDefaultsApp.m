//  NSUserDefaultsApp.m
//  Atalian
//  Created by Irfan-Ismaya on 05/02/19.
//  Copyright © 2019 Swamedia. All rights reserved.

#import "NSUserDefaultsApp.h"
#import "LoginController.h"

@implementation NSUserDefaultsApp
+ (void) setCheck: (NSString *)check {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:check forKey:@"check"];
    [defaults synchronize];
}
+ (NSString*) getCheck {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *check = [defaults stringForKey:@"check"];
    return check;
}

+ (void) setLogin: (NSString *)login {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:login forKey:@"login"];
    [defaults synchronize];
}
+ (NSString*) getLogin {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *login = [defaults stringForKey:@"login"];
    return login;
}

+ (void) setIntro: (NSString *)intro {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:intro forKey:@"intro"];
    [defaults synchronize];
}
+ (NSString*) getIntro {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *intro = [defaults stringForKey:@"intro"];
    return intro;
}

+ (void) setToken: (NSString *)token {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:token forKey:@"token"];
    [defaults synchronize];
}
+ (NSString*) getToken {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults stringForKey:@"token"];
    return token;
}

+ (void) setEmail: (NSString *)email {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:email forKey:@"email"];
    [defaults synchronize];
}
+ (NSString*) getEmail {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *email = [defaults stringForKey:@"email"];
    return email;
}

+ (void) setUsername: (NSString *)username {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:username forKey:@"username"];
    [defaults synchronize];
}
+ (NSString*) getUsername {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    return username;
}

+ (void) setPassword: (NSString *)password {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:password forKey:@"password"];
    [defaults synchronize];
}
+ (NSString*) getPassword {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *password = [defaults stringForKey:@"password"];
    return password;
}

+(void)setRoleId:(NSString *)roleid{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:roleid forKey:@"roleid"];
    [defaults synchronize];
}
+(NSString * )getRoleId{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *roleid = [defaults stringForKey:@"roleid"];
    return roleid;
}

+(void)setUserId:(NSString *)userid{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:userid forKey:@"userid"];
    [defaults synchronize];
}
+(NSString * )getUserId{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userid = [defaults stringForKey:@"userid"];
    return userid;
}

+(void)setCompanyProfileId:(NSString *)companyprofileid{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:companyprofileid forKey:@"companyprofileid"];
    [defaults synchronize];
}
+(NSString * )getCompanyProfileId{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *companyprofileid = [defaults stringForKey:@"companyprofileid"];
    return companyprofileid;
}
    
@end

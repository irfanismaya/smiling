//
//  UIUtils.m
//  Smiling
//
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "UIUtils.h"
#import "ISMessages.h"
#import "UIColor+HexString.h"

@implementation UIUtils

+ (void) showSnackbar:(NSString *)title message:(NSString *)message alertType:(NSUInteger) alertType {
    [ISMessages showCardAlertWithTitle:title
                               message:message
                              duration:3.f
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:alertType
                         alertPosition:ISAlertPositionTop
                               didHide:nil];
}

+ (UIView*) initLoadingView:(UIView *)view {
    UIView *viewLoading = [[UIView alloc] initWithFrame:CGRectMake(view.frame.size.width, view.frame.size.height, 64, 64)];
    viewLoading.center = CGPointMake(view.frame.size.width  / 2,
                                      view.frame.size.height / 2);
    
    [view addSubview:viewLoading];
    return viewLoading;
}

+ (MDCActivityIndicator*) initLoadingIndicatorView:(UIView *)viewLoading {
    MDCActivityIndicator *activityIndicator = [[MDCActivityIndicator alloc] init];
    [activityIndicator sizeToFit];
    activityIndicator.center = CGPointMake(viewLoading.frame.size.width  / 2,
                                            viewLoading.frame.size.height / 2);
    [activityIndicator setCycleColors:[[NSArray alloc] initWithObjects:[UIColor colorWithHexString:primary_color_purple],nil]];
    
    return activityIndicator;
}

@end

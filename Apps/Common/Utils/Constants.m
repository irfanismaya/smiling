//  Constants.m
//  Atalian
//  Created by Irfan-Ismaya on 05/02/19.
//  Copyright © 2019 Swamedia. All rights reserved.

#import "Constants.h"

@implementation Constants
NSString * const baseURL             = @"https://api.morfem.id/mms/v1";
//NSString * const baseURL             = @"http://mms.swamedia.id/api/v1";
//NSString * const baseURL             = @"http://172.17.0.125:8123/v1";
NSString * const login               = @"/login";
NSString * const logout              = @"/user/logout";
NSString * const forgotpassword      = @"/mail/reset";
NSString * const inbox               = @"/workorder/inbox";
NSString * const inboxcount          = @"/inbox/count";
NSString * const inboxdetail         = @"/workorder/inboxdetail";
NSString * const inboxfollowopen     = @"/workorder/followup";
NSString * const profile             = @"/user/profile";
NSString * const editprofile         = @"/user/update";
NSString * const dataprofile         = @"/user/profile";
NSString * const changepassword      = @"/user/changepassword";
NSString * const listcondition       = @"/workorder/getconditionsfinal";
NSString * const listpriority        = @"/workorder/getpriority";
NSString * const listconditioncb     = @"/workorder/getconditions";
NSString * const listproject         = @"/workorder/getprojects";
NSString * const listfacilities      = @"/workorder/getfacilities";
NSString * const listteknisi         = @"/workorder/getteknisibyproject";
NSString * const listasset           = @"/workorder/getassets";
NSString * const storesupervisor     = @"/workorder/store";
NSString * const detailmonitoring    = @"/workorder/detailticket";
NSString * const listmonitoring      = @"/workorder/monitoring";
NSString * const followuprogress     = @"/workorder/followwithevidence";
NSString * const codetransaction     = @"/workorder/gettransactioncode";
const int reqTimeout = 30;
NSString * const primary_color_purple     =  @"#9f43ab";
@end

//
//  UIUtils.h
//  Smiling
//
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "MaterialActivityIndicator.h"

@interface UIUtils : NSObject

+ (void) showSnackbar:(NSString *)title message:(NSString *)message alertType:(NSUInteger) alertType;
+ (UIView*) initLoadingView:(UIView *)view;
+ (MDCActivityIndicator*) initLoadingIndicatorView:(UIView *)viewLoading;

@end

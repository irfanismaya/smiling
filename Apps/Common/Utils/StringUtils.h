//
//  StringUtils.h
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

@interface StringUtils : NSObject

+ (NSString*) replaceSpaceFromUrl:(NSString *)originalUrl;

@end

//
//  BaseResponse.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

@interface BaseResponse : NSObject
@property (nonatomic) NSInteger page;
@property (nonatomic) NSInteger perPage;
@property (nonatomic) NSInteger total;
@property (nonatomic) NSInteger totalPages;
@property (nonatomic) NSArray *data;
@property (nonatomic) BOOL success;
@end

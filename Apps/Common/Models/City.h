//  City.h
//  Smiling
//  Created by Irfan-Ismaya on 03/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

@interface City : NSObject
@property (nonatomic) NSInteger id;
@property (nonatomic) NSInteger province_id;
@property (nonatomic) NSString *name;
@property (nonatomic) NSInteger is_active;
@property (nonatomic) NSInteger is_delete;
@property (nonatomic) NSString *created_at;
@property (nonatomic) NSString *updated_at;
@property (nonatomic) NSInteger created_by;
@property (nonatomic) NSInteger updated_by;
@property (nonatomic) NSString *desc;
@end

@interface BaseResponseCity : NSObject
@property (nonatomic) NSArray* data;
@property (nonatomic) BOOL success;
@end

//  Article.h
//  Smiling
//  Created by Irfan-Ismaya on 03/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.


@interface Article : NSObject
@property (nonatomic) NSInteger id;
@property (nonatomic) NSString *coverImage;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *desc;
@property (nonatomic) NSString *createdAt;
@end

@interface BaseResponseArticle : NSObject
@property (nonatomic) NSArray* data;
@property (nonatomic) BOOL success;
@end

@interface BaseResponseArticleDetail : NSObject
@property (nonatomic) Article* data;
@property (nonatomic) BOOL success;
@end

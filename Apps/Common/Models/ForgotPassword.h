//  ForgotPassword.h
//  Smiling
//  Created by Irfan-Ismaya on 22/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <Foundation/Foundation.h>

@interface ForgotPassword : NSObject
@property (nonatomic) NSString *token;
@end


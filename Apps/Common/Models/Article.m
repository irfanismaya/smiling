//  Article.m
//  Smiling
//  Created by Irfan-Ismaya on 03/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "Article.h"

@interface Article()
@end

@implementation Article
@end

@interface BaseResponseArticle ()
@end

@implementation BaseResponseArticle
@end

@interface BaseResponseArticleDetail ()
@end

@implementation BaseResponseArticleDetail
@end

//
//  BaseViewController.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "BaseViewProtocol.h"

@interface BaseViewController : UIViewController

-(void) showLoading;
-(void) hideLoading;

@property (nonatomic, strong) id <BaseViewProtocol> childViewController;

@end

//
//  RootWireframe.m
//  Smiling
//
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "RootWireframe.h"

@implementation RootWireframe

- (void)showRootViewController:(UIViewController *)viewController
                      inWindow:(UIWindow *)window
{
    UINavigationController *navigationController = [self navigationControllerFromWindow:window];
    navigationController.viewControllers = @[viewController];
}

- (UINavigationController *)navigationControllerFromWindow:(UIWindow *)window
{
    UINavigationController *navigationController = (UINavigationController *)[window rootViewController];
    return navigationController;
}

//Get from storyboard
- (id)viewControllerFromStoryboard:(NSString *)identifier
{
    UIStoryboard *storyboard = [self mainStoryboard];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:identifier];
    return viewController;
}

- (id)tabBarControllerFromStoryboard:(NSString *)identifier
{
    UIStoryboard *storyboard = [self mainStoryboard];
    UITabBarController *tabController = [storyboard instantiateViewControllerWithIdentifier:identifier];
    return tabController;
}

- (UIStoryboard *)mainStoryboard
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:[NSBundle mainBundle]];
    return storyboard;
}

@end

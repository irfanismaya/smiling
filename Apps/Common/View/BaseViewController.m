//
//  BaseViewController.m
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

//Loading View
@property (nonatomic, strong) UIView *viewLoading;
@property (nonatomic, strong) MDCActivityIndicator *activityIndicator;

//No Connection View
@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;
@property (nonatomic, strong) UIImageView *imageViewNoConnection;
@property (nonatomic, strong) UILabel *internetstatus;

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _viewLoading = [UIUtils initLoadingView:self.view];
    _activityIndicator = [UIUtils initLoadingIndicatorView:_viewLoading];
    
    [self callCheckInternet];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

-(void)callCheckInternet {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    NSString *remoteHostName = @"https://www.google.com/";
    NSString *remoteHostLabelFormatString = NSLocalizedString(@"Remote Host: %@", @"Remote host label format string");
    
    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
    [self.hostReachability startNotifier];
    [self updateInterfaceWithReachability:self.hostReachability];
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [self updateInterfaceWithReachability:self.internetReachability];
}

-(void)checkInternetConnection:(BOOL)connectionstatus {
    if(connectionstatus){
        _imageViewNoConnection = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_no_internet_satellite"]];
        _imageViewNoConnection.frame = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2, 200, 200);
        _imageViewNoConnection.center = CGPointMake(self.view.frame.size.width/2,
                                        self.view.frame.size.height/2);
        [_imageViewNoConnection setContentMode:UIViewContentModeScaleAspectFill];
        
        CGRect frame = CGRectMake(0, 0, 245, 20);
        _internetstatus = [[UILabel alloc] initWithFrame:frame];
        _internetstatus.center = CGPointMake(self.view.frame.size.width/2, self.view.bounds.size.height/1.5f);
        _internetstatus.text = @"Oops! No Internet Connection";
        _internetstatus.textAlignment = NSTextAlignmentJustified;
        _internetstatus.textColor = [UIColor grayColor];
        [_internetstatus setFont:[UIFont boldSystemFontOfSize:17]];
        
        [self.view addSubview:_imageViewNoConnection];
        [self.view addSubview:_internetstatus];
    }else{
        [_imageViewNoConnection removeFromSuperview];
        [_internetstatus removeFromSuperview];
    }
}

- (void)showLoading {
    [_viewLoading addSubview:_activityIndicator];
    _viewLoading.hidden = NO;
    [_activityIndicator startAnimating];
}

- (void)hideLoading {
    _viewLoading.hidden = YES;
    [_activityIndicator stopAnimating];
}

- (void) reachabilityChanged:(NSNotification *)note{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}

- (void)updateInterfaceWithReachability:(Reachability *)reachability{
    if (reachability == self.hostReachability){
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        BOOL connectionRequired = [reachability connectionRequired];
        if (connectionRequired){
            
        }else{
            
        }
    }
    
    if (reachability == self.internetReachability){
        [self configureTextField:reachability];
    }
}

- (void)configureTextField:(Reachability *)reachability{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    BOOL connectionRequired = [reachability connectionRequired];
    NSString* statusString = @"";
    
    switch (netStatus){
        case NotReachable:{
            [self.childViewController connectionHideView];
            [self checkInternetConnection:YES];
            connectionRequired = NO;
            break;
        }case ReachableViaWWAN:{
            [self.childViewController connectionShowView];
            [self checkInternetConnection:NO];
            break;
        }case ReachableViaWiFi:{
            [self.childViewController connectionShowView];
            [self checkInternetConnection:NO];
            break;
        }
    }
    
    if (connectionRequired){
        NSString *connectionRequiredFormatString = NSLocalizedString(@"%@, Connection Required", @"Concatenation of status string with connection requirement");
        statusString= [NSString stringWithFormat:connectionRequiredFormatString, statusString];
    }
}

@end

//
//  BaseViewProtocol.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

@protocol BaseViewProtocol <NSObject>
- (void)connectionShowView;
- (void)connectionHideView;
@end

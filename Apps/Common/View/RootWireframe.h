//
//  RootWireframe.h
//  Smiling
//
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

@class LoginController;

@interface RootWireframe : NSObject

- (void)showRootViewController:(UIViewController *)viewController
                      inWindow:(UIWindow *)window;

- (id)viewControllerFromStoryboard:(NSString *)identifier;
- (id)tabBarControllerFromStoryboard:(NSString *)identifier;

@end

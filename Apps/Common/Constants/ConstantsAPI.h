//  ConstantsAPI.h
//  Smiling
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

//#define BASE_URL @"https://reqres.in/api/"
#define BASE_URL @"http://47.74.130.246/swj_private/api/v01/"

//GET
#define USER @"users"

//POST
//#define LOGIN @"login"
//#define SIGNUP @"signup"
#define FORGOTPASSWORD @"forgotpassword"

#define LOGIN @"member/auth/login"
#define CITY @"master/citiess"
#define ARTICLE @"website/articles"
#define SIGNUP @"member/auth/signup"


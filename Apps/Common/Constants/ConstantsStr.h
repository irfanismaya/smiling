//  ConstantsStr.h
//  Smiling
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

//LoginController
#define LOGIN_TITLE @"Login"
#define USERNAME_EMPTY @"Silahkan masukan nama pengguna"
#define PASSWORD_EMPTY @"Silahkan masukan kata sandi"

#define REGISTER_TITLE @"Register"
#define FIRST_NAME_EMPTY @"Silahkan masukan nama depan"
#define LAST_NAME_EMPTY @"Silahkan masukan nama belakang"
#define GENDER_EMPTY @"Silahkan pilih jenis kelamin"
#define USERNAME_EMPTY @"Silahkan masukan nama pengguna"
#define HANDPONE_EMPTY @"Silahkan masukan nomor handphone"
#define EMAIL_EMPTY @"Silahkan masukan surat elektronik"
#define PASSWORD_EMPTY @"Silahkan masukan kata sandi"
#define PASSWORD_CONFIRM_EMPTY @"Silahkan masukan kata sandi ulang"

#define TAB_HOME @"Home"
#define TAB_NEARBY @"Nearby"
#define TAB_REFERRAL @"Referral"
#define TAB_ACCOUNT @"Account"

#define TAB_HOME_ICON @"baseline_location_city_black_24pt"
#define TAB_NEARBY_ICON @"baseline_place_black_24pt"
#define TAB_REFERRAL_ICON @"baseline_forum_black_24pt"
#define TAB_ACCOUNT_ICON @"baseline_person_outline_black_24pt"

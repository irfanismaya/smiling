//
//  NetworkManager+Promotion.m
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "NetworkManager+Promotion.h"
#import "BaseResponse.h"
#import "Promotion.h"
#import <DCKeyValueObjectMapping/DCKeyValueObjectMapping.h>
#import "DCArrayMapping.h"
#import "DCParserConfiguration.h"

@implementation NetworkManager (User)

+ (NSURLSessionDataTask *)getPromotionList:(NSInteger)page successBlock:(void (^)(NSArray *listPromotion))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
    return [[self sharedManager] GET:[NSString stringWithFormat:@"%@?page=%ld", USER, (long)page]
                          parameters:@{}
                            progress:nil
                             success: ^(NSURLSessionDataTask *_Nonnull task, id _Nonnull responseObject)
            {
                NSLog(@"JSON: %@", responseObject);
                
                DCArrayMapping *mapper = [DCArrayMapping mapperForClassElements:[Promotion class] forAttribute:@"data" onClass:[BaseResponse class]];
                
                DCParserConfiguration *config = [DCParserConfiguration configuration];
                [config addArrayMapper:mapper];
                
                DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[BaseResponse class]  andConfiguration:config];
                
                BaseResponse *baseResponse = [parser parseDictionary:responseObject];
                if (successBlock) {
                    successBlock(baseResponse.data);
                }
            }
                             failure: ^(NSURLSessionDataTask *_Nonnull task, NSError *_Nonnull error)
            {
                if (failureBlock) {
                    failureBlock(error);
                }
            }];
}

@end

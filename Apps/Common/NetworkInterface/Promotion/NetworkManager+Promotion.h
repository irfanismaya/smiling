//
//  NetworkManager+Promotion.h
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "NetworkManager.h"
#import "BaseResponse.h"
#import "Promotion.h"

@interface NetworkManager (Promotion)

+ (NSURLSessionDataTask *)getPromotionList:(NSInteger)page successBlock:(void (^)(NSArray *listPromotion))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

@end

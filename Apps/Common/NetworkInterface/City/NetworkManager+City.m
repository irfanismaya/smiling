//  NetworkManager+City.m
//  Smiling
//  Created by Irfan-Ismaya on 03/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NetworkManager+City.h"
#import "City.h"
#import <DCKeyValueObjectMapping/DCKeyValueObjectMapping.h>
#import "DCArrayMapping.h"
#import "DCParserConfiguration.h"

@implementation NetworkManager (City)

+ (NSURLSessionDataTask *)getCityList:(void (^)(NSArray *listCity))successBlock failureBlock:(void (^)(NSError *error))failureBlock{
    return [[self sharedManager] GET:CITY
                          parameters:nil
                            progress:nil
                             success: ^(NSURLSessionDataTask *_Nonnull task, id _Nonnull responseObject){
                NSLog(@"JSON: %@", responseObject);
                
                DCArrayMapping *mapper = [DCArrayMapping mapperForClassElements:[City class] forAttribute:@"data" onClass:[BaseResponseCity class]];
                                 
                DCObjectMapping *name = [DCObjectMapping mapKeyPath:@"name" toAttribute:@"name" onClass:[City class]];
                DCObjectMapping *desc = [DCObjectMapping mapKeyPath:@"description" toAttribute:@"desc" onClass:[City class]];
                
                DCParserConfiguration *config = [DCParserConfiguration configuration];
                [config addObjectMapping:name];
                [config addObjectMapping:desc];
                [config addArrayMapper:mapper];
                
                DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[BaseResponseCity class]  andConfiguration:config];
                
                BaseResponseCity *baseResponseCity = [parser parseDictionary:responseObject];
                if (successBlock) {
                    successBlock(baseResponseCity.data);
                }
            }failure: ^(NSURLSessionDataTask *_Nonnull task, NSError *_Nonnull error){
                if (failureBlock) {
                    failureBlock(error);
                }
            }];
}
@end

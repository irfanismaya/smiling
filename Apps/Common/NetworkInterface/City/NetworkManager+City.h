//  NetworkManager+City.h
//  Smiling
//  Created by Irfan-Ismaya on 03/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NetworkManager.h"
#import "City.h"

@interface NetworkManager (City)
+ (NSURLSessionDataTask *)getCityList:(void (^)(NSArray *listCity))successBlock failureBlock:(void (^)(NSError *error))failureBlock;
@end


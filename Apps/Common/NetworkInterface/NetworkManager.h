//  NetworkManager.h
//  Smiling
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <AFNetworking/AFNetworking.h>

@interface NetworkManager : AFHTTPSessionManager
+ (NetworkManager *)sharedManager;
@end

//  NetworkManager+ForgotPassword.m
//  Smiling
//  Created by Irfan-Ismaya on 22/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NetworkManager+ForgotPassword.h"
#import "ForgotPassword.h"
#import <DCKeyValueObjectMapping/DCKeyValueObjectMapping.h>

@implementation NetworkManager (ForgotPassword)
+ (NSURLSessionDataTask *)forgotpassword:(id)param successBlock:(void (^)(ForgotPassword *forgotpassword))successBlock failureBlock:(void (^)(NSError *error))failureBlock{
    return [[self sharedManager] POST:FORGOTPASSWORD
                           parameters:param
                             progress:nil
                              success: ^(NSURLSessionDataTask *_Nonnull task, id _Nonnull responseObject){
                                  NSLog(@"JSON: %@", responseObject);
                                  DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass: [ForgotPassword class]];
                                  ForgotPassword *forgotpassword = [parser parseDictionary:responseObject];
                                  
                                  if(forgotpassword != nil) {
                                      NSLog(@"Token: %@", forgotpassword.token);
                                  } else {
                                      NSLog(@"ForgotPassword nil");
                                  }
                                  
                                  if (successBlock) {
                                      successBlock(forgotpassword);
                                  }
                              }failure: ^(NSURLSessionDataTask *_Nonnull task, NSError *_Nonnull error){
                                  if (failureBlock) {
                                      failureBlock(error);
                                  }
                              }];
}

@end

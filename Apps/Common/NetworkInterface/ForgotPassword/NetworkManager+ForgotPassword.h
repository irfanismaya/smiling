//  NetworkManager+ForgotPassword.h
//  Smiling
//  Created by Irfan-Ismaya on 22/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NetworkManager.h"
#import "ForgotPassword.h"

@interface NetworkManager (ForgotPassword)
+ (NSURLSessionDataTask *)forgotpassword:(id)param successBlock:(void (^)(ForgotPassword *forgotpassword))successBlock failureBlock:(void (^)(NSError *error))failureBlock;
@end


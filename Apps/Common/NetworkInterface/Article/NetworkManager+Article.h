//  NetworkManager+Article.h
//  Smiling
//  Created by Irfan-Ismaya on 03/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NetworkManager.h"
#import "Article.h"

@interface NetworkManager (Article)
+ (NSURLSessionDataTask *)getArticleList:(NSInteger)page successBlock:(void (^)(NSArray *listArticle))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

+ (NSURLSessionDataTask *)getArticleDetail:(NSInteger)articleId successBlock:(void (^)(Article *article))successBlock failureBlock:(void (^)(NSError *error))failureBlock;
@end

//  NetworkManager+Article.m
//  Smiling
//  Created by Irfan-Ismaya on 03/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NetworkManager+Article.h"
#import "Article.h"
#import <DCKeyValueObjectMapping/DCKeyValueObjectMapping.h>
#import "DCArrayMapping.h"
#import "DCParserConfiguration.h"

@implementation NetworkManager (Article)

+ (NSURLSessionDataTask *)getArticleList:(NSInteger)page successBlock:(void (^)(NSArray *listArticle))successBlock failureBlock:(void (^)(NSError *error))failureBlock{
    return [[self sharedManager] GET:ARTICLE
                          parameters:nil
                            progress:nil
                             success: ^(NSURLSessionDataTask *_Nonnull task, id _Nonnull responseObject){
                         NSLog(@"getArticleList: %@", responseObject);
                         
                         DCArrayMapping *mapper = [DCArrayMapping mapperForClassElements:[Article class] forAttribute:@"data" onClass:[BaseResponseArticle class]];
                         DCObjectMapping *coverImage = [DCObjectMapping mapKeyPath:@"cover_image" toAttribute:@"coverImage" onClass:[Article class]];
                         DCObjectMapping *desc = [DCObjectMapping mapKeyPath:@"description" toAttribute:@"desc" onClass:[Article class]];
                         DCObjectMapping *createdAt = [DCObjectMapping mapKeyPath:@"created_at" toAttribute:@"createdAt" onClass:[Article class]];

                         DCParserConfiguration *config = [DCParserConfiguration configuration];
                         [config addObjectMapping:coverImage];
                         [config addObjectMapping:desc];
                         [config addObjectMapping:createdAt];
                         [config addArrayMapper:mapper];
                         
                         DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[BaseResponseArticle class]  andConfiguration:config];
                         
                         BaseResponseArticle *baseResponseArticle = [parser parseDictionary:responseObject];
//                         for (Article *object in baseResponse.data) {
//                             NSLog(@"coverImage: %@", object.coverImage);
//                         }
                         
                         if (successBlock) {
                             successBlock(baseResponseArticle.data);
                         }
                     }failure: ^(NSURLSessionDataTask *_Nonnull task, NSError *_Nonnull error){
                         if (failureBlock) {
                             failureBlock(error);
                         }
                     }];
}

+ (NSURLSessionDataTask *)getArticleDetail:(NSInteger)articleId successBlock:(void (^)(Article *article))successBlock failureBlock:(void (^)(NSError *error))failureBlock{
    return [[self sharedManager] GET:[NSString stringWithFormat:@"%@/%ld", ARTICLE, (long)articleId]
                          parameters:nil
                            progress:nil
                             success: ^(NSURLSessionDataTask *_Nonnull task, id _Nonnull responseObject){
                                 NSLog(@"getArticleDetail: %@", responseObject);
                                 
                                 DCObjectMapping *coverImage = [DCObjectMapping mapKeyPath:@"cover_image" toAttribute:@"coverImage" onClass:[Article class]];
                                 DCObjectMapping *desc = [DCObjectMapping mapKeyPath:@"description" toAttribute:@"desc" onClass:[Article class]];
                                 DCObjectMapping *createdAt = [DCObjectMapping mapKeyPath:@"created_at" toAttribute:@"createdAt" onClass:[Article class]];
                                 
                                 DCParserConfiguration *config = [DCParserConfiguration configuration];
                                 [config addObjectMapping:coverImage];
                                 [config addObjectMapping:desc];
                                 [config addObjectMapping:createdAt];
                                 
                                 DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[BaseResponseArticleDetail class]  andConfiguration:config];
                                 
                                 BaseResponseArticleDetail *baseResponseArticleDetail = [parser parseDictionary:responseObject];
                                 //                         for (Article *object in baseResponse.data) {
                                 //                             NSLog(@"coverImage: %@", object.coverImage);
                                 //                         }
                                 
                                 Article *article = (Article*)baseResponseArticleDetail.data;
                                 if (successBlock) {
                                     successBlock(article);
                                 }
                             }failure: ^(NSURLSessionDataTask *_Nonnull task, NSError *_Nonnull error){
                                 if (failureBlock) {
                                     failureBlock(error);
                                 }
                             }];
}
@end

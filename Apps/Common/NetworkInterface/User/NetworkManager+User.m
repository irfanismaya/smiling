//
//  NetworkManager+User.m
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "NetworkManager+Login.h"
#import "BaseResponse.h"
#import "User.h"
#import <DCKeyValueObjectMapping/DCKeyValueObjectMapping.h>
#import "DCArrayMapping.h"
#import "DCParserConfiguration.h"

@implementation NetworkManager (User)

+ (NSURLSessionDataTask *)getUserList:(NSInteger)page successBlock:(void (^)(NSArray *listUser))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
    return [[self sharedManager] GET:[NSString stringWithFormat:@"%@?page=%ld", USER, (long)page]
                           parameters:@{}
                             progress:nil
                              success: ^(NSURLSessionDataTask *_Nonnull task, id _Nonnull responseObject)
            {
                NSLog(@"JSON: %@", responseObject);
                
                DCArrayMapping *mapper = [DCArrayMapping mapperForClassElements:[User class] forAttribute:@"data" onClass:[BaseResponse class]];
                
                DCParserConfiguration *config = [DCParserConfiguration configuration];
                [config addArrayMapper:mapper];
                
                DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass:[BaseResponse class]  andConfiguration:config];

                BaseResponse *baseResponse = [parser parseDictionary:responseObject];
                if (successBlock) {
                    successBlock(baseResponse.data);
                }
            }
                              failure: ^(NSURLSessionDataTask *_Nonnull task, NSError *_Nonnull error)
            {
                if (failureBlock) {
                    failureBlock(error);
                }
            }];
}

@end

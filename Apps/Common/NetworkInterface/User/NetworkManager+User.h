//
//  NetworkManager+User.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "NetworkManager.h"
#import "BaseResponse.h"
#import "User.h"

@interface NetworkManager (User)

+ (NSURLSessionDataTask *)getUserList:(NSInteger)page successBlock:(void (^)(NSArray *listUser))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

@end

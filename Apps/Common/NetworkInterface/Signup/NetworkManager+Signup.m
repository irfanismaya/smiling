//  NetworkManager+Signup.m
//  Smiling
//  Created by Irfan-Ismaya on 16/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NetworkManager+Signup.h"
#import "Signup.h"
#import <DCKeyValueObjectMapping/DCKeyValueObjectMapping.h>

@implementation NetworkManager (Signup)

+ (NSURLSessionDataTask *)signup:(id)param successBlock:(void (^)(Signup *signup))successBlock failureBlock:(void (^)(NSError *error))failureBlock{
    return [[self sharedManager] POST:SIGNUP
                           parameters:param
                             progress:nil
                              success: ^(NSURLSessionDataTask *_Nonnull task, id _Nonnull responseObject){
                NSLog(@"JSON: %@", responseObject);
                DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass: [Signup class]];
                Signup *signup = [parser parseDictionary:responseObject];
                
                if(signup != nil) {
                    NSLog(@"Token: %@", signup.token);
                } else {
                    NSLog(@"Signup nil");
                }
                
                if (successBlock) {
                    successBlock(signup);
                }
            }failure: ^(NSURLSessionDataTask *_Nonnull task, NSError *_Nonnull error){
                if (failureBlock) {
                    failureBlock(error);
                }
            }];
}

@end

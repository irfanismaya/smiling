//  NetworkManager+Signup.h
//  Smiling
//  Created by Irfan-Ismaya on 16/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NetworkManager.h"
#import "Signup.h"

@interface NetworkManager (Signup)

+ (NSURLSessionDataTask *)signup:(id)param successBlock:(void (^)(Signup *signup))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

@end


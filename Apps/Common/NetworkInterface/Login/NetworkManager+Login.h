//  NetworkManager+Login.h
//  Smiling
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NetworkManager.h"
#import "Login.h"

@interface NetworkManager (Login)

+ (NSURLSessionDataTask *)login:(id)param successBlock:(void (^)(Login *login))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

@end

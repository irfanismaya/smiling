//  NetworkManager+Login.m
//  Smiling
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NetworkManager+Login.h"
#import "Login.h"
#import <DCKeyValueObjectMapping/DCKeyValueObjectMapping.h>

@implementation NetworkManager (Login)

+ (NSURLSessionDataTask *)login:(id)param successBlock:(void (^)(Login *login))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
    return [[self sharedManager] POST:LOGIN
                           parameters:param
                             progress:nil
                              success: ^(NSURLSessionDataTask *_Nonnull task, id _Nonnull responseObject)
            {
                NSLog(@"JSON: %@", responseObject);
                
                DCKeyValueObjectMapping *parser = [DCKeyValueObjectMapping mapperForClass: [Login class]];
                Login *login = [parser parseDictionary:responseObject];
                
                if(login != nil) {
                    NSLog(@"Token: %@", login.token);
                } else {
                    NSLog(@"Login nil");
                }
                
                if (successBlock) {
                    successBlock(login);
                }
            }
                              failure: ^(NSURLSessionDataTask *_Nonnull task, NSError *_Nonnull error)
            {
                if (failureBlock) {
                    failureBlock(error);
                }
            }];
}

@end

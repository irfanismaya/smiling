//
//  KBTabbar.m
//  Created by Irfan-Ismaya on 29/05/19.
//  Copyright © 2019 Disparbud. All rights reserved.

#import "KBTabbar.h"

@implementation KBTabbar


- (instancetype)init{
    self = [super init];
    if (self) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:@"point-of-service"] forState:UIControlStateNormal];
        btn.bounds = CGRectMake(0, 0, 65, 65);
        self.centerBtn = btn;
        [self addSubview:btn];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.centerBtn.center = CGPointMake(self.bounds.size.width * 0.5, self.bounds.size.height * 0.3);
    
    int index = 0;
    CGFloat wigth = self.bounds.size.width / 5;
    for (UIView* sub in self.subviews) {
        if ([sub isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
                switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                    case 1136:
                        printf("iPhone 5 or 5S or 5C");
                        sub.frame = CGRectMake(index * wigth, self.bounds.origin.y, wigth, self.bounds.size.height - 1);
                        break;
                    case 1334:
                        printf("iPhone 6/6S/7/8");
                        sub.frame = CGRectMake(index * wigth, self.bounds.origin.y, wigth, self.bounds.size.height - 1);
                        break;
                    case 2208:
                        printf("iPhone 6+/6S+/7+/8+");
                        sub.frame = CGRectMake(index * wigth, self.bounds.origin.y, wigth, self.bounds.size.height - 1);
                        break;
                    case 1792:
                        printf("iPhone XR");
                        sub.frame = CGRectMake(index * wigth, self.bounds.origin.y, wigth, self.bounds.size.height - 20);
                        break;
                    case 2436:
                        printf("iPhone X");
                        sub.frame = CGRectMake(index * wigth, self.bounds.origin.y, wigth, self.bounds.size.height - 20);
                        break;
                    case 2688:
                        printf("iPhone XS Max");
                        sub.frame = CGRectMake(index * wigth, self.bounds.origin.y, wigth, self.bounds.size.height - 20);
                        break;
                    default:
                        printf("unknown");
                }
            }
            index++;
            if (index == 2) {
                index++;
            }
        }
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    if (self.isHidden == NO) {
        CGPoint newPoint = [self convertPoint:point toView:self.centerBtn];
        if ( [self.centerBtn pointInside:newPoint withEvent:event]) {
            return self.centerBtn;
        }else{
            return [super hitTest:point withEvent:event];
        }
    }else {
        return [super hitTest:point withEvent:event];
    }
}

@end

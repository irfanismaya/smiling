//
//  DetailNewsCell.h
//  Smiling
//
//  Created by Irfan-Ismaya on 30/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailNewsCell : UITableViewCell
+ (DetailNewsCell*) detailNewsCell;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblContentNews;
@end

NS_ASSUME_NONNULL_END

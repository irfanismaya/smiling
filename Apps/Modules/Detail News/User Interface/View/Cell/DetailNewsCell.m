//
//  DetailNewsCell.m
//  Smiling
//
//  Created by Irfan-Ismaya on 30/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "DetailNewsCell.h"

@implementation DetailNewsCell

+ (DetailNewsCell*) detailNewsCell{
    DetailNewsCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"DetailNewsCell" owner:self options:nil] objectAtIndex:0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
       
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];
}

@end

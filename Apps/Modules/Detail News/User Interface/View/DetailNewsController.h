//  DetailNewsController.h
//  Smiling
//  Created by Irfan-Ismaya on 25/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
#import "TGFoursquareLocationDetail.h"
#import "DetailNewsCell.h"


NS_ASSUME_NONNULL_BEGIN

@interface DetailNewsController : UIViewController<UITableViewDataSource,UITableViewDelegate,TGFoursquareLocationDetailDelegate, SliderImagePagerDelegate, SliderImagePagerDataSource>
@property (nonatomic, strong) TGFoursquareLocationDetail *locationDetail;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitle;
@end

NS_ASSUME_NONNULL_END

//  DetailNewsController.m
//  Smiling
//  Created by Irfan-Ismaya on 25/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "DetailNewsController.h"
#import <MaterialComponents/MaterialAppBar.h>

@interface DetailNewsController ()
@property(nonatomic, strong) MDCAppBar *appBar;
@end

@implementation DetailNewsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.locationDetail = [[TGFoursquareLocationDetail alloc] initWithFrame:self.view.bounds];
    self.locationDetail.tableViewDataSource = self;
    self.locationDetail.tableViewDelegate = self;
    self.locationDetail.delegate = self;
    self.locationDetail.parallaxScrollFactor = 0.3;
    
    [self.view addSubview:self.locationDetail];
    
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self.view bringSubviewToFront:_headerView];
    
    self.locationDetail.headerView = _headerView;
    
    [self initAppBar];
}

-(void)initAppBar{
    self.view.tintColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Maribaya";
    _appBar = [[MDCAppBar alloc] init];
    [self addChildViewController:_appBar.headerViewController];
    
    self.appBar.navigationBar.tintColor = UIColor.whiteColor;
    self.appBar.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName : UIColor.whiteColor};
    self.appBar.headerViewController.headerView.backgroundColor = [UIColor clearColor];
    self.appBar.headerViewController.headerView.tintColor = UIColor.whiteColor;
    [self.appBar addSubviewsToParent];
    
    UIImage *menuItemImage = [UIImage imageNamed:@"baseline_arrow_back_ios_white_24pt"];
    UIImage *templatedMenuItemImage = [menuItemImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIBarButtonItem *menuItem =
    [[UIBarButtonItem alloc] initWithImage:templatedMenuItemImage
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(actionBack:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                
            case 1136:
                printf("iPhone 5 or 5S or 5C");
                self.headerView.frame = CGRectMake(0 , 0, self.view.frame.size.width, 76);
                break;
            case 1334:
                printf("iPhone 6/6S/7/8");
                self.headerView.frame = CGRectMake(0 , 0, self.view.frame.size.width, 76);
                break;
            case 2208:
                printf("iPhone 6+/6S+/7+/8+");
                self.headerView.frame = CGRectMake(0 , 0, self.view.frame.size.width, 100);
                break;
            case 2436:
                printf("iPhone X");
                self.headerView.frame = CGRectMake(0 , 0, self.view.frame.size.width, 100);
                break;
            case 2688:
                printf("iPhone XS Max");
                self.headerView.frame = CGRectMake(0 , 0, self.view.frame.size.width, 100);
                break;
            default:
                printf("unknown");
        }
    }
}

#pragma mark - UITableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   return 750.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DetailNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailNewsCell"];
    if(cell == nil){
        cell = [DetailNewsCell detailNewsCell];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.contentView.backgroundColor = [UIColor clearColor];
}

#pragma mark - LocationDetailViewDelegate
- (void)locationDetail:(TGFoursquareLocationDetail *)locationDetail imagePagerDidLoad:(SliderImagePager *)imagePager{
    imagePager.dataSource = self;
    imagePager.delegate = self;
    imagePager.pageControl.hidden=YES;
    imagePager.pageControl.currentPageIndicatorTintColor = [UIColor clearColor];
    imagePager.pageControl.pageIndicatorTintColor = [UIColor clearColor];
    imagePager.slideshowTimeInterval = 0.0f;
    imagePager.slideshowShouldCallScrollToDelegate = NO;
    
    self.locationDetail.nbImages = [self.locationDetail.imagePager.dataSource.arrayWithImages count];
    self.locationDetail.currentImage = 0;
    //[imagePager updateCaptionLabelForImageAtIndex:self.locationDetail.currentImage];
}

- (void)locationDetail:(TGFoursquareLocationDetail *)locationDetail tableViewDidLoad:(UITableView *)tableView{
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
}

- (void)locationDetail:(TGFoursquareLocationDetail *)locationDetail headerViewDidLoad:(UIView *)headerView{
    [headerView setAlpha:0.0];
    [headerView setHidden:YES];
}

#pragma mark - SliderImagePager DataSource
- (NSArray *) arrayWithImages{
    return @[@"https://irs2.4sqi.net/img/general/500x500/2514_BvEN_Q6lG50xZQ9TIG0XY8eYXzF3USSMdtTmxHCmqnE.jpg"];
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image{
    return UIViewContentModeScaleAspectFill;
}

#pragma mark - SliderImagePager Delegate
- (void) imagePager:(SliderImagePager *)imagePager didScrollToIndex:(NSUInteger)index{
    NSLog(@"%s %lu", __PRETTY_FUNCTION__, (unsigned long)index);
}

- (void) imagePager:(SliderImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index{
    NSLog(@"%s %lu", __PRETTY_FUNCTION__, (unsigned long)index);
}

#pragma mark - Button actions
- (void)back{
    NSLog(@"Here you should go back to previous view controller");
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end

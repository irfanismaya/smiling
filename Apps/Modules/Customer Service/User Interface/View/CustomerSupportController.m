//  CustomerSupportController.m
//  Smiling
//  Created by Irfan-Ismaya on 31/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "CustomerSupportController.h"
#import <MaterialComponents/MaterialAppBar.h>
#import <MaterialComponents/MaterialCards.h>
#import "Constants.h"
#import "UIColor+HexString.h"
#import "ISMessages.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface CustomerSupportController () <MFMailComposeViewControllerDelegate>
@property(nonatomic, strong) MDCAppBar *appBar;
@property (weak, nonatomic) IBOutlet MDCCard *cardEmail;
@property (weak, nonatomic) IBOutlet MDCCard *cardCall;
@end

@implementation CustomerSupportController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initAppBar];
    
    UITapGestureRecognizer *helpEmail =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(helpEmail:)];
    
    UITapGestureRecognizer *helpCall =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(helpCall:)];
    
    [self.cardEmail setUserInteractionEnabled:YES];
    [self.cardEmail addGestureRecognizer:helpEmail];
    
    [self.cardCall setUserInteractionEnabled:YES];
    [self.cardCall addGestureRecognizer:helpCall];
}

-(void)initAppBar{
    self.view.tintColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Customer Support";
    _appBar = [[MDCAppBar alloc] init];
    [self addChildViewController:_appBar.headerViewController];
    
    self.appBar.navigationBar.tintColor = UIColor.whiteColor;
    self.appBar.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName : UIColor.whiteColor};
    self.appBar.headerViewController.headerView.backgroundColor = [UIColor colorWithHexString:primary_color_purple];
    self.appBar.headerViewController.headerView.tintColor = UIColor.whiteColor;
    [self.appBar addSubviewsToParent];
    
    UIImage *menuItemImage = [UIImage imageNamed:@"baseline_arrow_back_ios_white_24pt"];
    UIImage *templatedMenuItemImage = [menuItemImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIBarButtonItem *menuItem =
    [[UIBarButtonItem alloc] initWithImage:templatedMenuItemImage
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(actionBack:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(nullable NSError *)error {
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@",error.description);
            break;
    }
    [controller dismissViewControllerAnimated:true completion:nil];
}

- (void)helpEmail:(UITapGestureRecognizer *)recognizer{
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [MFMailComposeViewController new];
        mailCont.mailComposeDelegate = self;
        
        [mailCont setSubject:@""];
        [mailCont setToRecipients:[NSArray arrayWithObject:@"help@smiling.com"]];
        [mailCont setMessageBody:@"Please enter message" isHTML:NO];
        [self presentViewController:mailCont animated:YES completion:nil];
    }
}

- (void)helpCall:(UITapGestureRecognizer *)recognizer{
    NSString *phNo = @"081200300";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl options:@{} completionHandler:^(BOOL success) {
            if (success) {}
        }];
    }else{
        [ISMessages showCardAlertWithTitle:@"Help"
                                   message:@"Call facility is not available!"
                                  duration:2.f
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:nil];
    }
}
@end

//  SearchLocationController.m
//  Smiling
//  Created by Irfan-Ismaya on 04/06/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "SearchLocationController.h"
#import "SearchStringPickerViewController.h"

@interface SearchLocationController ()<UITextFieldDelegate, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgBack;
@property (weak, nonatomic) IBOutlet UIView *uiViewSearch;
@property (weak, nonatomic) IBOutlet UITextField *tfLocation;
@property (weak, nonatomic) IBOutlet UITextField *tfNameLocation;
@end
NSArray *locationName;
@implementation SearchLocationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.uiViewSearch.layer.borderColor = [UIColor grayColor].CGColor;
    self.uiViewSearch.layer.borderWidth = 1.0f;
    self.uiViewSearch.layer.masksToBounds = TRUE;
    self.uiViewSearch.layer.cornerRadius = 4.0f;
    
    locationName  = @[@"BANDUNG", @"BOGOR", @"CIREBON", @"KUNINGAN", @"CIREBON", @"MAJALENGKA", @"PURWAKARTA", @"SUBANG", @"CIMAHI"];
    
    UITapGestureRecognizer *actionBack =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionBack:)];
    [self.imgBack setUserInteractionEnabled:YES];
    [self.imgBack addGestureRecognizer:actionBack];
    
    UIButton *eyeButtonVisible = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
    [eyeButtonVisible setTitle:@"" forState:UIControlStateNormal];
    [eyeButtonVisible setImage:[UIImage imageNamed:@"baseline_expand_more_black_24pt.png"] forState:UIControlStateNormal];
    [eyeButtonVisible addTarget:self
                         action:nil
               forControlEvents:UIControlEventTouchUpInside];
    
    self.tfLocation.textColor = [UIColor darkGrayColor];
    self.tfLocation.rightViewMode = UITextFieldViewModeAlways;
    self.tfLocation.rightView = eyeButtonVisible;
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.tfLocation) {
        [self.view endEditing:YES];
        [self goChooseLocation];
    }
    return YES;
}

- (IBAction)textFieldDidChange:(UITextField *)textField{
    if (textField == self.tfNameLocation) {
     
    }
}

- (void)goChooseLocation{
    [SearchStringPickerViewController showPickerWithTitle:@"Choose Location"
                                                     rows:locationName
                                         initialSelection:[locationName indexOfObject:self.tfLocation.text]
                                               sourceView:nil
                                                doneBlock:^(NSInteger selectedIndex, NSString *selectedValue) {
                                                    [self.tfLocation setText:selectedValue];
                                                }
                                              cancelBlock:nil
                                presentFromViewController:self];
}

- (IBAction)dismissKeyboard{
    [self.view endEditing:YES];
}

@end

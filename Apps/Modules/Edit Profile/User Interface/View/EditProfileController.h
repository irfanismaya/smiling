//  EditProfileController.h
//  Smiling
//  Created by Irfan-Ismaya on 12/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import <MaterialComponents/MaterialButtons.h>
#import <MaterialComponents/MaterialTextFields.h>
#import "MaterialButtons.h"
#import "CircleImageView.h"
#import "FileUtil.h"

@interface EditProfileController : UIViewController
@property (weak, nonatomic) IBOutlet CircleImageView *uiImageProfile;
@property (weak, nonatomic) IBOutlet UIScrollView *uiScrollView;
@property UIImage* image;
@end


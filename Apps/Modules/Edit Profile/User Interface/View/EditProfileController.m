//  EditProfileController.m
//  Smiling
//  Created by Irfan-Ismaya on 12/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "EditProfileController.h"
#import <MaterialComponents/MaterialAppBar.h>
#import <MaterialComponents/MaterialButtons.h>
#import "Constants.h"
#import "NSDate+TCUtils.h"
#import "UIColor+HexString.h"
#import "LSLDatePickerDialog.h"
#import "ActionSheetStringPicker.h"
#import "ActionSheetDatePicker.h"
#import "ISMessages.h"
#import "AHKActionSheet.h"
#import "JDStatusBarNotification.h"
#import "UIImage+ImageHelper.h"
#import <MaterialTextField/MaterialTextField.h>
#import "ActionSheetStringPicker.h"

NSString *const ErrorDomain = @"ErrorDomain";
NSInteger const ErrorDomainCode = 100;
@interface EditProfileController ()<UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) AbstractActionSheetPicker *actionSheetPicker;
@property(nonatomic, strong) MDCAppBar *appBar;
@property (weak, nonatomic) IBOutlet MFTextField *tfUsername;
@property (weak, nonatomic) IBOutlet MFTextField *tfPhoneNumber;
@property (weak, nonatomic) IBOutlet MFTextField *tfGender;
@property (weak, nonatomic) IBOutlet MFTextField *tfBirthday;
@property (weak, nonatomic) IBOutlet MDCRaisedButton *actionSave;
@end

@implementation EditProfileController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.selectedDate = [NSDate date];
    [self initAppBar];
    [self setupForm];
    
    UITapGestureRecognizer *changeImage =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(changeImage:)];
    
    [self.uiImageProfile setUserInteractionEnabled:YES];
    [self.uiImageProfile addGestureRecognizer:changeImage];
}


-(void)initAppBar{
    self.view.tintColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Edit Profile";
    _appBar = [[MDCAppBar alloc] init];
    [self addChildViewController:_appBar.headerViewController];
    
    self.appBar.navigationBar.tintColor = UIColor.whiteColor;
    self.appBar.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName : UIColor.whiteColor};
    self.appBar.headerViewController.headerView.backgroundColor = [UIColor colorWithHexString:primary_color_purple];
    self.appBar.headerViewController.headerView.tintColor = UIColor.whiteColor;
    [self.appBar addSubviewsToParent];
    
    UIImage *menuItemImage = [UIImage imageNamed:@"baseline_arrow_back_ios_white_24pt"];
    UIImage *templatedMenuItemImage = [menuItemImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIBarButtonItem *menuItem =
    [[UIBarButtonItem alloc] initWithImage:templatedMenuItemImage
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(actionBack:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)changeImage:(UITapGestureRecognizer *)recognizer{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Change Photo" message:nil
                    preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    [self dismissViewControllerAnimated:YES completion:^{}];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self pickerPhotoCamera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self pickerPhotoGallery];
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)setupForm{
    UIImageView *lockImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_person_black_24pt.png"]];
    lockImage.bounds = CGRectMake(0, 0, 35, 35);
    lockImage.contentMode = UIViewContentModeLeft;
    self.tfUsername.tintColor = [UIColor mf_veryDarkGrayColor];
    self.tfUsername.textColor = [UIColor mf_veryDarkGrayColor];
    self.tfUsername.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.tfUsername.placeholderAnimatesOnFocus = YES;
    UIFontDescriptor * fontDescriptor = [self.tfUsername.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *font = [UIFont fontWithDescriptor:fontDescriptor size:self.tfUsername.font.pointSize];
    self.tfUsername.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSFontAttributeName:font}];
    self.tfUsername.leftViewMode = UITextFieldViewModeAlways;
    self.tfUsername.leftView = lockImage;
    
    UIImageView *lockImagePhone = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_phone_black_24pt.png"]];
    lockImagePhone.bounds = CGRectMake(0, 0, 35, 35);
    lockImagePhone.contentMode = UIViewContentModeLeft;
    self.tfPhoneNumber.tintColor = [UIColor mf_veryDarkGrayColor];
    self.tfPhoneNumber.textColor = [UIColor mf_veryDarkGrayColor];
    self.tfPhoneNumber.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.tfPhoneNumber.placeholderAnimatesOnFocus = YES;
    UIFontDescriptor * fontDescriptorPhone = [self.tfPhoneNumber.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *fontPhone = [UIFont fontWithDescriptor:fontDescriptorPhone size:self.tfPhoneNumber.font.pointSize];
    self.tfPhoneNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Phone Number" attributes:@{NSFontAttributeName:fontPhone}];
    self.tfPhoneNumber.leftViewMode = UITextFieldViewModeAlways;
    self.tfPhoneNumber.leftView = lockImagePhone;
    
    UIImageView *lockImageGender = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_wc_black_24pt.png"]];
    lockImageGender.bounds = CGRectMake(0, 0, 35, 35);
    lockImageGender.contentMode = UIViewContentModeLeft;
    self.tfGender.tintColor = [UIColor mf_veryDarkGrayColor];
    self.tfGender.textColor = [UIColor mf_veryDarkGrayColor];
    self.tfGender.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.tfGender.placeholderAnimatesOnFocus = YES;
    UIFontDescriptor * fontDescriptorGender = [self.tfGender.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *fontGender = [UIFont fontWithDescriptor:fontDescriptorGender size:self.tfGender.font.pointSize];
    self.tfGender.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Gender" attributes:@{NSFontAttributeName:fontGender}];
    self.tfGender.leftViewMode = UITextFieldViewModeAlways;
    self.tfGender.leftView = lockImageGender;
    
    UIImageView *lockImageBirthday = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_date_range_black_24pt.png"]];
    lockImageBirthday.bounds = CGRectMake(0, 0, 35, 35);
    lockImageBirthday.contentMode = UIViewContentModeLeft;
    self.tfBirthday.tintColor = [UIColor mf_veryDarkGrayColor];
    self.tfBirthday.textColor = [UIColor mf_veryDarkGrayColor];
    self.tfBirthday.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.tfBirthday.placeholderAnimatesOnFocus = YES;
    UIFontDescriptor * fontDescriptorBirth = [self.tfBirthday.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *fontBirth = [UIFont fontWithDescriptor:fontDescriptorBirth size:self.tfBirthday.font.pointSize];
    self.tfBirthday.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Birthday" attributes:@{NSFontAttributeName:fontBirth}];
    self.tfBirthday.leftViewMode = UITextFieldViewModeAlways;
    self.tfBirthday.leftView = lockImageBirthday;
    
    self.actionSave.layer.masksToBounds = TRUE;
    self.actionSave.layer.cornerRadius = 27.5f;
    
}

- (IBAction)dismissKeyboard{
    [self.view endEditing:YES];
}

- (IBAction)textFieldDidChange:(UITextField *)textField{
    if (textField == self.tfUsername) {
        [self validateUsername];
    }else if (textField == self.tfPhoneNumber) {
        [self validatePhone];
    }else if (textField == self.tfGender) {
        [self validateGender];
    }else if (textField == self.tfBirthday) {
        [self validateBirthday];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.tfGender) {
        
    }else if (textField == self.tfBirthday) {
        
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.tfUsername) {
        [self.tfPhoneNumber becomeFirstResponder];
    }else if(textField == self.tfPhoneNumber){
        [self.tfGender becomeFirstResponder];
    }else if(textField == self.tfGender){
        [self.tfGender resignFirstResponder];
    }else{
        [self.tfGender resignFirstResponder];
    }
    return YES;
}

#pragma mark - Text field validation
- (void)validateUsername{
    NSError *error = nil;
    if (![self textUsernameIsValid]) {
        error = [self errorWithLocalizedDescription:@"The username must be at least 8 characters long and must contain at least one character and one number"];
    }else{
        
    }
    [self.tfUsername setError:error animated:YES];
}

- (void)validatePhone{
    NSError *error = nil;
    if (![self textPhoneIsValid]) {
        error = [self errorWithLocalizedDescription:@"The phone number must be at least 10 characters long or 12 characters"];
    }else{
        [self.view endEditing:YES];
    }
    [self.tfPhoneNumber setError:error animated:YES];
}

- (void)validateGender{
    NSError *error = nil;
    if (![self textGenderIsValid]) {
        error = [self errorWithLocalizedDescription:@"Gender field is required"];
    }else{
        [self.view endEditing:YES];
    }
    [self.tfGender setError:error animated:YES];
}

- (void)validateBirthday{
    NSError *error = nil;
    if (![self textBirthIsValid]) {
        error = [self errorWithLocalizedDescription:@"Birthday field is required"];
    }else{
        [self.view endEditing:YES];
    }
    [self.tfBirthday setError:error animated:YES];
}

- (BOOL)textUsernameIsValid{
    return self.tfUsername.text.length >= 8;
}

- (BOOL)textPhoneIsValid{
    return self.tfPhoneNumber.text.length == 10 || self.tfPhoneNumber.text.length == 12;
}

- (BOOL)textGenderIsValid{
    return self.tfGender.text.length <= 5;
}

- (BOOL)textBirthIsValid{
    return self.tfBirthday.text.length <= 5;
}

-(void)datePicker{
    [self.view endEditing:YES];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *minimumDateComponents = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    [minimumDateComponents setYear:2000];
    NSDate *minDate = [calendar dateFromComponents:minimumDateComponents];
    NSDate *maxDate = [NSDate date];
    
    
    ActionSheetDatePicker *pickerdate = [[ActionSheetDatePicker alloc] initWithTitle:@"Pilih Tanggal Lahir" datePickerMode:UIDatePickerModeDate selectedDate:self.selectedDate
                                                  target:self
                                                  action:@selector(dateWasSelected:element:)
                                                  origin:nil
    ];
    [pickerdate setMinimumDate:minDate];
    [pickerdate setMaximumDate:maxDate];
    pickerdate.hideCancel = NO;
    [pickerdate showActionSheetPicker];
}


- (IBAction)chooseGender:(id)sender {
    [self.view endEditing:YES];
    NSArray *gender = [NSArray arrayWithObjects:@"Laki-Laki", @"Perempuan", nil];
    ActionSheetStringPicker* picker = [[ActionSheetStringPicker alloc] initWithTitle:@"Pilih Jenis Kelamin" rows:gender initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self.tfGender setText:selectedValue];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:sender];
    picker.tapDismissAction = TapActionCancel;
    [picker showActionSheetPicker];
}

- (IBAction)dateBirthday:(id)sender {
    [self.view endEditing:YES];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *minimumDateComponents = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    [minimumDateComponents setYear:2000];
    NSDate *minDate = [calendar dateFromComponents:minimumDateComponents];
    NSDate *maxDate = [NSDate date];
    
    ActionSheetDatePicker *pickerdate = [[ActionSheetDatePicker alloc] initWithTitle:@"Pilih Tanggal Lahir" datePickerMode:UIDatePickerModeDate selectedDate:self.selectedDate
                                                  target:self
                                                  action:@selector(dateWasSelected:element:)
                                                  origin:sender];
    [pickerdate setMinimumDate:minDate];
    [pickerdate setMaximumDate:maxDate];
    pickerdate.hideCancel = NO;
    [pickerdate showActionSheetPicker];
}

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    self.self.tfBirthday.text = [self.selectedDate description];
}

- (IBAction)actionSave:(id)sender {
    if([self validationFormChangeProfile]){
        
    }
}

- (NSError *)errorWithLocalizedDescription:(NSString *)localizedDescription{
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: localizedDescription};
    return [NSError errorWithDomain:ErrorDomain code:ErrorDomainCode userInfo:userInfo];
}

-(void)pickerPhotoCamera{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"File Exist" message:@" No Camera found." preferredStyle: UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }];
        
        [controller addAction:action];
        [self presentViewController:controller animated:YES completion:nil];
        return;
    }
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:true completion:nil];
}

-(void)pickerPhotoGallery{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    self.image = [chosenImage scaleToFitWidth:345.0f];
    self.image = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(345, 245)];
    self.uiImageProfile.image = self.image;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(BOOL)validationFormChangeProfile{
    NSString* Name         = self.tfUsername.text;
    NSString* Phone        = self.tfPhoneNumber.text;
    NSString* Gender       = self.tfGender.text;
    NSString* Birthday     = self.tfBirthday.text;
    
    if([Name isEqualToString:@""] && [Phone isEqualToString:@""] && [Gender isEqualToString:@""] && [Birthday isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfUsername setError:errorName animated:YES];
        
        NSError *errorPhone = nil;
        errorPhone = [self errorWithLocalizedDescription:@"Phone field is required"];
        [self.tfPhoneNumber setError:errorPhone animated:YES];
        
        NSError *errorGender = nil;
        errorGender = [self errorWithLocalizedDescription:@"Gender field is required"];
        [self.tfGender setError:errorGender animated:YES];
        
        NSError *errorBirthday = nil;
        errorBirthday = [self errorWithLocalizedDescription:@"Birthday field is required"];
        [self.tfBirthday setError:errorBirthday animated:YES];
        
        return FALSE;
    }else if([Phone isEqualToString:@""] && [Gender isEqualToString:@""] && [Birthday isEqualToString:@""]){
        NSError *errorPhone = nil;
        errorPhone = [self errorWithLocalizedDescription:@"Phone field is required"];
        [self.tfPhoneNumber setError:errorPhone animated:YES];
        
        NSError *errorGender = nil;
        errorGender = [self errorWithLocalizedDescription:@"Gender field is required"];
        [self.tfGender setError:errorGender animated:YES];
        
        NSError *errorBirthday = nil;
        errorBirthday = [self errorWithLocalizedDescription:@"Birthday field is required"];
        [self.tfBirthday setError:errorBirthday animated:YES];
        
        return FALSE;
    }else if([Name isEqualToString:@""] && [Phone isEqualToString:@""] && [Gender isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfUsername setError:errorName animated:YES];
        
        NSError *errorPhone = nil;
        errorPhone = [self errorWithLocalizedDescription:@"Phone field is required"];
        [self.tfPhoneNumber setError:errorPhone animated:YES];
        
        NSError *errorGender = nil;
        errorGender = [self errorWithLocalizedDescription:@"Gender field is required"];
        [self.tfGender setError:errorGender animated:YES];
        
        return FALSE;
    }else if([Name isEqualToString:@""] && [Gender isEqualToString:@""] && [Birthday isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfUsername setError:errorName animated:YES];
        
        NSError *errorGender = nil;
        errorGender = [self errorWithLocalizedDescription:@"Gender field is required"];
        [self.tfGender setError:errorGender animated:YES];
        
        NSError *errorBirthday = nil;
        errorBirthday = [self errorWithLocalizedDescription:@"Birthday field is required"];
        [self.tfBirthday setError:errorBirthday animated:YES];
        
        return FALSE;
    }else if([Name isEqualToString:@""] && [Phone isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfUsername setError:errorName animated:YES];
        
        NSError *errorPhone = nil;
        errorPhone = [self errorWithLocalizedDescription:@"Phone field is required"];
        [self.tfPhoneNumber setError:errorPhone animated:YES];
        
        return FALSE;
    }else if([Gender isEqualToString:@""] && [Birthday isEqualToString:@""]){
        NSError *errorGender = nil;
        errorGender = [self errorWithLocalizedDescription:@"Gender field is required"];
        [self.tfGender setError:errorGender animated:YES];
        
        NSError *errorBirthday = nil;
        errorBirthday = [self errorWithLocalizedDescription:@"Birthday field is required"];
        [self.tfBirthday setError:errorBirthday animated:YES];
        
        return FALSE;
    }else if([Name isEqualToString:@""] && [Gender isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfUsername setError:errorName animated:YES];
        
        NSError *errorGender = nil;
        errorGender = [self errorWithLocalizedDescription:@"Gender field is required"];
        [self.tfGender setError:errorGender animated:YES];
        
        return FALSE;
    }else if([Name isEqualToString:@""] && [Birthday isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfUsername setError:errorName animated:YES];
        
        NSError *errorBirthday = nil;
        errorBirthday = [self errorWithLocalizedDescription:@"Birthday field is required"];
        [self.tfBirthday setError:errorBirthday animated:YES];
        
        return FALSE;
    }else if([Phone isEqualToString:@""] && [Gender isEqualToString:@""]){
        NSError *errorPhone = nil;
        errorPhone = [self errorWithLocalizedDescription:@"Phone field is required"];
        [self.tfPhoneNumber setError:errorPhone animated:YES];
        
        NSError *errorGender = nil;
        errorGender = [self errorWithLocalizedDescription:@"Gender field is required"];
        [self.tfGender setError:errorGender animated:YES];
        
        return FALSE;
    }else if([Name isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfUsername setError:errorName animated:YES];
        
        return FALSE;
    }else if([Phone isEqualToString:@""]){
        NSError *errorPhone = nil;
        errorPhone = [self errorWithLocalizedDescription:@"Phone field is required"];
        [self.tfPhoneNumber setError:errorPhone animated:YES];
        
        return FALSE;
    }else if([Gender isEqualToString:@""]){
        NSError *errorGender = nil;
        errorGender = [self errorWithLocalizedDescription:@"Gender field is required"];
        [self.tfGender setError:errorGender animated:YES];
        
        return FALSE;
    }else if([Birthday isEqualToString:@""]){
        NSError *errorBirthday = nil;
        errorBirthday = [self errorWithLocalizedDescription:@"Birthday field is required"];
        [self.tfBirthday setError:errorBirthday animated:YES];
        
        return FALSE;
    }else{
        return TRUE;
    }
    return TRUE;
}
@end

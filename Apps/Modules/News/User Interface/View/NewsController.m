//  NewsController.m
//  Smiling
//  Created by Irfan-Ismaya on 22/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NewsController.h"
#import "HeaderNews.h"
#import "NewsCollectionCell.h"
#import "DetailNewsController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface NewsController ()
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgBack;
@property (weak, nonatomic) IBOutlet UICollectionView *newsCollection;
@end
NSArray * newsGallery;
NSArray * imgNews;
static HeaderNews *headerView;
@implementation NewsController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initViewHeader];
    
    UITapGestureRecognizer *actionBack =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionBack:)];
    [self.imgBack setUserInteractionEnabled:YES];
    [self.imgBack addGestureRecognizer:actionBack];
    
    newsGallery = [[NSArray alloc] initWithObjects:
                  [UIImage imageNamed:@"bg1@2x.png"],
                  [UIImage imageNamed:@"bg2@2x.png"],
                  [UIImage imageNamed:@"bg3@2x.png"],
                  [UIImage imageNamed:@"bg4@2x.png"],nil];
    
    imgNews = @[@"https://cdn.shopify.com/s/files/1/1228/3124/products/photo_porterhouse_steak_bdbaef5e-7c42-4893-ae30-ea77646eb48f_1024x1024.jpg?v=1463010050", @"https://img.okezone.com/content/2016/10/22/298/1521574/ayam-bakar-sambal-petis-sudah-lezat-pedasnya-menggoda-x2edZIIwHD.jpg", @"http://cdn2.tstatic.net/jatim/foto/bank/images/ilustrasi-sate-kambing-2_20170901_142551.jpg", @"https://cdn.idntimes.com/content-images/community/2018/04/resep-es-krim-rumahan-dbf7bd137b8c817483e299b60f989923_600x400.jpg"];
}

-(void)initViewHeader{
    for (UIView *subview in self.searchBar.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
            break;
        }
        for (UIView *subsubview in subview.subviews) {
            if ([subsubview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [subsubview removeFromSuperview];
                break;
            }
        }
    }
    
    self.imgBack.tintColor = [UIColor blackColor];
    self.searchBar.barTintColor = [UIColor grayColor];
    self.searchBar.layer.borderColor = [UIColor grayColor].CGColor;
    self.searchBar.backgroundColor = [UIColor whiteColor];
    
    UITextField *searchField = [self.searchBar valueForKey:@"searchField"];
    searchField.backgroundColor = [UIColor colorWithHexString:@"#F6F9FF"];
    searchField.textColor = [UIColor grayColor];
    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Cari Berita"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor grayColor];
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return newsGallery.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderNews" forIndexPath:indexPath];
        headerView.imgNews.delegate = self;
        headerView.imgNews.dataSource = self;
        headerView.imgNews.slideshowTimeInterval = 4.5f;
        headerView.imgNews.clipsToBounds = true;
        headerView.imgNews.layer.cornerRadius = 10;
        reusableview = headerView;
    }
    
    return reusableview;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NewsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewsCollectionCell" forIndexPath:indexPath];
    
    cell.uiView.clipsToBounds = true;
    cell.uiView.layer.cornerRadius = 10;
    cell.uiView.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMinXMinYCorner;
    
    [cell.imageNews sd_setImageWithURL:[NSURL URLWithString:imgNews[indexPath.row]]
                        placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
    
    cell.imageNews.clipsToBounds = true;
    cell.imageNews.layer.cornerRadius = 10;
    cell.imageNews.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMinXMinYCorner;
    
    cell.uiView.clipsToBounds = true;
    cell.uiView.layer.cornerRadius = 10;
    cell.uiView.layer.maskedCorners = kCALayerMaxXMinYCorner | kCALayerMaxXMaxYCorner;
    
    cell.uiViewDate.clipsToBounds = true;
    cell.uiViewDate.layer.cornerRadius = 10;
    cell.uiViewDate.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DetailNewsController *detailNews = [storyboard instantiateViewControllerWithIdentifier:@"DetailNewsController"];
    [self.navigationController pushViewController:detailNews animated:YES];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

#pragma mark - KIImagePager DataSource
- (NSArray *) arrayWithImages:(KIImagePager*)pager
{
    return @[
             @"https://img-k.okeinfo.net/content/2019/03/01/605/2024398/melihat-kembali-tahapan-pemilu-2019-dari-pendaftaran-hingga-presiden-terpilih-dilantik-CrKmysGf9i.jpg",
             @"https://d2d0b2rxqzh1q5.cloudfront.net/sv/2.183/dir/789/image/7896169b84bd3c1fff16c8902df38909.jpg",
             @"https://mediafiles.cineplex.com/Events/2019/avengers-endgame/img-header-v1.jpg"
            ];
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager *)pager
{
    return UIViewContentModeScaleAspectFill;
}

#pragma mark - KIImagePager Delegate
- (void) imagePager:(KIImagePager *)imagePager didScrollToIndex:(NSUInteger)index{
    
}

- (void) imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index{
    
}

@end

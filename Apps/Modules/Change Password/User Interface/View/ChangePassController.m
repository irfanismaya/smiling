//  ChangePassController.m
//  Smiling
//  Created by Irfan-Ismaya on 30/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ChangePassController.h"
#import <MaterialComponents/MaterialButtons.h>
#import <MaterialComponents/MaterialAppBar.h>
#import <MaterialTextField/MaterialTextField.h>
#import "Constants.h"
#import "UIColor+HexString.h"

NSString *const MFDemoErrorDomain = @"MFDemoErrorDomain";
NSInteger const MFDemoErrorCode = 100;

@interface ChangePassController ()<UITextFieldDelegate, UITextViewDelegate>
@property(nonatomic, strong) MDCAppBar *appBar;
@property (weak, nonatomic) IBOutlet MFTextField *tfOldPass;
@property (weak, nonatomic) IBOutlet MFTextField *tfNewPass;
@property (weak, nonatomic) IBOutlet MFTextField *tfConfPass;
@property (weak, nonatomic) IBOutlet MDCRaisedButton *changePass;
@end

@implementation ChangePassController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initAppBar];
    [self setupForm];
}

-(void)initAppBar{
    self.view.tintColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Change Password";
    _appBar = [[MDCAppBar alloc] init];
    [self addChildViewController:_appBar.headerViewController];
    
    self.appBar.navigationBar.tintColor = UIColor.whiteColor;
    self.appBar.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName : UIColor.whiteColor};
    self.appBar.headerViewController.headerView.backgroundColor = [UIColor colorWithHexString:primary_color_purple];
    self.appBar.headerViewController.headerView.tintColor = UIColor.whiteColor;
    [self.appBar addSubviewsToParent];
    
    UIImage *menuItemImage = [UIImage imageNamed:@"baseline_arrow_back_ios_white_24pt"];
    UIImage *templatedMenuItemImage = [menuItemImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIBarButtonItem *menuItem =
    [[UIBarButtonItem alloc] initWithImage:templatedMenuItemImage
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(actionBack:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Setup

- (void)setupForm
{
    UIButton *eyeButtonVisible = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
    [eyeButtonVisible setTitle:@"" forState:UIControlStateNormal];
    [eyeButtonVisible setImage:[UIImage imageNamed:@"baseline_visibility_black_24pt.png"] forState:UIControlStateNormal];
    [eyeButtonVisible setImage:[UIImage imageNamed:@"baseline_visibility_off_black_24pt.png"] forState:UIControlStateSelected];
    [eyeButtonVisible addTarget:self
                         action:@selector(togglePassword)
               forControlEvents:UIControlEventTouchUpInside];
    UIImageView *lockImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_lock_black_24pt.png"]];
    lockImage.bounds = CGRectMake(0, 0, 35, 35);
    lockImage.contentMode = UIViewContentModeLeft;
    self.tfOldPass.tintColor = [UIColor mf_veryDarkGrayColor];
    self.tfOldPass.textColor = [UIColor mf_veryDarkGrayColor];
    self.tfOldPass.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.tfOldPass.placeholderAnimatesOnFocus = YES;
    self.tfOldPass.textColor = [UIColor darkGrayColor];
    self.tfOldPass.rightViewMode = UITextFieldViewModeAlways;
    self.tfOldPass.rightView = eyeButtonVisible;
    UIFontDescriptor * fontDescriptor = [self.tfOldPass.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *font = [UIFont fontWithDescriptor:fontDescriptor size:self.tfOldPass.font.pointSize];
    self.tfOldPass.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Old Password" attributes:@{NSFontAttributeName:font}];
    self.tfOldPass.leftViewMode = UITextFieldViewModeAlways;
    self.tfOldPass.leftView = lockImage;
    
    UIButton *eyeButtonVisibleNew = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
    [eyeButtonVisibleNew setTitle:@"" forState:UIControlStateNormal];
    [eyeButtonVisibleNew setImage:[UIImage imageNamed:@"baseline_visibility_black_24pt.png"] forState:UIControlStateNormal];
    [eyeButtonVisibleNew setImage:[UIImage imageNamed:@"baseline_visibility_off_black_24pt.png"] forState:UIControlStateSelected];
    [eyeButtonVisibleNew addTarget:self
                         action:@selector(togglePassword)
               forControlEvents:UIControlEventTouchUpInside];
    UIImageView *lockImageNew = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_lock_black_24pt.png"]];
    lockImageNew.bounds = CGRectMake(0, 0, 35, 35);
    lockImageNew.contentMode = UIViewContentModeLeft;
    self.tfNewPass.tintColor = [UIColor mf_veryDarkGrayColor];
    self.tfNewPass.textColor = [UIColor mf_veryDarkGrayColor];
    self.tfNewPass.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.tfNewPass.placeholderAnimatesOnFocus = YES;
    self.tfNewPass.textColor = [UIColor darkGrayColor];
    self.tfNewPass.rightViewMode = UITextFieldViewModeAlways;
    self.tfNewPass.rightView = eyeButtonVisibleNew;
    UIFontDescriptor * fontDescriptorNew = [self.tfNewPass.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *fontNew = [UIFont fontWithDescriptor:fontDescriptorNew size:self.tfNewPass.font.pointSize];
    self.tfNewPass.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSFontAttributeName:fontNew}];
    self.tfNewPass.leftViewMode = UITextFieldViewModeAlways;
    self.tfNewPass.leftView = lockImageNew;
    
    
    UIButton *eyeButtonVisibleConf = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
    [eyeButtonVisibleConf setTitle:@"" forState:UIControlStateNormal];
    [eyeButtonVisibleConf setImage:[UIImage imageNamed:@"baseline_visibility_black_24pt.png"] forState:UIControlStateNormal];
    [eyeButtonVisibleConf setImage:[UIImage imageNamed:@"baseline_visibility_off_black_24pt.png"] forState:UIControlStateSelected];
    [eyeButtonVisibleConf addTarget:self
                            action:@selector(togglePassword)
                  forControlEvents:UIControlEventTouchUpInside];
    UIImageView *lockImageNewConf = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_lock_black_24pt.png"]];
    lockImageNewConf.bounds = CGRectMake(0, 0, 35, 35);
    lockImageNewConf.contentMode = UIViewContentModeLeft;
    self.tfConfPass.tintColor = [UIColor mf_veryDarkGrayColor];
    self.tfConfPass.textColor = [UIColor mf_veryDarkGrayColor];
    self.tfConfPass.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.tfConfPass.placeholderAnimatesOnFocus = YES;
    self.tfConfPass.textColor = [UIColor darkGrayColor];
    self.tfConfPass.rightViewMode = UITextFieldViewModeAlways;
    self.tfConfPass.rightView = eyeButtonVisibleConf;
    UIFontDescriptor * fontDescriptorConf = [self.tfConfPass.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *fontConf = [UIFont fontWithDescriptor:fontDescriptorConf size:self.tfConfPass.font.pointSize];
    self.tfConfPass.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirmation Password" attributes:@{NSFontAttributeName:fontConf}];
    self.tfConfPass.leftViewMode = UITextFieldViewModeAlways;
    self.tfConfPass.leftView = lockImageNewConf;
    
    self.changePass.layer.masksToBounds = TRUE;
    self.changePass.layer.cornerRadius = 27.5f;
}

#pragma mark - Actions
- (IBAction)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)togglePassword
{
    self.tfOldPass.secureTextEntry = !self.tfOldPass.isSecureTextEntry;
}

- (IBAction)textFieldDidChange:(UITextField *)textField{
    if (textField == self.tfOldPass) {
        [self validateOldPass];
    }else if (textField == self.tfNewPass) {
        [self validateNewPass];
    }else if (textField == self.tfConfPass) {
        [self validateConfPass];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.tfOldPass) {
        [self.tfNewPass becomeFirstResponder];
    }else if(textField == self.tfNewPass){
        [self.tfConfPass becomeFirstResponder];
    }else if(textField == self.tfConfPass){
        [self.tfConfPass resignFirstResponder];
    }else{
        [self.tfConfPass resignFirstResponder];
    }
    return YES;
}


- (IBAction)actionChangePass:(id)sender {
    NSString* OldPass    = self.tfNewPass.text;
    NSString* ConfPass   = self.tfConfPass.text;
    if([self validationFormChangePass]){
        if([OldPass isEqualToString:ConfPass]){
            
        }else{
            NSError *error = nil;
            error = [self errorWithLocalizedDescription:@"Password not match."];
            [self.tfNewPass setError:error animated:YES];
            [self.tfConfPass setError:error animated:YES];
        }
    }
}

#pragma mark - Text field validation
- (void)validateOldPass{
    NSError *error = nil;
    if (![self textOldPassIsValid]) {
        error = [self errorWithLocalizedDescription:@"The password must be at least 8 characters long and must contain at least one character and one number"];
    }
    [self.tfOldPass setError:error animated:YES];
}

- (void)validateNewPass{
    NSError *error = nil;
    if (![self textNewPassIsValid]) {
        error = [self errorWithLocalizedDescription:@"The password must be at least 8 characters long and must contain at least one character and one number"];
    }
    [self.tfNewPass setError:error animated:YES];
}

- (void)validateConfPass{
    NSError *error = nil;
    if (![self textConfPassIsValid]) {
        error = [self errorWithLocalizedDescription:@"The password must be at least 8 characters long and must contain at least one character and one number"];
    }
    [self.tfConfPass setError:error animated:YES];
}

- (BOOL)textOldPassIsValid
{
    return self.tfOldPass.text.length >= 8;
}

- (BOOL)textNewPassIsValid
{
    return self.tfNewPass.text.length >= 8;
}

- (BOOL)textConfPassIsValid
{
    return self.tfConfPass.text.length >= 8;
}

-(BOOL)validationFormChangePass{
    NSString* OldPass    = self.tfOldPass.text;
    NSString* NewPass    = self.tfNewPass.text;
    NSString* ConfPass   = self.tfConfPass.text;
    
    if([OldPass isEqualToString:@""] && [NewPass isEqualToString:@""] && [ConfPass isEqualToString:@""]){
        NSError *errorOlPass = nil;
        errorOlPass = [self errorWithLocalizedDescription:@"Old password field is required"];
        [self.tfOldPass setError:errorOlPass animated:YES];
        
        NSError *errorNewPass = nil;
        errorNewPass = [self errorWithLocalizedDescription:@"New password field is required"];
        [self.tfNewPass setError:errorNewPass animated:YES];
        
        NSError *errorConfPass = nil;
        errorConfPass = [self errorWithLocalizedDescription:@"Confirmation password field is required"];
        [self.tfConfPass setError:errorConfPass animated:YES];
        
        return FALSE;
    }else  if([OldPass isEqualToString:@""] && [NewPass isEqualToString:@""]){
        NSError *errorOlPass = nil;
        errorOlPass = [self errorWithLocalizedDescription:@"Old password field is required"];
        [self.tfOldPass setError:errorOlPass animated:YES];

        NSError *errorNewPass = nil;
        errorNewPass = [self errorWithLocalizedDescription:@"New password field is required"];
        [self.tfNewPass setError:errorNewPass animated:YES];
        return FALSE;
    }else if([NewPass isEqualToString:@""] && [ConfPass isEqualToString:@""]){
        NSError *errorNewPass = nil;
        errorNewPass = [self errorWithLocalizedDescription:@"New password field is required"];
        [self.tfNewPass setError:errorNewPass animated:YES];

        NSError *errorConfPass = nil;
        errorConfPass = [self errorWithLocalizedDescription:@"Confirmation password field is required"];
        [self.tfConfPass setError:errorConfPass animated:YES];
        return FALSE;
    }else if([OldPass isEqualToString:@""] && [ConfPass isEqualToString:@""]){
        NSError *errorOlPass = nil;
        errorOlPass = [self errorWithLocalizedDescription:@"Old password field is required"];
        [self.tfOldPass setError:errorOlPass animated:YES];
        
        NSError *errorConfPass = nil;
        errorConfPass = [self errorWithLocalizedDescription:@"Confirmation password field is required"];
        [self.tfConfPass setError:errorConfPass animated:YES];
        return FALSE;
    }else if([OldPass isEqualToString:@""]){
        NSError *errorOlPass = nil;
        errorOlPass = [self errorWithLocalizedDescription:@"Old password field is required"];
        [self.tfOldPass setError:errorOlPass animated:YES];
        return FALSE;
    }else if([NewPass isEqualToString:@""]){
        NSError *errorNewPass = nil;
        errorNewPass = [self errorWithLocalizedDescription:@"New password field is required"];
        [self.tfNewPass setError:errorNewPass animated:YES];
        return FALSE;
    }else if([ConfPass isEqualToString:@""]){
        NSError *errorConfPass = nil;
        errorConfPass = [self errorWithLocalizedDescription:@"Confirmation password field is required"];
        [self.tfConfPass setError:errorConfPass animated:YES];
        return FALSE;
    }else{
        return TRUE;
    }
    return TRUE;
}

- (BOOL)isValidPassword:(NSString*)password{
    NSRegularExpression* regex = [[NSRegularExpression alloc] initWithPattern:@"^.*(?=.{6,})(?=.*[a-z])(?=.*[A-Z]).*$" options:0 error:nil];
    return [regex numberOfMatchesInString:password options:0 range:NSMakeRange(0, [password length])] > 0;
}

- (NSError *)errorWithLocalizedDescription:(NSString *)localizedDescription
{
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: localizedDescription};
    return [NSError errorWithDomain:MFDemoErrorDomain code:MFDemoErrorCode userInfo:userInfo];
}

@end

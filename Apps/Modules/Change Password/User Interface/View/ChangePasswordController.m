//  ChangePasswordController.m
//  MutiaraClinic
//  Created by Irfan-Ismaya on 08/05/19.
//  Copyright © 2019 Swamedia. All rights reserved.

#import "ChangePasswordController.h"
#import <MaterialComponents/MaterialAppBar.h>
#import <MaterialComponents/MaterialButtons.h>
#import <MaterialComponents/MaterialTextFields.h>
#import "MaterialTextFields+ColorThemer.h"
#import "MaterialTextFields+TypographyThemer.h"
#import "Constants.h"
#import "NSUserDefaultsApp.h"
#import "NSDate+TCUtils.h"
#import "UIColor+HexString.h"
#import "ActionSheetDatePicker.h"
#import "UIUtils.h"
#import "Constants.h"
#import "ISMessages.h"

@interface ChangePasswordController ()
@property(nonatomic, strong) MDCAppBar *appBar;
@property (weak, nonatomic) IBOutlet MDCTextField *tfOldPassword;
@property (weak, nonatomic) IBOutlet MDCTextField *tfNewPassword;
@property (weak, nonatomic) IBOutlet MDCTextField *tfConfPassword;
@property (weak, nonatomic) IBOutlet MDCRaisedButton *buttonChangePass;

@property(nonatomic) MDCTextInputControllerBase *oldpasswordTextFieldController;
@property(nonatomic) MDCTextInputControllerOutlined *newpasswordTextFieldController;
@property(nonatomic) MDCTextInputControllerOutlined *confpasswordTextFieldController;

@property(nonatomic, strong) MDCSemanticColorScheme *colorScheme;
@property(nonatomic, strong) MDCTypographyScheme *typographyScheme;

@end

@implementation ChangePasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initAppBar];
    [self initForm];
}

-(void)initAppBar{
    self.view.tintColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Ganti Kata Sandi";
    _appBar = [[MDCAppBar alloc] init];
    [self addChildViewController:_appBar.headerViewController];
    
    self.appBar.navigationBar.tintColor = UIColor.whiteColor;
    self.appBar.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName : UIColor.whiteColor};
    self.appBar.headerViewController.headerView.backgroundColor = [UIColor colorWithHexString:primary_color_purple];
    self.appBar.headerViewController.headerView.tintColor = UIColor.whiteColor;
    [self.appBar addSubviewsToParent];
    
    UIImage *menuItemImage = [UIImage imageNamed:@"baseline_arrow_back_ios_white_24pt"];
    UIImage *templatedMenuItemImage = [menuItemImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIBarButtonItem *menuItem =
    [[UIBarButtonItem alloc] initWithImage:templatedMenuItemImage
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(actionBack:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

- (void)styleTextInputController:(id<MDCTextInputController>)controller {
    [MDCOutlinedTextFieldColorThemer applySemanticColorScheme:self.colorScheme
                                        toTextInputController:controller];
    [MDCTextFieldTypographyThemer applyTypographyScheme:self.typographyScheme
                                  toTextInputController:controller];
    [MDCTextFieldTypographyThemer applyTypographyScheme:self.typographyScheme
                                            toTextInput:controller.textInput];
}

-(void)initForm{
    
    self.oldpasswordTextFieldController =
    [[MDCTextInputControllerLegacyDefault alloc] initWithTextInput:self.tfOldPassword];
    self.oldpasswordTextFieldController.inlinePlaceholderColor = [UIColor colorWithHexString:primary_color_purple];
    self.oldpasswordTextFieldController.normalColor = [UIColor colorWithHexString:primary_color_purple];
    self.oldpasswordTextFieldController.activeColor = [UIColor colorWithHexString:primary_color_purple];
    self.oldpasswordTextFieldController.floatingPlaceholderActiveColor = [UIColor colorWithHexString:primary_color_purple];
    self.oldpasswordTextFieldController.placeholderText = @"Kata Sandi Lama";
    //[self styleTextInputController:self.oldpasswordTextFieldController];
    self.tfOldPassword.textColor = [UIColor darkGrayColor];
    self.tfOldPassword.leftViewMode = UITextFieldViewModeAlways;
    self.tfOldPassword.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_lock_black_24pt.png"]];
    
    self.newpasswordTextFieldController =
    [[MDCTextInputControllerOutlined alloc] initWithTextInput:self.tfNewPassword];
    self.newpasswordTextFieldController.inlinePlaceholderColor = [UIColor colorWithHexString:primary_color_purple];
    self.newpasswordTextFieldController.normalColor = [UIColor colorWithHexString:primary_color_purple];
    self.newpasswordTextFieldController.activeColor = [UIColor colorWithHexString:primary_color_purple];
    self.newpasswordTextFieldController.floatingPlaceholderActiveColor = [UIColor colorWithHexString:primary_color_purple];
    self.newpasswordTextFieldController.placeholderText = @"Kata Sandi Baru";
    self.tfNewPassword.textColor = [UIColor darkGrayColor];
    self.tfNewPassword.leftViewMode = UITextFieldViewModeAlways;
    self.tfNewPassword.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_lock_black_24pt.png"]];
    
    self.confpasswordTextFieldController =
    [[MDCTextInputControllerOutlined alloc] initWithTextInput:self.tfConfPassword];
    self.confpasswordTextFieldController.inlinePlaceholderColor = [UIColor colorWithHexString:primary_color_purple];
    self.confpasswordTextFieldController.normalColor = [UIColor colorWithHexString:primary_color_purple];
    self.confpasswordTextFieldController.activeColor = [UIColor colorWithHexString:primary_color_purple];
    self.confpasswordTextFieldController.floatingPlaceholderActiveColor = [UIColor colorWithHexString:primary_color_purple];
    self.confpasswordTextFieldController.placeholderText = @"Ulangi Kata Sandi";
    self.tfConfPassword.textColor = [UIColor darkGrayColor];
    self.tfConfPassword.leftViewMode = UITextFieldViewModeAlways;
    self.tfConfPassword.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_lock_black_24pt.png"]];
    
    self.buttonChangePass.layer.masksToBounds = TRUE;
    self.buttonChangePass.layer.cornerRadius = 27.5f;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)dismissKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.tfOldPassword) {
        [self.tfNewPassword becomeFirstResponder];
    }else if(textField == self.tfNewPassword){
        [self.tfConfPassword becomeFirstResponder];
    }else if(textField == self.tfConfPassword){
        [self.tfConfPassword becomeFirstResponder];
    }else{
        [self.tfConfPassword resignFirstResponder];
    }
    return YES;
}

- (IBAction)actionChangePass:(id)sender {
    NSString* OldPass    = self.tfNewPassword.text;
    NSString* ConfPass   = self.tfConfPassword.text;
    if([self validationFormChangePass]){
        if([OldPass isEqualToString:ConfPass]){
            [UIUtils showSnackbar:@"Ganti Kata Sandi" message:@"Kata sandi tidak cocok" alertType:ISAlertTypeWarning];
        }else{
            
        }
    }
}

-(BOOL)validationFormChangePass{
    NSString* NewPass    = self.tfOldPassword.text;
    NSString* OldPass    = self.tfNewPassword.text;
    NSString* ConfPass   = self.tfConfPassword.text;
    
    if([NewPass isEqualToString:@""]){
        
        //self.tfOldPassword.leading.textColor = UIColor.redColor;
        self.tfOldPassword.leadingUnderlineLabel.text = @"UNDERLINE";
        
        //self.oldpasswordTextFieldController.errorText = @"asdsadas";
//        [UIUtils showSnackbar:@"Ganti Kata Sandi" message:@"Masukan kata sandi lama" alertType:ISAlertTypeWarning];
        return FALSE;
    }else if([OldPass isEqualToString:@""]){
        [UIUtils showSnackbar:@"Ganti Kata Sandi" message:@"Masukan kata sandi baru" alertType:ISAlertTypeWarning];
        return FALSE;
    }else if([ConfPass isEqualToString:@""]){
        [UIUtils showSnackbar:@"Ganti Kata Sandi" message:@"Ulangi kata sandi" alertType:ISAlertTypeWarning];
        return FALSE;
    }else{
        return TRUE;
    }
    return TRUE;
}

@end

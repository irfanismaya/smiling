//  AccountInteractor.m
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ReferralInteractor.h"
#import "ReferralManagerAPI.h"

@interface ReferralInteractor ()

@property (nonatomic, strong) ReferralManagerAPI *referralManagerAPI;

@end

@implementation ReferralInteractor

- (instancetype)initWithReferralManager:(ReferralManagerAPI *)referralManagerAPI
{
    if ((self = [super init]))
    {
        _referralManagerAPI = referralManagerAPI;
    }
    
    return self;
}

#pragma mark - ReferralInteractorInput
- (void)getUserList:(NSDictionary *)params {
    __weak typeof(self) weakSelf = self;
    
    [self.referralManagerAPI getUserList:params completion:^(NSArray *listUser, NSError *error) {
        [weakSelf.referralPresenter didGetUserList:listUser error:error];
    }];
}

@end


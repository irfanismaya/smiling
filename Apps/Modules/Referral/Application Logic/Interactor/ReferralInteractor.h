//  ReferralInteractor.h
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ReferralModuleProtocol.h"
@class ReferralManagerAPI;

@interface ReferralInteractor : NSObject <ReferralInteractorInput>

- (instancetype)initWithReferralManager:(ReferralManagerAPI *)referralManagerAPI;

@property (nonatomic, weak) id <ReferralInteractorOutput> referralPresenter;

@end

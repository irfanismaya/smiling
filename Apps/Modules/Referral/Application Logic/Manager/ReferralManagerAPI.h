//  ReferalManagerAPI.h
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ReferralModuleProtocol.h"

@interface ReferralManagerAPI : NSObject <ReferralManagerAPIInput>

@end

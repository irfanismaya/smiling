//  ReferralPresenter.h
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ReferralModuleProtocol.h"
#import "ReferralWireframe.h"

@interface ReferralPresenter : NSObject <ReferralPresenterProtocol, ReferralInteractorOutput>

@property (nonatomic, strong) ReferralWireframe* referralWireframe;
@property (nonatomic, strong) UIViewController<ReferralViewProtocol> *referralController;

@property (nonatomic, strong) id <ReferralInteractorInput> referralInteractor;

@end

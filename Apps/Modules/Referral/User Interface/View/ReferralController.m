//  ReferralController.m
//  Smiling
//  Created by Irfan-Ismaya on 04/04/19.
//  Copyright © 2019 Merqurion Solution. All rights reserved.

#import "ReferralController.h"

@interface ReferralController ()
@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *internetstatus;
@property (weak, nonatomic) IBOutlet UITableView *tvReferral;
@property (weak, nonatomic) IBOutlet UISearchBar *searchReferral;
@end

@implementation ReferralController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callCheckInternet];
    [self checkReferralData:YES];
    [self initSearchBar];
}

-(void)initSearchBar{
    for (UIView *subview in self.searchBar.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
            break;
        }
        for (UIView *subsubview in subview.subviews) {
            if ([subsubview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [subsubview removeFromSuperview];
                break;
            }
        }
    }
    
    self.searchBar.barTintColor = [UIColor grayColor];
    self.searchBar.layer.borderColor = [UIColor grayColor].CGColor;
    self.searchBar.backgroundColor = [UIColor whiteColor];
    
    UITextField *searchField = [self.searchBar valueForKey:@"searchField"];
    searchField.backgroundColor = [UIColor colorWithHexString:@"#F6F9FF"];
    searchField.textColor = [UIColor grayColor];
    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Some Text"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor grayColor];
}

-(void)callCheckInternet{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    NSString *remoteHostName = @"https://www.google.com/";
    NSString *remoteHostLabelFormatString = NSLocalizedString(@"Remote Host: %@", @"Remote host label format string");
    
    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
    [self.hostReachability startNotifier];
    [self updateInterfaceWithReachability:self.hostReachability];
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [self updateInterfaceWithReachability:self.internetReachability];
}

-(void)checkInternetConnection:(BOOL)connectionstatus{
    if(connectionstatus){
        _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_no_internet_satellite"]];
        _imageView.frame = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2, 200, 200);
        _imageView.center = CGPointMake(self.view.frame.size.width/2,
                                        self.view.frame.size.height/2);
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        
        CGRect frame = CGRectMake(0, 0, 245, 20);
        _internetstatus = [[UILabel alloc] initWithFrame:frame];
        _internetstatus.center = CGPointMake(self.view.frame.size.width/2, self.view.bounds.size.height/1.5f);
        _internetstatus.text = @"Oops! No Internet Connection";
        _internetstatus.textAlignment = NSTextAlignmentJustified;
        _internetstatus.textColor = [UIColor grayColor];
        [_internetstatus setFont:[UIFont boldSystemFontOfSize:17]];
        
        [self.view addSubview:_imageView];
        [self.view addSubview:_internetstatus];
    }else{
        [_imageView removeFromSuperview];
        [_internetstatus removeFromSuperview];
    }
}

-(void)checkReferralData:(BOOL)datastatus{
    if(datastatus){
        _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_no_chat"]];
        _imageView.frame = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2, 200, 200);
        _imageView.center = CGPointMake(self.view.frame.size.width/2,
                                        self.view.frame.size.height/2);
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        
        CGRect frame = CGRectMake(0, 0, 245, 20);
        _internetstatus = [[UILabel alloc] initWithFrame:frame];
        _internetstatus.center = CGPointMake(self.view.frame.size.width/2, self.view.bounds.size.height/1.5f);
        _internetstatus.text = @"Oops! No message at this time";
        _internetstatus.textAlignment = NSTextAlignmentJustified;
        _internetstatus.textColor = [UIColor grayColor];
        [_internetstatus setFont:[UIFont boldSystemFontOfSize:17]];
        _tvReferral.hidden = YES;
        [self.view addSubview:_imageView];
        [self.view addSubview:_internetstatus];
    }else{
        _tvReferral.hidden = NO;
        [_imageView removeFromSuperview];
        [_internetstatus removeFromSuperview];
    }
}

- (void) reachabilityChanged:(NSNotification *)note{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}

- (void)updateInterfaceWithReachability:(Reachability *)reachability{
    if (reachability == self.hostReachability){
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        BOOL connectionRequired = [reachability connectionRequired];
        if (connectionRequired){
            
        }else{
            
        }
    }
    
    if (reachability == self.internetReachability){
        [self configureTextField:reachability];
    }
}

- (void)configureTextField:(Reachability *)reachability{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    BOOL connectionRequired = [reachability connectionRequired];
    NSString* statusString = @"";
    
    switch (netStatus){
        case NotReachable:{
            self.tvReferral.hidden = YES;
            self.searchReferral.hidden = YES;
            [self checkInternetConnection:YES];
            connectionRequired = NO;
            break;
        }case ReachableViaWWAN:{
            self.tvReferral.hidden = NO;
            self.searchReferral.hidden = NO;
            [self checkInternetConnection:NO];
            break;
        }case ReachableViaWiFi:{
            self.tvReferral.hidden = NO;
            self.searchReferral.hidden = NO;
            [self checkInternetConnection:NO];
            break;
        }
    }
    
    if (connectionRequired){
        NSString *connectionRequiredFormatString = NSLocalizedString(@"%@, Connection Required", @"Concatenation of status string with connection requirement");
        statusString= [NSString stringWithFormat:connectionRequiredFormatString, statusString];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

@end

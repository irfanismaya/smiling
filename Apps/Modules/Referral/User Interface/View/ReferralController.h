//  ReferralController.h
//  Smiling
//  Created by Irfan-Ismaya on 04/04/19.
//  Copyright © 2019 Merqurion Solution. All rights reserved.

#import "ReferralModuleProtocol.h"

@interface ReferralController : UIViewController <ReferralViewProtocol>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) id<ReferralPresenterProtocol> referralPresenter;
@end

//  ReferralWireframe.m
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ReferralWireframe.h"
#import "ReferralController.h"
#import "ReferralPresenter.h"
#import "RootWireframe.h"

#import "ReferralManagerAPI.h"
#import "ReferralInteractor.h"

static NSString *ReferralControllerIdentifier = @"ReferralController";

@interface ReferralWireframe ()

@property (nonatomic, strong) ReferralController *referralController;

@end

@implementation ReferralWireframe

- (void)presentReferralInterfaceFromViewController:(UIViewController *)viewController
{
    [self initWireframe];
    [viewController presentViewController:self.referralController animated:YES completion:nil];
}

- (UIViewController *)configuredViewController {
    [self initWireframe];
    return self.referralController;
}

- (void) initWireframe {
    self.referralController = [self viewControllerFromStoryboard:ReferralControllerIdentifier];
    
    ReferralManagerAPI *referralManagerAPI = [[ReferralManagerAPI alloc] init];
    ReferralInteractor *referralInteractor = [[ReferralInteractor alloc] initWithReferralManager:referralManagerAPI];
    
    self.referralPresenter = [[ReferralPresenter alloc] init];
    self.referralPresenter.referralWireframe = self;
    self.referralPresenter.referralInteractor = referralInteractor;
    self.referralPresenter.referralController = self.referralController;
    
    referralInteractor.referralPresenter = self.referralPresenter;
    self.referralController.referralPresenter = self.referralPresenter;
}

- (UIImage *)tabIcon {
    return [UIImage imageNamed:TAB_REFERRAL_ICON];
}

- (NSString *)tabTitle {
    return TAB_REFERRAL;
}

@end

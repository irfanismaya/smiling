//  ReferralWireframe.h
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "TabBarModuleProtocol.h"
#import "RootWireframe.h"
@class ReferralPresenter;

@interface ReferralWireframe : RootWireframe <TabBarViewProtocol>

@property (nonatomic, strong) ReferralPresenter *referralPresenter;

- (void)presentReferralInterfaceFromViewController:(UIViewController *)viewController;

@end

//  ReferralModuleInterface.h
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

//khusus buat presenter
@protocol ReferralPresenterProtocol <NSObject>
- (void)doGetUserList:(NSInteger)page;
@end

//khusus buat interactor
@protocol ReferralInteractorInput <NSObject>
- (void)getUserList:(NSDictionary *)params;
@end

@protocol ReferralInteractorOutput <NSObject>
- (void)didGetUserList:(NSArray *)listUser error:(NSError *)error;
@end

@protocol ReferralManagerAPIInput <NSObject>
- (void)getUserList:(NSDictionary *)params completion:(void (^)(NSArray *listUser, NSError *error))completion;
@end

//khusus buat view
@protocol ReferralViewProtocol <NSObject>
- (void)showLoading;
- (void)hideLoading;
@end

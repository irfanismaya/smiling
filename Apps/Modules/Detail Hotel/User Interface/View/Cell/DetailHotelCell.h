//  DetailHotelCell.h
//  Smiling
//  Created by Irfan-Ismaya on 01/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
@import GoogleMaps;
NS_ASSUME_NONNULL_BEGIN

@interface DetailHotelCell : UITableViewCell
+ (DetailHotelCell*) detailHotelCell;
@property (weak, nonatomic) IBOutlet GMSMapView *uiViewLocation;

@end

NS_ASSUME_NONNULL_END

//  SignupInteractor.m
//  Smiling
//  Created by Irfan-Ismaya on 16/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "SignupInteractor.h"
#import "SignupManagerAPI.h"

@interface SignupInteractor ()
@property (nonatomic, strong) SignupManagerAPI *signupManagerAPI;
@end

@implementation SignupInteractor
- (instancetype)initWithSignupManager:(SignupManagerAPI *)signupManagerAPI{
    if ((self = [super init]))
    {
        _signupManagerAPI = signupManagerAPI;
    }
    return self;
}

#pragma mark - SignupInteractorInput
- (void)signup:(NSDictionary *)params { 
    __weak typeof(self) weakSelf = self;
    [self.signupManagerAPI signup:params completion:^(Signup *signup, NSError *error) {
        [weakSelf.signupPresenter didSignup:signup error:error];
    }];
}

@end

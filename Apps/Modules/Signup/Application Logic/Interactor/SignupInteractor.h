//  SignupInteractor.h
//  Smiling
//  Created by Irfan-Ismaya on 16/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "SignupModuleProtocol.h"
@class SignupManagerAPI;

@interface SignupInteractor : NSObject <SignupInteractorInput>
- (instancetype)initWithSignupManager:(SignupManagerAPI *)signupManagerAPI;
@property (nonatomic, weak) id <SignupInteractorOutput> signupPresenter;
@end


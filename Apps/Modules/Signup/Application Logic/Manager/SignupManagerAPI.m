//  SignupManagerAPI.m
//  Smiling
//  Created by Irfan-Ismaya on 16/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "SignupManagerAPI.h"
#import "NetworkManager+Signup.h"

@interface SignupManagerAPI ()
@end

@implementation SignupManagerAPI
- (void)signup:(NSDictionary *)params completion:(void (^)(Signup *, NSError *))completion {
    [NetworkManager signup:params successBlock:^(Signup *signup) {
        completion(signup, nil);
    } failureBlock:^(NSError *error) {
        completion(nil, error);
    }];
}
@end

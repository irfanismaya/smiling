//  SignupWireframe.h
//  Smiling
//  Created by Irfan-Ismaya on 16/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "RootWireframe.h"
@class SignupPresenter;
@class TabBarWireframe;

@interface SignupWireframe : RootWireframe

@property (nonatomic, strong) SignupPresenter *signupPresenter;
@property (nonatomic, strong) TabBarWireframe *tabBarWireframe;

- (void)presentSignupInterfaceFromNavController:(UINavigationController *)navController;
- (void)goToHomeView;

@end


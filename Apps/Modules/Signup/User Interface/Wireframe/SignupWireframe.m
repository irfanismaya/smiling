//  SignupWireframe.m
//  Smiling
//  Created by Irfan-Ismaya on 16/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "SignupWireframe.h"
#import "SignUpController.h"
#import "SignupPresenter.h"
#import "RootWireframe.h"
#import "SignupManagerAPI.h"
#import "SignupInteractor.h"

#import "TabBarWireframe.h"

static NSString *SignupControllerIdentifier = @"SignUpController";
@interface SignupWireframe ()
@property (nonatomic, strong) SignUpController *signupController;
@end

@implementation SignupWireframe
- (void)presentSignupInterfaceFromNavController:(UINavigationController *)navController {
    self.signupController = [self viewControllerFromStoryboard:SignupControllerIdentifier];
    
    SignupManagerAPI *signupManagerAPI = [[SignupManagerAPI alloc] init];
    SignupInteractor *signupInteractor = [[SignupInteractor alloc] initWithSignupManager:signupManagerAPI];
    
    self.signupPresenter = [[SignupPresenter alloc] init];
    self.signupPresenter.signupWireframe = self;
    self.signupPresenter.signupInteractor = signupInteractor;
    self.signupPresenter.signupController = self.signupController;
    
    signupInteractor.signupPresenter = self.signupPresenter;
    self.signupController.signupPresenter = self.signupPresenter;
    
    self.tabBarWireframe = [[TabBarWireframe alloc] init];
    
    //menampilkan halaman signup
    [navController pushViewController:self.signupController animated:YES];
}

- (void)goToHomeView {
     [self.tabBarWireframe presentTabBarInterfaceFromNavController:self.signupController.navigationController];
}
@end

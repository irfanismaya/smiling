//  SignupPresenter.m
//  Smiling
//  Created by Irfan-Ismaya on 16/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "SignupPresenter.h"
#import "SignupWireframe.h"

@interface SignupPresenter ()
@end

@implementation SignupPresenter

-(BOOL)validationFormSignUp:(NSString *)firstName gender:(NSString *)gender username:(NSString *)username phone:(NSString *)phone email:(NSString *)email password:(NSString *)password confirmPass:(NSString *)confirmPass {
    if([firstName isEqualToString:@""]) {
        [UIUtils showSnackbar:REGISTER_TITLE message:FIRST_NAME_EMPTY alertType:ISAlertTypeWarning];
        return FALSE;
    } else if([gender isEqualToString:@""]) {
        [UIUtils showSnackbar:REGISTER_TITLE message:GENDER_EMPTY alertType:ISAlertTypeWarning];
        return FALSE;
    } else if([username isEqualToString:@""]) {
        [UIUtils showSnackbar:REGISTER_TITLE message:USERNAME_EMPTY alertType:ISAlertTypeWarning];
        return FALSE;
    } else if([phone isEqualToString:@""]) {
        [UIUtils showSnackbar:REGISTER_TITLE message:HANDPONE_EMPTY alertType:ISAlertTypeWarning];
        return FALSE;
    }else if([email isEqualToString:@""]){
        [UIUtils showSnackbar:REGISTER_TITLE message:EMAIL_EMPTY alertType:ISAlertTypeWarning];
        return FALSE;
    }else if([password isEqualToString:@""]){
        [UIUtils showSnackbar:REGISTER_TITLE message:PASSWORD_EMPTY alertType:ISAlertTypeWarning];
        return FALSE;
    }else if([confirmPass isEqualToString:@""]){
        [UIUtils showSnackbar:REGISTER_TITLE message:PASSWORD_CONFIRM_EMPTY alertType:ISAlertTypeWarning];
        return FALSE;
    }else{
        return TRUE;
    }
    return TRUE;
}

#pragma mark - SignupPresenterProtocol
- (void)doSignup:(NSString *)firstName lastName:(NSString *)lastName gender:(NSString *)gender username:(NSString *)username phone:(NSString *)phone email:(NSString *)email password:(NSString *)password confirmPass:(NSString *)confirmPass {
    if([self validationFormSignUp:firstName gender:gender username:username phone:phone email:email password:password confirmPass:confirmPass]) {
        NSDictionary *params = @{@"first_name":firstName,
                                 @"last_name":lastName,
                                 @"phone":phone,
                                 @"gender":gender,
                                 @"birth_date":@"1990-05-28",
                                 @"username":username,
                                 @"email":email,
                                 @"password":password
                                 };
        
        [self.signupController showLoading];
        [self.signupInteractor signup:params];
    }
}

#pragma mark - SignupInteractorOutput
- (void)didSignup:(Signup *)signup error:(NSError *)error { 
    if(signup != nil) {
        [self.signupController hideLoading];

        //pindah halaman ke home
        [self.signupWireframe goToHomeView];
    } else {
        [self.signupController hideLoading];
    }
}

@end

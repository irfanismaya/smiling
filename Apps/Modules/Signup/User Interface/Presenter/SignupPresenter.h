//  SignupPresenter.h
//  Smiling
//  Created by Irfan-Ismaya on 16/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "SignupModuleProtocol.h"
#import "SignupWireframe.h"

@interface SignupPresenter : NSObject <SignupPresenterProtocol, SignupInteractorOutput>
@property (nonatomic, strong) SignupWireframe* signupWireframe;
@property (nonatomic, strong) UIViewController<SignupViewProtocol> *signupController;
@property (nonatomic, strong) id <SignupInteractorInput> signupInteractor;
@end


//  SignUpController.h
//  Smiling
//  Created by Irfan-Ismaya on 08/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
#import <MaterialComponents/MaterialButtons.h>
#import <MaterialComponents/MaterialTextFields.h>
#import "MaterialButtons.h"
#import "SignupModuleProtocol.h"
#import "BaseViewProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface SignUpController : BaseViewController<BaseViewProtocol, SignupViewProtocol>

@property (weak, nonatomic) IBOutlet MDCRaisedButton *buttonRegister;
@property (weak, nonatomic) IBOutlet UIScrollView *uiScrollView;

@property (nonatomic, strong) id<SignupPresenterProtocol> signupPresenter;

- (IBAction)dateBirthDay:(id)sender;
- (IBAction)buttonRegister:(id)sender;

@end

NS_ASSUME_NONNULL_END

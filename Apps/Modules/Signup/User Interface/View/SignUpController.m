//  SignUpController.m
//  Smiling
//  Created by Irfan-Ismaya on 08/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "SignUpController.h"
#import <MaterialComponents/MaterialAppBar.h>
#import <MaterialTextField/MaterialTextField.h>
#import "Constants.h"
#import "NSDate+TCUtils.h"
#import "UIColor+HexString.h"
#import "LSLDatePickerDialog.h"
#import "ActionSheetStringPicker.h"
#import "ActionSheetDatePicker.h"
#import "ISMessages.h"

NSString *const ErrorDomainSignup = @"ErrorDomain";
NSInteger const ErrorDomainSignupCode = 100;

@interface SignUpController ()<UITextFieldDelegate, UITextViewDelegate>
@property(nonatomic, strong) MDCAppBar *appBar;
@property (nonatomic, strong) AbstractActionSheetPicker *actionSheetPicker;
@property (nonatomic, strong) NSDate *selectedDate;
@property (weak, nonatomic) IBOutlet MFTextField *tfName;
@property (weak, nonatomic) IBOutlet MFTextField *tfEmail;
@property (weak, nonatomic) IBOutlet MFTextField *tfPassword;
@property (weak, nonatomic) IBOutlet MFTextField *tfBirthday;
@property (weak, nonatomic) IBOutlet MFTextField *tfReferral;

@end

@implementation SignUpController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.selectedDate = [NSDate date];
    [self initDelegateField];
    [self initAppBar];
    [self initForm];
}

-(void)initDelegateField{
    self.tfName.delegate = self;
    self.tfEmail.delegate = self;
    self.tfPassword.delegate = self;
    self.tfBirthday.delegate = self;
    self.tfReferral.delegate = self;
}

-(void)initAppBar{
    self.view.tintColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Daftar";
    _appBar = [[MDCAppBar alloc] init];
    [self addChildViewController:_appBar.headerViewController];
    
    self.appBar.navigationBar.tintColor = UIColor.whiteColor;
    self.appBar.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName : UIColor.whiteColor};
    self.appBar.headerViewController.headerView.backgroundColor = [UIColor colorWithHexString:primary_color_purple];
    self.appBar.headerViewController.headerView.tintColor = UIColor.whiteColor;
    [self.appBar addSubviewsToParent];
    
    UIImage *menuItemImage = [UIImage imageNamed:@"baseline_arrow_back_ios_white_24pt"];
    UIImage *templatedMenuItemImage = [menuItemImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIBarButtonItem *menuItem =
    [[UIBarButtonItem alloc] initWithImage:templatedMenuItemImage
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(actionBack:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

-(void)initForm{
    UIImageView *lockImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_person_black_24pt.png"]];
    lockImage.bounds = CGRectMake(0, 0, 35, 35);
    lockImage.contentMode = UIViewContentModeLeft;
    self.tfName.tintColor = [UIColor mf_veryDarkGrayColor];
    self.tfName.textColor = [UIColor mf_veryDarkGrayColor];
    self.tfName.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.tfName.placeholderAnimatesOnFocus = YES;
    UIFontDescriptor * fontDescriptor = [self.tfName.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *font = [UIFont fontWithDescriptor:fontDescriptor size:self.tfName.font.pointSize];
    self.tfName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Nama" attributes:@{NSFontAttributeName:font}];
    self.tfName.leftViewMode = UITextFieldViewModeAlways;
    self.tfName.leftView = lockImage;
    
    UIImageView *lockImageEmail = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_email_black_24pt.png"]];
    lockImageEmail.bounds = CGRectMake(0, 0, 35, 35);
    lockImageEmail.contentMode = UIViewContentModeLeft;
    self.tfEmail.tintColor = [UIColor mf_veryDarkGrayColor];
    self.tfEmail.textColor = [UIColor mf_veryDarkGrayColor];
    self.tfEmail.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.tfEmail.placeholderAnimatesOnFocus = YES;
    UIFontDescriptor * fontDescriptorEmail = [self.tfEmail.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *fontEmail = [UIFont fontWithDescriptor:fontDescriptorEmail size:self.tfEmail.font.pointSize];
    self.tfEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Surat Elektronik" attributes:@{NSFontAttributeName:fontEmail}];
    self.tfEmail.leftViewMode = UITextFieldViewModeAlways;
    self.tfEmail.leftView = lockImageEmail;
    
    UIImageView *lockImagePass = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_lock_black_24pt.png"]];
    lockImagePass.bounds = CGRectMake(0, 0, 35, 35);
    lockImagePass.contentMode = UIViewContentModeLeft;
    self.tfPassword.tintColor = [UIColor mf_veryDarkGrayColor];
    self.tfPassword.textColor = [UIColor mf_veryDarkGrayColor];
    self.tfPassword.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.tfPassword.placeholderAnimatesOnFocus = YES;
    UIFontDescriptor * fontDescriptorPass = [self.tfPassword.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *fontPass = [UIFont fontWithDescriptor:fontDescriptorPass size:self.tfPassword.font.pointSize];
    self.tfPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Kata Sandi" attributes:@{NSFontAttributeName:fontPass}];
    self.tfPassword.leftViewMode = UITextFieldViewModeAlways;
    self.tfPassword.leftView = lockImagePass;
    
    UIImageView *lockImageBirthday = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_date_range_black_24pt.png"]];
    lockImageBirthday.bounds = CGRectMake(0, 0, 35, 35);
    lockImageBirthday.contentMode = UIViewContentModeLeft;
    self.tfBirthday.tintColor = [UIColor mf_veryDarkGrayColor];
    self.tfBirthday.textColor = [UIColor mf_veryDarkGrayColor];
    self.tfBirthday.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.tfBirthday.placeholderAnimatesOnFocus = YES;
    UIFontDescriptor * fontDescriptorBirth = [self.tfBirthday.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *fontBirth = [UIFont fontWithDescriptor:fontDescriptorBirth size:self.tfBirthday.font.pointSize];
    self.tfBirthday.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Tanggal Lahir" attributes:@{NSFontAttributeName:fontBirth}];
    self.tfBirthday.leftViewMode = UITextFieldViewModeAlways;
    self.tfBirthday.leftView = lockImageBirthday;
    
    UIImageView *lockImageRef = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_forum_black_24pt.png"]];
    lockImageRef.bounds = CGRectMake(0, 0, 35, 35);
    lockImageRef.contentMode = UIViewContentModeLeft;
    self.tfReferral.tintColor = [UIColor mf_veryDarkGrayColor];
    self.tfReferral.textColor = [UIColor mf_veryDarkGrayColor];
    self.tfReferral.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.tfReferral.placeholderAnimatesOnFocus = YES;
    UIFontDescriptor * fontDescriptorRef = [self.tfReferral.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *fontRef = [UIFont fontWithDescriptor:fontDescriptorRef size:self.tfReferral.font.pointSize];
    self.tfReferral.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Referral" attributes:@{NSFontAttributeName:fontRef}];
    self.tfReferral.leftViewMode = UITextFieldViewModeAlways;
    self.tfReferral.leftView = lockImageRef;
    
    self.buttonRegister.clipsToBounds = true;
    self.buttonRegister.layer.cornerRadius = 24;
    [self.uiScrollView setShowsVerticalScrollIndicator:NO];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)dismissKeyboard:(id)sender {
     [self.view endEditing:YES];
}

- (IBAction)textFieldDidChange:(UITextField *)textField{
    if (textField == self.tfName) {
        [self validateName];
    }else if (textField == self.tfEmail) {
        [self validateEmail];
    }else if (textField == self.tfPassword) {
        [self validateBirthday];
    }else if (textField == self.tfBirthday) {
        [self validatePassword];
    }else if (textField == self.tfReferral) {
        [self validateReferral];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.tfName) {
        [self.tfEmail becomeFirstResponder];
    }else if(textField == self.tfEmail){
        [self.tfPassword becomeFirstResponder];
    }else if(textField == self.tfPassword){
        [self.tfBirthday becomeFirstResponder];
    }else if(textField == self.tfBirthday){
        [self.tfReferral becomeFirstResponder];
    }else{
        [self.tfReferral becomeFirstResponder];
    }
    return YES;
}

#pragma mark - Text field validation
- (void)validateName{
    NSError *error = nil;
    if (![self textNameIsValid]) {
        error = [self errorWithLocalizedDescription:@"Name not valid"];
    }else{
        
    }
    [self.tfName setError:error animated:YES];
}

- (void)validateEmail{
    NSError *error = nil;
    NSString *email = self.tfEmail.text;
    if (![self textEmailIsValid:email]) {
        error = [self errorWithLocalizedDescription:@"Email not valid"];
    }else{
        
    }
    [self.tfEmail setError:error animated:YES];
}

- (void)validatePassword{
    NSError *error = nil;
    if (![self textPasswordIsValid]) {
        error = [self errorWithLocalizedDescription:@"The username must be at least 8 characters long and must contain at least one character and one number"];
    }else{
        
    }
    [self.tfPassword setError:error animated:YES];
}

- (void)validateBirthday{
    NSError *error = nil;
    if (![self textBirthIsValid]) {
        error = [self errorWithLocalizedDescription:@"Birthday not valid"];
    }else{
        [self.view endEditing:YES];
    }
    [self.tfBirthday setError:error animated:YES];
}

- (void)validateReferral{
    NSError *error = nil;
    if (![self textReferralIsValid]) {
        error = [self errorWithLocalizedDescription:@"Referral not valid"];
    }else{
        
    }
    [self.tfReferral setError:error animated:YES];
}

- (BOOL)textNameIsValid{
    return self.tfName.text.length >= 8;
}

- (BOOL)textEmailIsValid:(NSString*)email{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (BOOL)textPasswordIsValid{
    return self.tfPassword.text.length <= 8;
}

- (BOOL)textBirthIsValid{
    return self.tfBirthday.text.length <= 12;
}

- (BOOL)textReferralIsValid{
    return self.tfReferral.text.length <= 6;
}

- (IBAction)dateBirthDay:(id)sender {
    [self.view endEditing:YES];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *minimumDateComponents = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    [minimumDateComponents setYear:2000];
    NSDate *minDate = [calendar dateFromComponents:minimumDateComponents];
    NSDate *maxDate = [NSDate date];
    
    
    ActionSheetDatePicker *pickerdate = [[ActionSheetDatePicker alloc] initWithTitle:@"Pilih Tanggal Lahir" datePickerMode:UIDatePickerModeDate selectedDate:self.selectedDate
        target:self
        action:@selector(dateWasSelected:element:)
        origin:sender
    ];
    [pickerdate setMinimumDate:minDate];
    [pickerdate setMaximumDate:maxDate];
    pickerdate.hideCancel = NO;
    [pickerdate showActionSheetPicker];
}

- (IBAction)buttonRegister:(id)sender {
    if([self validationFormChangeProfile]){
        
    }
}

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    self.self.tfBirthday.text = [self.selectedDate description];
}

#pragma mark - LoginViewProtocol
- (void)showLoading {
    _tfName.hidden = YES;
    _tfBirthday.hidden = YES;
    _tfEmail.hidden = YES;
    _tfPassword.hidden = YES;
    _tfReferral.hidden = YES;
    _buttonRegister.hidden = YES;
    [super showLoading];
}

- (void)hideLoading {
    _tfName.hidden = YES;
    _tfBirthday.hidden = YES;
    _tfEmail.hidden = YES;
    _tfPassword.hidden = YES;
    _tfReferral.hidden = YES;
    _buttonRegister.hidden = NO;
    [super hideLoading];
}

- (NSError *)errorWithLocalizedDescription:(NSString *)localizedDescription{
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: localizedDescription};
    return [NSError errorWithDomain:ErrorDomainSignup code:ErrorDomainSignupCode userInfo:userInfo];
}

-(BOOL)validationFormChangeProfile{
    NSString* Name         = self.tfName.text;
    NSString* Email        = self.tfEmail.text;
    NSString* Password     = self.tfPassword.text;
    NSString* Birthday     = self.tfBirthday.text;
    NSString* Refferal     = self.tfReferral.text;
    
    if([Name isEqualToString:@""] && [Email isEqualToString:@""] && [Password isEqualToString:@""] && [Birthday isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfName setError:errorName animated:YES];
        
        NSError *errorEmail = nil;
        errorEmail = [self errorWithLocalizedDescription:@"Email field is required"];
        [self.tfEmail setError:errorEmail animated:YES];
        
        NSError *errorPassword = nil;
        errorPassword = [self errorWithLocalizedDescription:@"Password field is required"];
        [self.tfPassword setError:errorPassword animated:YES];
        
        NSError *errorBirthday = nil;
        errorBirthday = [self errorWithLocalizedDescription:@"Birthday field is required"];
        [self.tfBirthday setError:errorBirthday animated:YES];
        
        return FALSE;
    }else if([Email isEqualToString:@""] && [Password isEqualToString:@""] && [Birthday isEqualToString:@""]){
        NSError *errorEmail = nil;
        errorEmail = [self errorWithLocalizedDescription:@"Email field is required"];
        [self.tfEmail setError:errorEmail animated:YES];
        
        NSError *errorPassword = nil;
        errorPassword = [self errorWithLocalizedDescription:@"Password field is required"];
        [self.tfPassword setError:errorPassword animated:YES];
        
        NSError *errorBirthday = nil;
        errorBirthday = [self errorWithLocalizedDescription:@"Birthday field is required"];
        [self.tfBirthday setError:errorBirthday animated:YES];
        
        return FALSE;
    }else if([Name isEqualToString:@""] && [Email isEqualToString:@""] && [Password isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfName setError:errorName animated:YES];
        
        NSError *errorEmail = nil;
        errorEmail = [self errorWithLocalizedDescription:@"Email field is required"];
        [self.tfEmail setError:errorEmail animated:YES];
        
        NSError *errorPassword = nil;
        errorPassword = [self errorWithLocalizedDescription:@"Password field is required"];
        [self.tfPassword setError:errorPassword animated:YES];
        
        return FALSE;
    }else if([Name isEqualToString:@""] && [Password isEqualToString:@""] && [Birthday isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfName setError:errorName animated:YES];
        
        NSError *errorPassword = nil;
        errorPassword = [self errorWithLocalizedDescription:@"Password field is required"];
        [self.tfPassword setError:errorPassword animated:YES];
        
        NSError *errorBirthday = nil;
        errorBirthday = [self errorWithLocalizedDescription:@"Birthday field is required"];
        [self.tfBirthday setError:errorBirthday animated:YES];
        
        return FALSE;
    }else if([Name isEqualToString:@""] && [Email isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfName setError:errorName animated:YES];
        
        NSError *errorEmail = nil;
        errorEmail = [self errorWithLocalizedDescription:@"Email field is required"];
        [self.tfEmail setError:errorEmail animated:YES];
        
        return FALSE;
    }else if([Password isEqualToString:@""] && [Birthday isEqualToString:@""]){
        NSError *errorPassword = nil;
        errorPassword = [self errorWithLocalizedDescription:@"Password field is required"];
        [self.tfPassword setError:errorPassword animated:YES];
        
        NSError *errorBirthday = nil;
        errorBirthday = [self errorWithLocalizedDescription:@"Birthday field is required"];
        [self.tfBirthday setError:errorBirthday animated:YES];
        
        return FALSE;
    }else if([Name isEqualToString:@""] && [Password isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfName setError:errorName animated:YES];
        
        NSError *errorPassword = nil;
        errorPassword = [self errorWithLocalizedDescription:@"Password field is required"];
        [self.tfPassword setError:errorPassword animated:YES];
        
        return FALSE;
    }else if([Name isEqualToString:@""] && [Birthday isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfName setError:errorName animated:YES];
        
        NSError *errorBirthday = nil;
        errorBirthday = [self errorWithLocalizedDescription:@"Birthday field is required"];
        [self.tfBirthday setError:errorBirthday animated:YES];
        
        return FALSE;
    }else if([Email isEqualToString:@""] && [Password isEqualToString:@""]){
        NSError *errorEmail = nil;
        errorEmail = [self errorWithLocalizedDescription:@"Email field is required"];
        [self.tfEmail setError:errorEmail animated:YES];
        
        NSError *errorPassword = nil;
        errorPassword = [self errorWithLocalizedDescription:@"Password field is required"];
        [self.tfPassword setError:errorPassword animated:YES];
        
        return FALSE;
    }else if([Name isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Name field is required"];
        [self.tfName setError:errorName animated:YES];
        
        return FALSE;
    }else if([Email isEqualToString:@""]){
        NSError *errorEmail = nil;
        errorEmail = [self errorWithLocalizedDescription:@"Email field is required"];
        [self.tfEmail setError:errorEmail animated:YES];
        
        return FALSE;
    }else if([Password isEqualToString:@""]){
        NSError *errorPassword = nil;
        errorPassword = [self errorWithLocalizedDescription:@"Password field is required"];
        [self.tfPassword setError:errorPassword animated:YES];
        
        return FALSE;
    }else if([Birthday isEqualToString:@""]){
        NSError *errorBirthday = nil;
        errorBirthday = [self errorWithLocalizedDescription:@"Birthday field is required"];
        [self.tfBirthday setError:errorBirthday animated:YES];
        
        return FALSE;
    }else{
        return TRUE;
    }
    return TRUE;
}

@end

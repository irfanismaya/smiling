//  SignupModuleProtocol.h
//  Smiling
//  Created by Irfan-Ismaya on 16/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "Signup.h"

//khusus buat presenter
@protocol SignupPresenterProtocol <NSObject>
- (void)doSignup:(NSString *)firstName lastName:(NSString *)lastName gender:(NSString *)gender username:(NSString *)username phone:(NSString *)phone email:(NSString *)email password:(NSString *)password confirmPass:(NSString *)confirmPass;
@end

//khusus buat interactor
@protocol SignupInteractorInput <NSObject>
- (void)signup:(NSDictionary *)params;
@end

@protocol SignupInteractorOutput <NSObject>
- (void)didSignup:(Signup *)signup error:(NSError *)error;
@end

@protocol SignupManagerAPIInput <NSObject>
- (void)signup:(NSDictionary *)params completion:(void (^)(Signup *signup, NSError *error))completion;
@end

//khusus buat view
@protocol SignupViewProtocol <NSObject>
- (void)showLoading;
- (void)hideLoading;
@end

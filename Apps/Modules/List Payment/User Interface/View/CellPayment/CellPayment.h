//  CellPayment.h
//  Smiling
//  Created by Irfan-Ismaya on 05/06/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
#import "TNCircularCheckBox.h"

@interface CellPayment : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *uiView;
@property (weak, nonatomic) IBOutlet TNCircularCheckBox *checkBox;
@property (weak, nonatomic) IBOutlet UIImageView *imgPayment;
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;
@property (weak, nonatomic) IBOutlet UIImageView *imgDelete;
@end

//  ListPaymentController.m
//  Smiling
//  Created by Irfan-Ismaya on 05/06/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ListPaymentController.h"
#import <MaterialComponents/MaterialAppBar.h>
#import "CellPayment.h"

@interface ListPaymentController ()<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tvPayment;
@property(nonatomic, strong) MDCAppBar *appBar;
@end

@implementation ListPaymentController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initAppBar];
}

-(void)initAppBar{
    self.view.tintColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Your Payment Method";
    _appBar = [[MDCAppBar alloc] init];
    [self addChildViewController:_appBar.headerViewController];
    
    self.appBar.navigationBar.tintColor = UIColor.whiteColor;
    self.appBar.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName : UIColor.whiteColor};
    self.appBar.headerViewController.headerView.backgroundColor = [UIColor colorWithHexString:primary_color_purple];
    self.appBar.headerViewController.headerView.tintColor = UIColor.whiteColor;
    [self.appBar addSubviewsToParent];
    
    UIImage *menuItemImage = [UIImage imageNamed:@"baseline_arrow_back_ios_white_24pt"];
    UIImage *templatedMenuItemImage = [menuItemImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIBarButtonItem *menuItem =
    [[UIBarButtonItem alloc] initWithImage:templatedMenuItemImage
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(actionBack:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"CellPayment";
    CellPayment *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        UINib *cellNib = [UINib nibWithNibName:cellIdentifier bundle:nil];
        [self.tvPayment registerNib:cellNib forCellReuseIdentifier:cellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.uiView.layer.cornerRadius = 6.0f;
    cell.uiView.layer.borderWidth = 1.0f;
    cell.uiView.layer.borderColor = [UIColor grayColor].CGColor;
    cell.imgPayment.layer.cornerRadius = 6.0f;
    cell.checkBox.layer.borderColor = [UIColor blueColor].CGColor;
    return cell;
}



@end

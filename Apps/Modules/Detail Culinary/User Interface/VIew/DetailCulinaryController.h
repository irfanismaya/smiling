//  DetailCulinaryController.h
//  Smiling
//  Created by Irfan-Ismaya on 01/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
#import "TGFoursquareLocationDetail.h"
#import "DetailCulinaryCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailCulinaryController : UIViewController<UITableViewDataSource,UITableViewDelegate,TGFoursquareLocationDetailDelegate, SliderImagePagerDelegate, SliderImagePagerDataSource>
@property (nonatomic, strong) TGFoursquareLocationDetail *locationDetail;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitle;

@end

NS_ASSUME_NONNULL_END

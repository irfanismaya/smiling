//
//  DetailTourismModuleProtocol.h
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "Article.h"

//khusus buat presenter
@protocol DetailTourismPresenterProtocol <NSObject>
- (void)doGetArticleDetail:(NSInteger)articleId;
@end

//khusus buat interactor
@protocol DetailTourismInteractorInput <NSObject>
- (void)getArticleDetail:(NSInteger)articleId;
@end

@protocol DetailTourismInteractorOutput <NSObject>
- (void)didGetArticleDetail:(Article *)article error:(NSError *)error;
@end

@protocol DetailTourismManagerAPIInput <NSObject>
- (void)getArticleDetail:(NSInteger)articleId completion:(void (^)(Article *article, NSError *error))completion;
@end

//khusus buat view
@protocol DetailTourismViewProtocol <NSObject>
- (void)showLoading;
- (void)hideLoading;
- (void)setArticle:(Article *)article;
@end


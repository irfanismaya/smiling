//
//  DetailTourismManagerAPI.h
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "DetailTourismModuleProtocol.h"
@interface DetailTourismManagerAPI : NSObject<DetailTourismManagerAPIInput>
@end

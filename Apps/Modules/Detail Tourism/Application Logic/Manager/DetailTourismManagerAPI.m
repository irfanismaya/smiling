//
//  DetailTourismManagerAPI.m
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "DetailTourismManagerAPI.h"
#import "NetworkManager+Article.h"

@interface DetailTourismManagerAPI ()
@end

@implementation DetailTourismManagerAPI
- (void)getArticleDetail:(NSInteger)articleId completion:(void (^)(NSArray *, NSError *))completion {
    [NetworkManager getArticleDetail:articleId successBlock:^(Article *article) {
        completion(article, nil);
    } failureBlock:^(NSError *error) {
        completion(nil, error);
    }];
}
@end

//
//  DetailTourismInteractor.h
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "DetailTourismModuleProtocol.h"
@class DetailTourismManagerAPI;

@interface DetailTourismInteractor : NSObject <DetailTourismInteractorInput>
- (instancetype)initWithDetailTourismManager:(DetailTourismManagerAPI *)detailTourismManagerAPI;
@property (nonatomic, weak) id <DetailTourismInteractorOutput> detailTourismPresenter;
@end

//
//  DetailTourismInteractor.m
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "DetailTourismInteractor.h"
#import "DetailTourismManagerAPI.h"

#import "Article.h"

@interface DetailTourismInteractor ()
@property (nonatomic, strong) DetailTourismManagerAPI *detailTourismManagerAPI;
@end

@implementation DetailTourismInteractor
- (instancetype)initWithDetailTourismManager:(DetailTourismManagerAPI *)detailTourismManagerAPI{
    if ((self = [super init]))
    {
        _detailTourismManagerAPI = detailTourismManagerAPI;
    }
    return self;
}

#pragma mark - DetailTourismInteractorInput
- (void)getArticleDetail:(NSInteger)articleId {
    __weak typeof(self) weakSelf = self;
    [self.detailTourismManagerAPI getArticleDetail:articleId completion:^(Article *article, NSError *error) {
        [weakSelf.detailTourismPresenter didGetArticleDetail:article error:error];
    }];
}

@end

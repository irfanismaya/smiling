//
//  DetailTourismWireframe.h
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "RootWireframe.h"
#import "Article.h"
@class DetailTourismPresenter;

@interface DetailTourismWireframe : RootWireframe

@property (nonatomic, strong) DetailTourismPresenter *detailTourismPresenter;

- (void)presentDetailTourismInterfaceFromNavController:(UINavigationController *)navController articleId:(NSInteger)articleId;
//- (void)goToHomeView;

@end

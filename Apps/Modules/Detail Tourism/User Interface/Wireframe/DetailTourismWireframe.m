//
//  DetailTourismWireframe.m
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "DetailTourismWireframe.h"
#import "DetailTourismController.h"
#import "DetailTourismPresenter.h"
#import "RootWireframe.h"
#import "DetailTourismManagerAPI.h"
#import "DetailTourismInteractor.h"

static NSString *DetailTourismControllerIdentifier = @"DetailTourismController";
@interface DetailTourismWireframe ()
@property (nonatomic, strong) DetailTourismController *detailTourismController;
@end

@implementation DetailTourismWireframe
- (void)presentDetailTourismInterfaceFromNavController:(UINavigationController *)navController articleId:(NSInteger)articleId {
    self.detailTourismController = [self viewControllerFromStoryboard:DetailTourismControllerIdentifier];
    self.detailTourismController.articleId = articleId;
    
    DetailTourismManagerAPI *detailTourismManagerAPI = [[DetailTourismManagerAPI alloc] init];
    DetailTourismInteractor *detailTourismInteractor = [[DetailTourismInteractor alloc] initWithDetailTourismManager:detailTourismManagerAPI];
    
    self.detailTourismPresenter = [[DetailTourismPresenter alloc] init];
    self.detailTourismPresenter.detailTourismWireframe = self;
    self.detailTourismPresenter.detailTourismInteractor = detailTourismInteractor;
    self.detailTourismPresenter.detailTourismController = self.detailTourismController;
    
    detailTourismInteractor.detailTourismPresenter = self.detailTourismPresenter;
    self.detailTourismController.detailTourismPresenter = self.detailTourismPresenter;
    
    //menampilkan halaman tourism
    [navController pushViewController:self.detailTourismController animated:YES];
}

//- (void)goToHomeView {
//    [self.tabBarWireframe presentTabBarInterfaceFromViewController:self.signupController];
//}
@end

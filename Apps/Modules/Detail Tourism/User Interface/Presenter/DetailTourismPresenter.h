//
//  DetailTourismPresenter.h
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "DetailTourismModuleProtocol.h"
#import "DetailTourismWireframe.h"

@interface DetailTourismPresenter : NSObject <DetailTourismPresenterProtocol, DetailTourismInteractorOutput>
@property (nonatomic, strong) DetailTourismWireframe* detailTourismWireframe;
@property (nonatomic, strong) UIViewController<DetailTourismViewProtocol> *detailTourismController;
@property (nonatomic, strong) id <DetailTourismInteractorInput> detailTourismInteractor;
@end

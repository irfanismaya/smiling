//
//  DetailTourismPresenter.m
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "DetailTourismPresenter.h"
#import "DetailTourismWireframe.h"

@interface DetailTourismPresenter ()
@end

@implementation DetailTourismPresenter

#pragma mark - DetailTourismPresenterProtocol
- (void)doGetArticleDetail:(NSInteger)articleId {
    [self.detailTourismController showLoading];
    [self.detailTourismInteractor getArticleDetail:articleId];
}

#pragma mark - DetailTourismInteractorOutput
- (void)didGetArticleDetail:(Article *)article error:(NSError *)error {
    [self.detailTourismController setArticle:article];
    [self.detailTourismController hideLoading];
}

@end

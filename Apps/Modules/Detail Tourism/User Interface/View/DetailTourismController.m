//  DetailTourismController.m
//  Smiling
//  Created by Irfan-Ismaya on 11/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "DetailTourismController.h"
#import "Constants.h"
#import "UIColor+HexString.h"
#import "TourismCollectionCell.h"
#import <MaterialComponents/MaterialAppBar.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <KSPhotoBrowser/KSSDImageManager.h>
#import <KSPhotoBrowser/KSPhotoBrowser.h>

@interface DetailTourismController ()
@property(nonatomic, strong) MDCAppBar *appBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgHeader;
@property (weak, nonatomic) IBOutlet UIScrollView *uiScrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSMutableArray *urls;
@end
NSArray *imgGalleryDetail;
NSArray *imgGalleryTourism;
NSArray *imgTourismK;

@implementation DetailTourismController

- (void)viewDidLoad {
    [super viewDidLoad];
    imgGalleryDetail = [[NSArray alloc] initWithObjects:
                  [UIImage imageNamed:@"bg1@2x.png"],
                  [UIImage imageNamed:@"bg2@2x.png"],
                  [UIImage imageNamed:@"bg3@2x.png"],
                  [UIImage imageNamed:@"bg4@2x.png"],nil];
    imgGalleryDetail = @[@"http://4.bp.blogspot.com/-5gdE_6VQkSw/T5hsKVd9UMI/AAAAAAAABVY/i7MDRf5z7wA/s1600/maya-ubud-bali-wooden-bridge-to-the-spa.jpg",@"http://4.bp.blogspot.com/-5gdE_6VQkSw/T5hsKVd9UMI/AAAAAAAABVY/i7MDRf5z7wA/s1600/maya-ubud-bali-wooden-bridge-to-the-spa.jpg",@"http://4.bp.blogspot.com/-5gdE_6VQkSw/T5hsKVd9UMI/AAAAAAAABVY/i7MDRf5z7wA/s1600/maya-ubud-bali-wooden-bridge-to-the-spa.jpg",@"http://4.bp.blogspot.com/-5gdE_6VQkSw/T5hsKVd9UMI/AAAAAAAABVY/i7MDRf5z7wA/s1600/maya-ubud-bali-wooden-bridge-to-the-spa.jpg"];
    
    _urls = @[].mutableCopy;
    for (int i = 0; i < imgGalleryDetail.count; i++) {
        [_urls addObjectsFromArray:imgGalleryDetail];
    }
    
     imgTourismK = @[@"https://tempatwisataseru.com/wp-content/uploads/2015/10/Curug-Cikaso.jpg", @"https://v-images2.antarafoto.com/festival-seni-budaya-jawa-barat-oyl651-prv.jpg", @"http://alienco.net/wp-content/uploads/2016/01/hiburan-rakyat-jawa-barat-kuda-lumping-640x402.jpg", @"https://media.suara.com/pictures/653x366/2018/12/14/37976-kemenpar.jpg"];
    
    [self initAppBar];
    [self initView];
    [self initLocation];
    
    [_detailTourismPresenter doGetArticleDetail:_articleId];
}

-(void)initView{
    [self.uiCollectionGallery setShowsHorizontalScrollIndicator:NO];
    self.uiViewLoc.clipsToBounds = true;
    self.uiViewLoc.layer.cornerRadius = 4;
    
    self.uiViewValueRating.clipsToBounds = true;
    self.uiViewValueRating.layer.cornerRadius = 4;
    
    self.uiViewParent.clipsToBounds = true;
    self.uiViewParent.layer.cornerRadius = 27.5;
    
    self.uiViewCenter.clipsToBounds = true;
    self.uiViewCenter.layer.cornerRadius = 27.5;
    self.uiViewCenter.layer.maskedCorners = kCALayerMinXMinYCorner | kCALayerMaxXMinYCorner | kCALayerMaxXMaxYCorner;
    
    [self.imgHeader sd_setImageWithURL:[NSURL URLWithString:@"https://i2.wp.com/www.urbandung.com/wp-content/uploads/2016/04/rumah-hobbit-bandung.jpg?fit=1500%2C845&ssl=1"]
                          placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
}

-(void)initLocation{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.868
                                                            longitude:151.2086
                                                                 zoom:6];
    [self.uiViewLoc animateToCameraPosition:camera];
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = camera.target;
    marker.snippet = @"Smiling Jawa Barat";
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = self.uiViewLoc;
}

-(void)initAppBar{
    self.view.tintColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Maribaya";
    _appBar = [[MDCAppBar alloc] init];
    [self addChildViewController:_appBar.headerViewController];
    
    self.appBar.navigationBar.tintColor = UIColor.whiteColor;
    self.appBar.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName : UIColor.whiteColor};
    self.appBar.headerViewController.headerView.backgroundColor = [UIColor colorWithHexString:primary_color_purple];
    self.appBar.headerViewController.headerView.tintColor = UIColor.whiteColor;
    [self.appBar addSubviewsToParent];
    
    UIImage *menuItemImage = [UIImage imageNamed:@"baseline_arrow_back_ios_white_24pt"];
    UIImage *templatedMenuItemImage = [menuItemImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIBarButtonItem *menuItem =
    [[UIBarButtonItem alloc] initWithImage:templatedMenuItemImage
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(actionBack:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showBrowserWithPhotoItems:(NSArray *)items selectedIndex:(NSUInteger)selectedIndex {
    self.items = items;
    KSPhotoBrowser *browser = [KSPhotoBrowser browserWithPhotoItems:items selectedIndex:selectedIndex];
    browser.delegate = self;
    browser.dismissalStyle = KSPhotoBrowserInteractiveDismissalStyleSlide;
    browser.backgroundStyle = KSPhotoBrowserBackgroundStyleBlur;
    browser.loadingStyle = KSPhotoBrowserPageIndicatorStyleDot;
    browser.pageindicatorStyle = KSPhotoBrowserImageLoadingStyleDeterminate;
    browser.bounces = YES;
    [browser showFromViewController:self];
}

#pragma mark - KSPhotoBrowserDelegate
- (void)ks_photoBrowser:(KSPhotoBrowser *)browser didSelectItem:(KSPhotoItem *)item atIndex:(NSUInteger)index {
    NSLog(@"selected index: %ld", index);
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return imgGalleryDetail.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TourismCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TourismCollectionCell" forIndexPath:indexPath];
    [cell.imageContent sd_setImageWithURL:[NSURL URLWithString:imgTourismK[indexPath.row]]
                         placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
    cell.imageContent.layer.cornerRadius = 4;
    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *items = @[].mutableCopy;
    for (int i = 0; i < _urls.count; i++) {
        TourismCollectionCell *cell = (TourismCollectionCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        NSString *url = [_urls[i] stringByReplacingOccurrencesOfString:@"thumbnail" withString:@"bmiddle"];
        KSPhotoItem *item = [KSPhotoItem itemWithSourceView:cell.imageContent imageUrl:[NSURL URLWithString:url]];
        [items addObject:item];
    }
    [self showBrowserWithPhotoItems:items selectedIndex:indexPath.item];
}

#pragma mark - HomeViewProtocol
- (void)showLoading {
    _uiScrollView.hidden = YES;
    [super showLoading];
}

- (void)hideLoading {
    _uiScrollView.hidden = NO;
    [super hideLoading];
}

-(void)setArticle:(Article *)article {
    [_imgHeader sd_setImageWithURL:[NSURL URLWithString:[StringUtils replaceSpaceFromUrl:article.coverImage]]
                         placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
    
    _lblTitle.text = article.title;
    
    if(![article.desc isEqualToString:@""]) {
        NSMutableAttributedString *attributedString =
        [[NSMutableAttributedString alloc]initWithData: [article.desc dataUsingEncoding:NSUnicodeStringEncoding]
                                               options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                    documentAttributes: nil
                                                 error: nil];
        
        [_lblDesc setAttributedText:attributedString];
    } else {
        _lblDesc.text = @"";
    }
}

@end

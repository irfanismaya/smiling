//  DetailTourismController.h
//  Smiling
//  Created by Irfan-Ismaya on 11/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
#import <MaterialComponents/MaterialButtons.h>
#import <MaterialComponents/MaterialTextFields.h>
#import "MaterialButtons.h"
#import "DetailTourismModuleProtocol.h"
#import "BaseViewProtocol.h"
#import "KSPhotoBrowser.h"
@import GoogleMaps;

NS_ASSUME_NONNULL_BEGIN

@interface DetailTourismController : BaseViewController<BaseViewProtocol, DetailTourismViewProtocol, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, KSPhotoBrowserDelegate>
@property (weak, nonatomic) IBOutlet UIView *uiViewParent;
@property (weak, nonatomic) IBOutlet UIView *uiViewCenter;
@property (weak, nonatomic) IBOutlet UIView *uiViewValueRating;
@property (weak, nonatomic) IBOutlet GMSMapView *uiViewLoc;
@property (weak, nonatomic) IBOutlet UICollectionView *uiCollectionGallery;

@property (nonatomic, strong) id<DetailTourismPresenterProtocol> detailTourismPresenter;

@property (nonatomic) NSInteger articleId;

@end

NS_ASSUME_NONNULL_END

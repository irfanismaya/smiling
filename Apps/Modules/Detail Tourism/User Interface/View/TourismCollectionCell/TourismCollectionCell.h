//  TourismCollectionCell.h
//  Smiling
//  Created by Irfan-Ismaya on 12/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TourismCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageContent;
@end

NS_ASSUME_NONNULL_END

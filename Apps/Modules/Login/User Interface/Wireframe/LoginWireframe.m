//  LoginWireframe.m
//  Smiling
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "LoginWireframe.h"
#import "LoginController.h"
#import "LoginPresenter.h"
#import "RootWireframe.h"
#import "TabBarWireframe.h"

#import "LoginManagerAPI.h"
#import "LoginInteractor.h"

#import "SignupWireframe.h"

static NSString *LoginControllerIdentifier = @"LoginController";

@interface LoginWireframe ()

@property (nonatomic, strong) LoginController *loginController;

@end

@implementation LoginWireframe

- (void)presentLoginInterfaceFromWindow:(UIWindow *)window
{
    self.loginController = [self viewControllerFromStoryboard:LoginControllerIdentifier];

    LoginManagerAPI *loginManagerAPI = [[LoginManagerAPI alloc] init];
    LoginInteractor *loginInteractor = [[LoginInteractor alloc] initWithLoginManager:loginManagerAPI];
    
    self.loginPresenter = [[LoginPresenter alloc] init];
    self.loginPresenter.loginWireframe = self;
    self.loginPresenter.loginInteractor = loginInteractor;
    self.loginPresenter.loginController = self.loginController;
    
    loginInteractor.loginPresenter = self.loginPresenter;
    self.loginController.loginPresenter = self.loginPresenter;
    
    self.tabBarWireframe = [[TabBarWireframe alloc] init];
    self.signupWireframe = [[SignupWireframe alloc] init];
    
    //menampilkan halaman login
    [self showRootViewController:self.loginController
                                      inWindow:window];
}

- (void)goToHomeView {
    [self.tabBarWireframe presentTabBarInterfaceFromNavController:self.loginController.navigationController];
}

- (void)goToSignup{
    [self.signupWireframe presentSignupInterfaceFromNavController:self.loginController.navigationController];
}

@end

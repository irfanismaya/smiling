//  LoginWireframe.h
//  Smiling
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "RootWireframe.h"
@class LoginPresenter;
@class TabBarWireframe;
@class SignupWireframe;

@interface LoginWireframe : RootWireframe

@property (nonatomic, strong) LoginPresenter *loginPresenter;
@property (nonatomic, strong) TabBarWireframe *tabBarWireframe;
@property (nonatomic, strong) SignupWireframe *signupWireframe;

- (void)presentLoginInterfaceFromWindow:(UIWindow *)window;
- (void)goToHomeView;
- (void)goToSignup;

@end

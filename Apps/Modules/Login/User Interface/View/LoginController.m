//  LoginController.m
//  Smiling
//  Created by Irfan-Ismaya on 08/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "LoginController.h"
#import "NSUserDefaultsApp.h"
#import "MDCCard.h"
#import "NSUserDefaultsApp.h"
#import "Constants.h"
#import "MaterialButtons.h"
#import "MPCheckBoxDelegate.h"
#import "BIZPopupViewController.h"
#import <EAIntroView/EAIntroView.h>
#import <SMPageControl/SMPageControl.h>
#import "MaterialActivityIndicator.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <MaterialTextField/MaterialTextField.h>

#define GoogleClientID @"472558079792-36jbjabuiqva6o0q7a8523098o2url44.apps.googleusercontent.com"
static NSString * const sampleDescription1 = @"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
static NSString * const sampleDescription2 = @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.";
static NSString * const sampleDescription3 = @"Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.";
static NSString * const sampleDescription4 = @"Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit.";

static NSString *segueLogin = @"segueLogin";
static BOOL loginResult = NO;
BOOL skip = YES;

NSString *const ErrorDomainLogin = @"ErrorDomain";
NSInteger const ErrorDomainLoginCode = 100;

@interface LoginController ()<EAIntroDelegate> {
    UIView *rootView;
    EAIntroView *_intro;
}

@property (weak, nonatomic) IBOutlet MDCCard *carduiview;
@property (weak, nonatomic) IBOutlet MPCheckBox *remember;
@property (weak, nonatomic) IBOutlet MDCRaisedButton *buttonLogin;
@property (weak, nonatomic) IBOutlet UILabel *lblremember;
@property (weak, nonatomic) IBOutlet MFTextField *username;
@property (weak, nonatomic) IBOutlet MFTextField *password;
@property (weak, nonatomic) IBOutlet GIDSignInButton *signInButton;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet FBSDKButton *btnLoginFB;
@property (weak, nonatomic) IBOutlet UIImageView *uiImageBackground;

@property (nonatomic, strong) GIDSignIn *signIn;

@end

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    super.childViewController = self;
    
    self.username.delegate = self;
    self.password.delegate = self;
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    _signIn = [GIDSignIn sharedInstance];
    _signIn.shouldFetchBasicProfile = YES;
    _signIn.clientID = GoogleClientID;
    _signIn.scopes = @[@"profile", @"https://www.googleapis.com/auth/plus.login"];
    _signIn.delegate = self;
    
    UITapGestureRecognizer *goAbout =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(goSignGoogle:)];
    [self.signInButton setUserInteractionEnabled:YES];
    [self.signInButton addGestureRecognizer:goAbout];
    
    loginResult = NO;
    [self initform];
    
    if([NSUserDefaultsApp getIntro] == nil){
        rootView = self.navigationController.view;
        [self showIntroWithCrossDissolve];
    }
    
    if([NSUserDefaultsApp getUsername] != nil){
        loginResult = YES;
        [self performSegueWithIdentifier:@"segueLogin" sender:self];
    }
}

- (void)goSignGoogle:(UITapGestureRecognizer *)recognizer {
    [_signIn signIn];
}

-(void)initform{
    UIImageView *lockImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_person_black_24pt.png"]];
    lockImage.bounds = CGRectMake(0, 0, 35, 35);
    lockImage.contentMode = UIViewContentModeLeft;
    self.username.tintColor = [UIColor mf_veryDarkGrayColor];
    self.username.textColor = [UIColor mf_veryDarkGrayColor];
    self.username.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.username.placeholderAnimatesOnFocus = YES;
    UIFontDescriptor * fontDescriptor = [self.username.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *font = [UIFont fontWithDescriptor:fontDescriptor size:self.username.font.pointSize];
    self.username.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Nama" attributes:@{NSFontAttributeName:font}];
    self.username.leftViewMode = UITextFieldViewModeAlways;
    self.username.leftView = lockImage;
    
    UIImageView *lockImagePass = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"baseline_lock_black_24pt.png"]];
    lockImagePass.bounds = CGRectMake(0, 0, 35, 35);
    lockImagePass.contentMode = UIViewContentModeLeft;
    self.password.tintColor = [UIColor mf_veryDarkGrayColor];
    self.password.textColor = [UIColor mf_veryDarkGrayColor];
    self.password.defaultPlaceholderColor = [UIColor mf_darkGrayColor];
    self.password.placeholderAnimatesOnFocus = YES;
    UIFontDescriptor * fontDescriptorPass = [self.password.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitVertical];
    UIFont *fontPass = [UIFont fontWithDescriptor:fontDescriptorPass size:self.password.font.pointSize];
    self.password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Kata Sandi" attributes:@{NSFontAttributeName:fontPass}];
    self.password.leftViewMode = UITextFieldViewModeAlways;
    self.password.leftView = lockImagePass;
    
    self.buttonLogin.layer.cornerRadius = 24;
    self.buttonLogin.layer.masksToBounds = TRUE;
    
    [self.btnLoginFB setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Login Facebook"]
                                forState:UIControlStateNormal];

    [self.remember setCheckColor:[UIColor grayColor]];
    [self.remember setBorderColor:[UIColor grayColor]];
    
    self.carduiview.layer.cornerRadius = 12;
    self.carduiview.layer.masksToBounds = true;
    
    self.buttonLogin.enabled = YES;
}

- (IBAction)dismissKeyboard{
     [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.username) {
        [self.password becomeFirstResponder];
    }else{
        [self.password resignFirstResponder];
    }
    return YES;
}

-(void)mpCheckBoxDidChangeState:(kMPCheckBoxState)state checkBox:(MPCheckBox *)checkBox{
    if ([checkBox isEqual:self.remember]) {
        [self.remember setState:kMPCheckBoxStateChecked animated:NO];
    }else{
        [self.remember setState:kMPCheckBoxStateUnchecked animated:NO];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if([identifier isEqualToString:segueLogin]) {
        return loginResult;
    }else{
        return YES;
    }
}

- (void)validateUsername{
    NSError *error = nil;
    if (![self textUsernameIsValid]) {
        error = [self errorWithLocalizedDescription:@"Name not valid"];
    }else{
        
    }
    [self.username setError:error animated:YES];
}

- (void)validatePassword{
    NSError *error = nil;
    NSString *email = self.username.text;
    if (![self textPasswordIsValid]) {
        error = [self errorWithLocalizedDescription:@"Email not valid"];
    }else{
        
    }
    [self.password setError:error animated:YES];
}

- (BOOL)textUsernameIsValid{
    return self.username.text.length <= 5;
}

- (BOOL)textPasswordIsValid{
    return self.password.text.length <= 8;
}

- (void)showIntroWithCrossDissolve {
    EAIntroPage *page1 = [EAIntroPage page];
    page1.title = @"Kuningan";
    page1.desc = sampleDescription1;
    NSURL *imageURL1 = [NSURL URLWithString:@"https://ksmtour.com/media/images/articles27/Curug-putri-landung.jpg"];
    NSData *imageData1 = [NSData dataWithContentsOfURL:imageURL1];
    page1.bgImage  = [UIImage imageWithData:imageData1];
    //page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title1"]];
    
    EAIntroPage *page2 = [EAIntroPage page];
    page2.title = @"Bogor";
    page2.desc = sampleDescription2;
    NSURL *imageURL2 = [NSURL URLWithString:@"https://1.bp.blogspot.com/-aLQulVGMtuM/WfgAIlZ8xAI/AAAAAAAACdA/BDoCFrNq8bU975Kqfknri7GrJe5X12dbQCLcBGAs/s1600/n1.jpg"];
    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL2];
    page2.bgImage = [UIImage imageWithData:imageData2];
    
    EAIntroPage *page3 = [EAIntroPage page];
    page3.title = @"Bandung";
    page3.desc = sampleDescription3;
    NSURL *imageURL3 = [NSURL URLWithString:@"https://www.wanderlustchloe.com/wp-content/uploads/2018/12/Bali-honeymoon-hotel-12.jpg"];
    NSData *imageData3 = [NSData dataWithContentsOfURL:imageURL3];
    page3.bgImage = [UIImage imageWithData:imageData3];
    
    EAIntroPage *page4 = [EAIntroPage page];
    page4.title = @"Pangandaran";
    page4.desc = sampleDescription4;
    NSURL *imageURL4 = [NSURL URLWithString:@"https://www.wanderlustchloe.com/wp-content/uploads/2018/12/Bali-honeymoon-hotel-13.jpg"];
    NSData *imageData4 = [NSData dataWithContentsOfURL:imageURL4];
    page4.bgImage = [UIImage imageWithData:imageData4];
    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:rootView.bounds andPages:@[page1,page2,page3,page4]];
    intro.skipButtonAlignment = EAViewAlignmentCenter;
    [intro.skipButton setTitleColor:[UIColor colorWithHexString:primary_color_purple] forState:UIControlStateNormal];
    intro.skipButtonY = 80.f;
    intro.pageControlY = 42.f;
    
    [intro setDelegate:self];
    
    [intro showInView:rootView animateDuration:0.3];
}

- (void)introDidFinish:(EAIntroView *)introView wasSkipped:(BOOL)wasSkipped {
    if(wasSkipped) {
        [NSUserDefaultsApp setIntro:@"1"];
    } else {
        [NSUserDefaultsApp setIntro:@"1"];
    }
}

- (IBAction)didChangeUITextField:(UITextField *)textField{
    if (textField == self.username) {
        [self validateUsername];
    }else if (textField == self.password) {
        [self validatePassword];
    }
}


- (IBAction)actionForgotPassword:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *smallViewController = [storyboard instantiateViewControllerWithIdentifier:@"ForgotPassword"];
    
    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:smallViewController contentSize:CGSizeMake(300, 200)];
    [self presentViewController:popupViewController animated:NO completion:nil];
    [popupViewController setShowDismissButton:NO];
}

- (IBAction)actionLogin:(id)sender {
    if([self validationFormLogin]){
       [self.loginPresenter doLogin:_username.text password:_password.text];
    }
}

- (IBAction)btnLoginFB:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
}

-(void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error{
    
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *name = user.profile.name;
    NSString *email = user.profile.email;
    NSLog(@"Customer details: %@ %@ %@ %@", userId, idToken, name, email);
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController {
//    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
//    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnSignupClick:(id)sender {
    [self.loginPresenter goToSignup];
}


#pragma mark - LoginViewProtocol
- (void)showLoading {
    _username.hidden = YES;
    _password.hidden = YES;
    _buttonLogin.hidden = YES;
    _signInButton.hidden = YES;
    _remember.hidden = YES;
    _lblremember.hidden = YES;
    _btnForgotPassword.hidden = YES;
    _btnRegister.hidden = YES;
    _btnLoginFB.hidden = YES;
    [super showLoading];
}

- (void)hideLoading {
    _username.hidden = NO;
    _password.hidden = NO;
    _buttonLogin.hidden = NO;
    _signInButton.hidden = NO;
    _remember.hidden = NO;
    _lblremember.hidden = NO;
    _btnForgotPassword.hidden = NO;
    _btnRegister.hidden = NO;
    _btnLoginFB.hidden = NO;
    [super hideLoading];
}

#pragma mark - BaseViewProtocol
- (void)connectionHideView {
    self.carduiview.hidden = YES;
    self.uiImageBackground.hidden = YES;
}

- (void)connectionShowView {
    self.carduiview.hidden = NO;
    self.uiImageBackground.hidden = NO;
}

- (NSError *)errorWithLocalizedDescription:(NSString *)localizedDescription{
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: localizedDescription};
    return [NSError errorWithDomain:ErrorDomainLogin code:ErrorDomainLoginCode userInfo:userInfo];
}

-(BOOL)validationFormLogin{
    NSString* Username     = self.username.text;
    NSString* Password     = self.password.text;
    
    if([Username isEqualToString:@""] && [Password isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Username field is required"];
        [self.username setError:errorName animated:YES];
        
        NSError *errorPassword = nil;
        errorPassword = [self errorWithLocalizedDescription:@"Password field is required"];
        [self.password setError:errorPassword animated:YES];
        
        return FALSE;
    }else if([Username isEqualToString:@""]){
        NSError *errorName = nil;
        errorName = [self errorWithLocalizedDescription:@"Username field is required"];
        [self.username setError:errorName animated:YES];
        
        return FALSE;
    }else if([Password isEqualToString:@""]){
        NSError *errorPassword = nil;
        errorPassword = [self errorWithLocalizedDescription:@"Password field is required"];
        [self.password setError:errorPassword animated:YES];
        
        return FALSE;
    }else{
        return TRUE;
    }
    return TRUE;
}
@end

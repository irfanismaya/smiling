//  LoginController.h
//  Smiling
//  Created by Irfan-Ismaya on 08/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
#import "MDCCard.h"
#import <MaterialComponents/MaterialButtons.h>
#import <MaterialComponents/MaterialTextFields.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "MaterialButtons.h"
#import "MPCheckBox.h"
#import "MPCheckBoxDelegate.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import "LoginModuleProtocol.h"
#import "BaseViewProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginController : BaseViewController<BaseViewProtocol, LoginViewProtocol, MPCheckBoxDelegate, UITextFieldDelegate, GIDSignInDelegate, GIDSignInUIDelegate>

@property (nonatomic, strong) id<LoginPresenterProtocol> loginPresenter;

@end

NS_ASSUME_NONNULL_END

//  LoginPresenter.m
//  Smiling
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "LoginPresenter.h"
#import "LoginWireframe.h"

@interface LoginPresenter ()

@end

@implementation LoginPresenter

-(BOOL)validationFormLogin:(NSString *)username password:(NSString *)password {
    if([username isEqualToString:@""]){
        [UIUtils showSnackbar:LOGIN_TITLE message:USERNAME_EMPTY alertType:ISAlertTypeWarning];
        return FALSE;
    }else if([password isEqualToString:@""]){
        [UIUtils showSnackbar:LOGIN_TITLE message:PASSWORD_EMPTY alertType:ISAlertTypeWarning];
        return FALSE;
    }else{
        return TRUE;
    }
    return TRUE;
}

#pragma mark - LoginPresenterProtocol
- (void)doLogin:(NSString *)username password:(NSString *)password {
    if([self validationFormLogin:username password:password]) {
        NSDictionary *params = @{@"username":@"peter@klaven",
                                 @"password":@"cityslicka"};
        
        [self.loginController showLoading];
        [self.loginInteractor login:params];
    }
}

- (void)goToSignup {
    [self.loginWireframe goToSignup];
}

#pragma mark - LoginInteractorOutput
- (void)didLogin:(Login *)login error:(NSError *)error {
    if(login != nil) {
        [self.loginController hideLoading];
        
        //pindah halaman ke home
        [self.loginWireframe goToHomeView];
    } else {
        [self.loginController hideLoading];
    }
}

@end

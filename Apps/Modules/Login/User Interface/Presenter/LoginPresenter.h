//  LoginPresenter.h
//  Smiling
//  Created by Rully Winata on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "LoginModuleProtocol.h"
#import "LoginWireframe.h"

@interface LoginPresenter : NSObject <LoginPresenterProtocol, LoginInteractorOutput>

@property (nonatomic, strong) LoginWireframe* loginWireframe;
@property (nonatomic, strong) UIViewController<LoginViewProtocol> *loginController;

@property (nonatomic, strong) id <LoginInteractorInput> loginInteractor;

@end

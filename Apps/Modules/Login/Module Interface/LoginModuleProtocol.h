//  LoginModulProtocol.h
//  Smiling
//  Created by Irfan-Ismaya on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "Login.h"

//khusus buat presenter
@protocol LoginPresenterProtocol <NSObject>
- (void)doLogin:(NSString *)username password:(NSString *)password;
- (void)goToSignup;
@end

//khusus buat interactor
@protocol LoginInteractorInput <NSObject>
- (void)login:(NSDictionary *)params;
@end

//return dari interactor ke presenter
@protocol LoginInteractorOutput <NSObject>
- (void)didLogin:(Login *)login error:(NSError *)error;
@end

@protocol LoginManagerAPIInput <NSObject>
- (void)login:(NSDictionary *)params completion:(void (^)(Login *login, NSError *error))completion;
@end

//khusus buat view
@protocol LoginViewProtocol <NSObject>
- (void)showLoading;
- (void)hideLoading;
@end

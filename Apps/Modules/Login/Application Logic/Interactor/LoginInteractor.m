//  LoginInteractor.m
//  Smiling
//  Created by Irfan-Ismaya on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "LoginInteractor.h"
#import "LoginManagerAPI.h"

@interface LoginInteractor ()

@property (nonatomic, strong) LoginManagerAPI *loginManagerAPI;

@end

@implementation LoginInteractor

- (instancetype)initWithLoginManager:(LoginManagerAPI *)loginManagerAPI
{
    if ((self = [super init]))
    {
        _loginManagerAPI = loginManagerAPI;
    }
    
    return self;
}

#pragma mark - LoginInteractorInput
- (void)login:(NSDictionary *)params {
    __weak typeof(self) weakSelf = self;
    
    [self.loginManagerAPI login:params completion:^(Login *login, NSError *error) {
        [weakSelf.loginPresenter didLogin:login error:error];
    }];
}

@end

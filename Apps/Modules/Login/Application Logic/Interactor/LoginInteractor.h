//  LoginInteractor.h
//  Smiling
//  Created by Irfan-Ismaya on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "LoginModuleProtocol.h"
@class LoginManagerAPI;

@interface LoginInteractor : NSObject <LoginInteractorInput>

- (instancetype)initWithLoginManager:(LoginManagerAPI *)loginManagerAPI;

@property (nonatomic, weak) id <LoginInteractorOutput> loginPresenter;

@end

//  LoginManagerAPI.m
//  Smiling
//  Created by Irfan-Ismaya on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "LoginManagerAPI.h"
#import "NetworkManager+Login.h"

@interface LoginManagerAPI ()
@end

@implementation LoginManagerAPI

- (void)login:(NSDictionary *)params completion:(void (^)(Login *, NSError *))completion {
    [NetworkManager login:params successBlock:^(Login *login) {
        completion(login, nil);
    } failureBlock:^(NSError *error) {
        completion(nil, error);
    }];
}

@end

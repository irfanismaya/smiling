//  LoginManagerAPI.h
//  Smiling
//  Created by Irfan-Ismaya on 15/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "LoginModuleProtocol.h"

@interface LoginManagerAPI : NSObject <LoginManagerAPIInput>

@end

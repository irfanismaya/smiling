//  NearbyModuleInterface.h
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

//khusus buat presenter
@protocol NearbyPresenterProtocol <NSObject>
- (void)doGetUserList:(NSInteger)page;
@end

//khusus buat interactor
@protocol NearbyInteractorInput <NSObject>
- (void)getUserList:(NSDictionary *)params;
@end

@protocol NearbyInteractorOutput <NSObject>
- (void)didGetUserList:(NSArray *)listUser error:(NSError *)error;
@end

@protocol NearbyManagerAPIInput <NSObject>
- (void)getUserList:(NSDictionary *)params completion:(void (^)(NSArray *listUser, NSError *error))completion;
@end

//khusus buat view
@protocol NearbyViewProtocol <NSObject>
- (void)showLoading;
- (void)hideLoading;
@end

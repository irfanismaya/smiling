//  NearbyController.m
//  Smiling
//  Created by Irfan-Ismaya on 04/04/19.
//  Copyright © 2019 Merqurion Solution. All rights reserved.

#import "NearbyController.h"
#import "TYTabPagerBar.h"
#import "TYPagerController.h"
#import "ListViewController.h"
#import "CollectionViewController.h"
#import "CustomViewController.h"
#import "PromotionController.h"

@interface NearbyController ()<TYTabPagerBarDataSource,TYTabPagerBarDelegate,TYPagerControllerDataSource,TYPagerControllerDelegate>

@property (nonatomic, weak) TYTabPagerBar *tabBar;
@property (nonatomic, weak) TYPagerController *pagerController;

@property (nonatomic, strong) NSArray *datas;
@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *internetstatus;
@property (weak, nonatomic) IBOutlet UIView *uiViewTab;

@end

@implementation NearbyController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callCheckInternet];
    
    [self addTabPageBar];
    
    [self addPagerController];
    
    [self loadData];
}

-(void)callCheckInternet{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    NSString *remoteHostName = @"https://www.google.com/";
    NSString *remoteHostLabelFormatString = NSLocalizedString(@"Remote Host: %@", @"Remote host label format string");
    
    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
    [self.hostReachability startNotifier];
    [self updateInterfaceWithReachability:self.hostReachability];
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [self updateInterfaceWithReachability:self.internetReachability];
}

-(void)checkInternetConnection:(BOOL)connectionstatus{
    if(connectionstatus){
        _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_no_internet_satellite"]];
        _imageView.frame = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2, 200, 200);
        _imageView.center = CGPointMake(self.view.frame.size.width/2,
                                        self.view.frame.size.height/2);
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        
        CGRect frame = CGRectMake(0, 0, 245, 20);
        _internetstatus = [[UILabel alloc] initWithFrame:frame];
        _internetstatus.center = CGPointMake(self.view.frame.size.width/2, 440);
        _internetstatus.text = @"Oops! No Internet Connection";
        _internetstatus.textAlignment = NSTextAlignmentJustified;
        _internetstatus.textColor = [UIColor grayColor];
        [_internetstatus setFont:[UIFont boldSystemFontOfSize:17]];
        
        [self.view addSubview:_imageView];
        [self.view addSubview:_internetstatus];
    }else{
        [_imageView removeFromSuperview];
        [_internetstatus removeFromSuperview];
    }
}

-(void)checkNearbyxData:(BOOL)datastatus{
    if(datastatus){
        _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_no_feed"]];
        _imageView.frame = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2, 200, 200);
        _imageView.center = CGPointMake(self.view.frame.size.width/2,
                                        self.view.frame.size.height/2);
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        
        CGRect frame = CGRectMake(0, 0, 245, 20);
        _internetstatus = [[UILabel alloc] initWithFrame:frame];
        _internetstatus.center = CGPointMake(self.view.frame.size.width/2, self.view.bounds.size.height/1.5f);
        _internetstatus.text = @"Oops! No Nearby at this time";
        _internetstatus.textAlignment = NSTextAlignmentJustified;
        _internetstatus.textColor = [UIColor grayColor];
        [_internetstatus setFont:[UIFont boldSystemFontOfSize:17]];
        [self.view addSubview:_imageView];
        [self.view addSubview:_internetstatus];
    }else{
        [_imageView removeFromSuperview];
        [_internetstatus removeFromSuperview];
    }
}

- (void) reachabilityChanged:(NSNotification *)note{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}

- (void)updateInterfaceWithReachability:(Reachability *)reachability{
    if (reachability == self.hostReachability){
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        BOOL connectionRequired = [reachability connectionRequired];
        if (connectionRequired){
            
        }else{
            
        }
    }
    
    if (reachability == self.internetReachability){
        [self configureTextField:reachability];
    }
}

- (void)configureTextField:(Reachability *)reachability{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    BOOL connectionRequired = [reachability connectionRequired];
    NSString* statusString = @"";
    
    switch (netStatus){
        case NotReachable:{
            [self checkInternetConnection:YES];
            connectionRequired = NO;
            break;
        }case ReachableViaWWAN:{
            [self checkInternetConnection:NO];
            break;
        }case ReachableViaWiFi:{
            [self checkInternetConnection:NO];
            break;
        }
    }
    
    if (connectionRequired){
        NSString *connectionRequiredFormatString = NSLocalizedString(@"%@, Connection Required", @"Concatenation of status string with connection requirement");
        statusString= [NSString stringWithFormat:connectionRequiredFormatString, statusString];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

- (void)addTabPageBar {
    TYTabPagerBar *tabBar = [[TYTabPagerBar alloc]init];
    tabBar.layout.barStyle = TYPagerBarStyleProgressElasticView;
    tabBar.dataSource = self;
    tabBar.delegate = self;
    [tabBar registerClass:[TYTabPagerBarCell class] forCellWithReuseIdentifier:[TYTabPagerBarCell cellIdentifier]];
    [self.uiViewTab addSubview:tabBar];
    _tabBar = tabBar;
}

- (void)addPagerController {
    TYPagerController *pagerController = [[TYPagerController alloc]init];
    pagerController.layout.prefetchItemCount = 1;
    //pagerController.layout.autoMemoryCache = NO;
    // 只有当scroll滚动动画停止时才加载pagerview，用于优化滚动时性能
    pagerController.layout.addVisibleItemOnlyWhenScrollAnimatedEnd = YES;
    pagerController.dataSource = self;
    pagerController.delegate = self;
    [self addChildViewController:pagerController];
    [self.uiViewTab addSubview:pagerController.view];
    _pagerController = pagerController;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _tabBar.frame = CGRectMake(0, -10, CGRectGetWidth(self.uiViewTab.frame), 36);
    _pagerController.view.frame = CGRectMake(0, CGRectGetMaxY(_tabBar.frame), CGRectGetWidth(self.uiViewTab.frame), CGRectGetHeight(self.uiViewTab.frame)- CGRectGetMaxY(_tabBar.frame));
}

- (void)loadData {
    NSMutableArray *datas = [NSMutableArray array];
    for (NSInteger i = 0; i < 10; ++i) {
        [datas addObject:i%2 == 0 ? [NSString stringWithFormat:@"Tab %ld",i]:[NSString stringWithFormat:@"Tab Tab %ld",i]];
    }
    _datas = [datas copy];
    
    [self reloadData];
}

#pragma mark - TYTabPagerBarDataSource

- (NSInteger)numberOfItemsInPagerTabBar {
    return _datas.count;
}

- (UICollectionViewCell<TYTabPagerBarCellProtocol> *)pagerTabBar:(TYTabPagerBar *)pagerTabBar cellForItemAtIndex:(NSInteger)index {
    UICollectionViewCell<TYTabPagerBarCellProtocol> *cell = [pagerTabBar dequeueReusableCellWithReuseIdentifier:[TYTabPagerBarCell cellIdentifier] forIndex:index];
    cell.titleLabel.text = _datas[index];
    return cell;
}

#pragma mark - TYTabPagerBarDelegate

- (CGFloat)pagerTabBar:(TYTabPagerBar *)pagerTabBar widthForItemAtIndex:(NSInteger)index {
    NSString *title = _datas[index];
    return [pagerTabBar cellWidthForTitle:title];
}

- (void)pagerTabBar:(TYTabPagerBar *)pagerTabBar didSelectItemAtIndex:(NSInteger)index {
    [_pagerController scrollToControllerAtIndex:index animate:YES];
}

#pragma mark - TYPagerControllerDataSource

- (NSInteger)numberOfControllersInPagerController {
    return 10;
}

- (UIViewController *)pagerController:(TYPagerController *)pagerController controllerForIndex:(NSInteger)index prefetching:(BOOL)prefetching {
    if (index%3 == 0) {
        PromotionController *VC = [[PromotionController alloc]init];
        return VC;
    }else if (index%3 == 1) {
        ListViewController *VC = [[ListViewController alloc]init];
//        VC.text = [@(index) stringValue];
        return VC;
    }else {
        CollectionViewController *VC = [[CollectionViewController alloc]init];
        VC.text = [@(index) stringValue];
        return VC;
    }
}

#pragma mark - TYPagerControllerDelegate

- (void)pagerController:(TYPagerController *)pagerController transitionFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex animated:(BOOL)animated {
    [_tabBar scrollToItemFromIndex:fromIndex toIndex:toIndex animate:animated];
}

-(void)pagerController:(TYPagerController *)pagerController transitionFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex progress:(CGFloat)progress {
    [_tabBar scrollToItemFromIndex:fromIndex toIndex:toIndex progress:progress];
}

- (void)reloadData {
    [_tabBar reloadData];
    [_pagerController reloadData];
}

@end

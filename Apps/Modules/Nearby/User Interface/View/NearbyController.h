//  NearbyController.h
//  Smiling
//  Created by Irfan-Ismaya on 04/04/19.
//  Copyright © 2019 Merqurion Solution. All rights reserved.

#import "NearbyModuleProtocol.h"

@interface NearbyController : UIViewController <NearbyViewProtocol>

@property (nonatomic, strong) id<NearbyPresenterProtocol> nearbyPresenter;

@end

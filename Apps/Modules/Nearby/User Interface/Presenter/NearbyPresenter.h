//  NearbyPresenter.h
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NearbyModuleProtocol.h"
#import "NearbyWireframe.h"

@interface NearbyPresenter : NSObject <NearbyPresenterProtocol, NearbyInteractorOutput>

@property (nonatomic, strong) NearbyWireframe* nearbyWireframe;
@property (nonatomic, strong) UIViewController<NearbyViewProtocol> *nearbyController;

@property (nonatomic, strong) id <NearbyInteractorInput> nearbyInteractor;

@end

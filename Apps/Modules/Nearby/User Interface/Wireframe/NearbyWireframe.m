//  NearbyWireframe.m
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NearbyWireframe.h"
#import "NearbyController.h"
#import "NearbyPresenter.h"
#import "RootWireframe.h"

#import "NearbyManagerAPI.h"
#import "NearbyInteractor.h"

static NSString *NearbyControllerIdentifier = @"NearbyController";

@interface NearbyWireframe ()

@property (nonatomic, strong) NearbyController *nearbyController;

@end

@implementation NearbyWireframe

- (void)presentNearbyInterfaceFromViewController:(UIViewController *)viewController
{
    [self initWireframe];
    [viewController presentViewController:self.nearbyController animated:YES completion:nil];
}

- (UIViewController *)configuredViewController {
    [self initWireframe];
    return self.nearbyController;
}

- (void) initWireframe {
    self.nearbyController = [self viewControllerFromStoryboard:NearbyControllerIdentifier];
    
    NearbyManagerAPI *nearbyManagerAPI = [[NearbyManagerAPI alloc] init];
    NearbyInteractor *nearbyInteractor = [[NearbyInteractor alloc] initWithNearbyManager:nearbyManagerAPI];
    
    self.nearbyPresenter = [[NearbyPresenter alloc] init];
    self.nearbyPresenter.nearbyWireframe = self;
    self.nearbyPresenter.nearbyInteractor = nearbyInteractor;
    self.nearbyPresenter.nearbyController = self.nearbyController;
    
    nearbyInteractor.nearbyPresenter = self.nearbyPresenter;
    self.nearbyController.nearbyPresenter = self.nearbyPresenter;
}

- (UIImage *)tabIcon {
    return [UIImage imageNamed:TAB_NEARBY_ICON];
}

- (NSString *)tabTitle {
    return TAB_NEARBY;
}

@end

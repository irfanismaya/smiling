//  NearbyWireframe.h
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "TabBarModuleProtocol.h"
#import "RootWireframe.h"
@class NearbyPresenter;

@interface NearbyWireframe : RootWireframe <TabBarViewProtocol>

@property (nonatomic, strong) NearbyPresenter *nearbyPresenter;

- (void)presentNearbyInterfaceFromViewController:(UIViewController *)viewController;

@end

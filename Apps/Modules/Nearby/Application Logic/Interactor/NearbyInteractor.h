//  NearbyInteractor.h
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NearbyModuleProtocol.h"
@class NearbyManagerAPI;

@interface NearbyInteractor : NSObject <NearbyInteractorInput>

- (instancetype)initWithNearbyManager:(NearbyManagerAPI *)nearbyManagerAPI;

@property (nonatomic, weak) id <NearbyInteractorOutput> nearbyPresenter;

@end

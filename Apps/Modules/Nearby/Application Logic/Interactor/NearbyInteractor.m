//  NearbyInteractor.m
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "NearbyInteractor.h"
#import "NearbyManagerAPI.h"

@interface NearbyInteractor ()

@property (nonatomic, strong) NearbyManagerAPI *nearbyManagerAPI;

@end

@implementation NearbyInteractor

- (instancetype)initWithNearbyManager:(NearbyManagerAPI *)nearbyManagerAPI
{
    if ((self = [super init]))
    {
        _nearbyManagerAPI = nearbyManagerAPI;
    }
    
    return self;
}

#pragma mark - NearbyInteractorInput
- (void)getUserList:(NSDictionary *)params {
    __weak typeof(self) weakSelf = self;
    
    [self.nearbyManagerAPI getUserList:params completion:^(NSArray *listUser, NSError *error) {
        [weakSelf.nearbyPresenter didGetUserList:listUser error:error];
    }];
}

@end


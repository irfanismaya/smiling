//  RulesController.h
//  Smiling
//  Created by Irfan-Ismaya on 31/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface RulesController : UIViewController<WKNavigationDelegate, WKUIDelegate>
@property (assign, nonatomic) NSString *page_title;
@property (weak, nonatomic) IBOutlet WKWebView *webView;
@end


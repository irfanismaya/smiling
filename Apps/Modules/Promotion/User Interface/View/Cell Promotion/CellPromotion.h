//  CellPromotion.h
//  Smiling
//  Created by Irfan-Ismaya on 04/06/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
@import WOWRibbonView;

@interface CellPromotion : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewParent;
@property (weak, nonatomic) IBOutlet UIView *viewCirclePromotion;
@property (weak, nonatomic) IBOutlet WOWRibbonView *viewRibbonPromotion;
@property (weak, nonatomic) IBOutlet UIImageView *distanceImage;
@property (weak, nonatomic) IBOutlet UILabel *distanceLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *imgParent;
@end


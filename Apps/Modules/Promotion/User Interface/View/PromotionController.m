//  PromotionController.m
//  Smiling
//  Created by Irfan-Ismaya on 04/06/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "PromotionController.h"
#import "CellPromotion.h"

@interface PromotionController ()<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tvPromotion;

@end

@implementation PromotionController

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    self.tvPromotion.frame = self.view.bounds;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tvPromotion.delegate = self;
    self.tvPromotion.dataSource = self;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 160;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"CellPromotion";
    CellPromotion *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        UINib *cellNib = [UINib nibWithNibName:cellIdentifier bundle:nil];
        [self.tvPromotion registerNib:cellNib forCellReuseIdentifier:cellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.viewParent.layer.cornerRadius = 6.0f;
    cell.imgParent.layer.cornerRadius = 6.0f;
    cell.viewCirclePromotion.layer.masksToBounds = TRUE;
    cell.viewCirclePromotion.layer.cornerRadius = 27.5f;
    
    return cell;
}

@end

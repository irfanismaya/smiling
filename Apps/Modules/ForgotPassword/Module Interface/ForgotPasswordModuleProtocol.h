//  ForgotPasswordModuleProtocol.h
//  Smiling
//  Created by Irfan-Ismaya on 22/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ForgotPassword.h"

//khusus buat presenter
@protocol ForgotPasswordPresenterProtocol <NSObject>
- (void)doForgotPassword:(NSString *)email;
@end

//khusus buat interactor
@protocol ForgotPasswordInteractorInput <NSObject>
- (void)forgotpassword:(NSDictionary *)params;
@end

@protocol ForgotPasswordInteractorOutput <NSObject>
- (void)didForgotPassword:(ForgotPassword *)forgotpassword error:(NSError *)error;
@end

@protocol ForgotPasswordManagerAPIInput <NSObject>
- (void)forgotpassword:(NSDictionary *)params completion:(void (^)(ForgotPassword *forgotpassword, NSError *error))completion;
@end

//khusus buat view
@protocol ForgotPasswordViewProtocol <NSObject>
- (void)showLoading;
- (void)hideLoading;
@end

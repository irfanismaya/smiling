//  ForgotPasswordController.h
//  Smiling
//  Created by Irfan-Ismaya on 04/04/19.
//  Copyright © 2019 Merqurion Solution. All rights reserved.

#import <UIKit/UIKit.h>
#import <MaterialComponents/MaterialButtons.h>
#import <MaterialComponents/MaterialTextFields.h>
#import "MaterialButtons.h"

NS_ASSUME_NONNULL_BEGIN

@interface ForgotPasswordController : UIViewController
@property (weak, nonatomic) IBOutlet MDCTextField *tfemail;
@property(nonatomic) MDCTextInputControllerOutlined *emailTextFieldController;
- (IBAction)actionCancel:(id)sender;
- (IBAction)actionSend:(id)sender;
@end

NS_ASSUME_NONNULL_END

//  ForgotPasswordController.m
//  Smiling
//  Created by Irfan-Ismaya on 04/04/19.
//  Copyright © 2019 Merqurion Solution. All rights reserved.

#import "ForgotPasswordController.h"
#import "UIColor+HexString.h"
#import "ISMessages.h"

@interface ForgotPasswordController ()

@end

@implementation ForgotPasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.emailTextFieldController =
    [[MDCTextInputControllerOutlined alloc] initWithTextInput:self.tfemail];
    self.emailTextFieldController.inlinePlaceholderColor = [UIColor colorWithHexString:primary_color_purple];
    self.emailTextFieldController.normalColor = [UIColor colorWithHexString:primary_color_purple];
    self.emailTextFieldController.activeColor = [UIColor colorWithHexString:primary_color_purple];
    self.emailTextFieldController.floatingPlaceholderActiveColor = [UIColor colorWithHexString:primary_color_purple];
    self.emailTextFieldController.placeholderText = @"Surat Elektronik";
}

- (IBAction)actionCancel:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)actionSend:(id)sender {
    NSString *Email = self.tfemail.text;
    if([Email isEqualToString:@""]){
        [ISMessages showCardAlertWithTitle:@"Lupa Kata Sandi"
                                   message:@"Silahkan masukan surat elektronik"
                                  duration:2.f
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:nil];
    }else{
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

@end

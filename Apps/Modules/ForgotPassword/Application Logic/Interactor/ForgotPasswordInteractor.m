//  ForgotPasswordInteractor.m
//  Smiling
//  Created by Irfan-Ismaya on 22/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ForgotPasswordInteractor.h"
#import "ForgotPasswordManagerAPI.h"

@interface ForgotPasswordInteractor ()
@property (nonatomic, strong) ForgotPasswordManagerAPI *forgotpasswordManagerAPI;
@end

@implementation ForgotPasswordInteractor

- (instancetype)initWithForgotPasswordManager:(ForgotPasswordManagerAPI *)forgotpasswordManagerAPI{
    if ((self = [super init])){
        _forgotpasswordManagerAPI = forgotpasswordManagerAPI;
    }
    return self;
}

#pragma mark - LoginInteractorInput
- (void)forgotpassword:(NSDictionary *)params {
    __weak typeof(self) weakSelf = self;
    [self.forgotpasswordManagerAPI forgotpassword:params completion:^(ForgotPassword *forgotpassword, NSError *error) {
        [weakSelf.forgotpasswordPresenter didForgotPassword:forgotpassword error:error];
    }];
}

@end

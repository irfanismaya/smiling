//  ForgotPasswordInteractor.h
//  Smiling
//  Created by Irfan-Ismaya on 22/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ForgotPasswordModuleProtocol.h"
@class ForgotPasswordManagerAPI;

@interface ForgotPasswordInteractor : NSObject <ForgotPasswordInteractorInput>
- (instancetype)initWithForgotPasswordManager:(ForgotPasswordManagerAPI *)loginManagerAPI;
@property (nonatomic, weak) id <ForgotPasswordInteractorOutput> forgotpasswordPresenter;
@end

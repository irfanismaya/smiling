//  ForgotPasswordManagerAPI.m
//  Smiling
//  Created by Irfan-Ismaya on 22/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ForgotPasswordManagerAPI.h"
#import "NetworkManager+ForgotPassword.h"

@interface ForgotPasswordManagerAPI()
@end

@implementation ForgotPasswordManagerAPI
- (void)forgotpassword:(NSDictionary *)params completion:(void (^)(ForgotPassword *, NSError *))completion {
    [NetworkManager forgotpassword:params successBlock:^(ForgotPassword *forgotpassword) {
        completion(forgotpassword, nil);
    } failureBlock:^(NSError *error) {
        completion(nil, error);
    }];
}
@end

//  ForgotPasswordManagerAPI.h
//  Smiling
//  Created by Irfan-Ismaya on 22/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "ForgotPasswordModuleProtocol.h"
@interface ForgotPasswordManagerAPI : NSObject <ForgotPasswordManagerAPIInput>
@end


//  HotelController.m
//  Smiling
//  Created by Irfan-Ismaya on 22/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "HotelController.h"
#import "HotelHeader.h"
#import "HotelCollectionCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DetailHotelController.h"

@interface HotelController ()
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *uiViewFilter;
@property (weak, nonatomic) IBOutlet UIImageView *imgBack;
@property (weak, nonatomic) IBOutlet UIImageView *imgFilter;
@property (weak, nonatomic) IBOutlet UICollectionView *hotelCollection;
@end
NSArray * hotelGallery;
NSArray * imgHotel;
static HotelHeader *headerView;
@implementation HotelController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initViewHeader];
    
    UITapGestureRecognizer *actionBack =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionBack:)];
    [self.imgBack setUserInteractionEnabled:YES];
    [self.imgBack addGestureRecognizer:actionBack];
    
    hotelGallery = [[NSArray alloc] initWithObjects:
                       [UIImage imageNamed:@"bg1@2x.png"],
                       [UIImage imageNamed:@"bg2@2x.png"],
                       [UIImage imageNamed:@"bg3@2x.png"],
                       [UIImage imageNamed:@"bg4@2x.png"],nil];
    
    imgHotel = @[@"https://t-ec.bstatic.com/images/hotel/max1024x768/455/45596534.jpg", @"https://t-ec.bstatic.com/images/hotel/max1024x768/120/120763057.jpg", @"https://cintaihidup.com/wp-content/uploads/2017/04/sapulidi-700x466.jpg", @"https://cintaihidup.com/wp-content/uploads/2017/04/imah-seniman-700x393.jpg"];
}

-(void)initViewHeader{
    for (UIView *subview in self.searchBar.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
            break;
        }
        for (UIView *subsubview in subview.subviews) {
            if ([subsubview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [subsubview removeFromSuperview];
                break;
            }
        }
    }
    
    self.imgBack.tintColor = [UIColor blackColor];
    self.imgFilter.tintColor = [UIColor grayColor];
    self.uiViewFilter.layer.cornerRadius = 10;
    self.searchBar.barTintColor = [UIColor grayColor];
    self.searchBar.layer.borderColor = [UIColor grayColor].CGColor;
    self.searchBar.backgroundColor = [UIColor whiteColor];
    
    UITextField *searchField = [self.searchBar valueForKey:@"searchField"];
    searchField.backgroundColor = [UIColor colorWithHexString:@"#F6F9FF"];
    searchField.textColor = [UIColor grayColor];
    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Cari Hotel"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor grayColor];
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return hotelGallery.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HotelHeader" forIndexPath:indexPath];
        headerView.imageHotel.delegate = self;
        headerView.imageHotel.dataSource = self;
        headerView.imageHotel.slideshowTimeInterval = 4.5f;
        headerView.imageHotel.clipsToBounds = true;
        headerView.imageHotel.layer.cornerRadius = 10;
        reusableview = headerView;
    }
    
    return reusableview;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DetailHotelController *detailHotel = [storyboard instantiateViewControllerWithIdentifier:@"DetailHotelController"];
    [self.navigationController pushViewController:detailHotel animated:YES];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HotelCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HotelCollectionCell" forIndexPath:indexPath];
    
    cell.uiView.clipsToBounds = true;
    cell.uiView.layer.cornerRadius = 10;
    cell.uiView.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMinXMinYCorner;
    
    [cell.imgCulinary sd_setImageWithURL:[NSURL URLWithString:imgHotel[indexPath.row]]
                        placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
    
    cell.imgCulinary.clipsToBounds = true;
    cell.imgCulinary.layer.cornerRadius = 10;
    cell.imgCulinary.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMinXMinYCorner;
    
    cell.uiView.clipsToBounds = true;
    cell.uiView.layer.cornerRadius = 10;
    cell.uiView.layer.maskedCorners = kCALayerMaxXMinYCorner | kCALayerMaxXMaxYCorner;
    
    cell.uiViewDate.clipsToBounds = true;
    cell.uiViewDate.layer.cornerRadius = 10;
    cell.uiViewDate.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner;
    
    cell.btnPrice.clipsToBounds = true;
    cell.btnPrice.layer.cornerRadius = 15.f;
    
    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

#pragma mark - KIImagePager DataSource
- (NSArray *) arrayWithImages:(KIImagePager*)pager
{
    return @[
             @"https://s-ec.bstatic.com/images/hotel/max1024x768/147/147997361.jpg",
             @"https://s-ec.bstatic.com/images/hotel/max1024x768/681/68184730.jpg",
             @"http://lh3.googleusercontent.com/-ml7ikWREpIs/VeAFnYOQ59I/AAAAAAAAPys/3AKkQVUCcbg/s1600/1440040657309-01.jpeg"
             ];
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager *)pager
{
    return UIViewContentModeScaleAspectFill;
}

#pragma mark - KIImagePager Delegate
- (void) imagePager:(KIImagePager *)imagePager didScrollToIndex:(NSUInteger)index{
    
}

- (void) imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index{
    
}

@end

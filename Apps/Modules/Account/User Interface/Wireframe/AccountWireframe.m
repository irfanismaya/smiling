//
//  AccountWireframe.m
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "AccountWireframe.h"
#import "AccountController.h"
#import "AccountPresenter.h"
#import "RootWireframe.h"

#import "AccountManagerAPI.h"
#import "AccountInteractor.h"

static NSString *AccountControllerIdentifier = @"AccountController";

@interface AccountWireframe ()

@property (nonatomic, strong) AccountController *accountController;

@end

@implementation AccountWireframe

- (void)presentAccountInterfaceFromViewController:(UIViewController *)viewController
{
    [self initWireframe];
    [viewController presentViewController:self.accountController animated:YES completion:nil];
}

- (UIViewController *)configuredViewController {
    [self initWireframe];
    return self.accountController;
}

- (void) initWireframe {
    self.accountController = [self viewControllerFromStoryboard:AccountControllerIdentifier];
    
    AccountManagerAPI *accountManagerAPI = [[AccountManagerAPI alloc] init];
    AccountInteractor *accountInteractor = [[AccountInteractor alloc] initWithAccountManager:accountManagerAPI];
    
    self.accountPresenter = [[AccountPresenter alloc] init];
    self.accountPresenter.accountWireframe = self;
    self.accountPresenter.accountInteractor = accountInteractor;
    self.accountPresenter.accountController = self.accountController;
    
    accountInteractor.accountPresenter = self.accountPresenter;
    self.accountController.accountPresenter = self.accountPresenter;
}

- (UIImage *)tabIcon {
    return [UIImage imageNamed:TAB_ACCOUNT_ICON];
}

- (NSString *)tabTitle {
    return TAB_ACCOUNT;
}

@end

//
//  AccountWireframe.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "TabBarModuleProtocol.h"
#import "RootWireframe.h"
@class AccountPresenter;

@interface AccountWireframe : RootWireframe <TabBarViewProtocol>

@property (nonatomic, strong) AccountPresenter *accountPresenter;

- (void)presentAccountInterfaceFromViewController:(UIViewController *)viewController;

@end

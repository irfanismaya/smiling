//
//  AccountPresenter.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "AccountModuleProtocol.h"
#import "AccountWireframe.h"

@interface AccountPresenter : NSObject <AccountPresenterProtocol, AccountInteractorOutput>

@property (nonatomic, strong) AccountWireframe* accountWireframe;
@property (nonatomic, strong) UIViewController<AccountViewProtocol> *accountController;

@property (nonatomic, strong) id <AccountInteractorInput> accountInteractor;

@end

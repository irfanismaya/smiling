//  AccountController.h
//  Smiling
//  Created by Irfan-Ismaya on 04/04/19.
//  Copyright © 2019 Merqurion Solution. All rights reserved.

#import <UIKit/UIKit.h>
#import <MaterialComponents/MaterialButtons.h>
#import <MaterialComponents/MaterialTextFields.h>
#import <MaterialComponents/MaterialCards.h>
#import "MaterialButtons.h"
#import "CircleImageView.h"
#import "FileUtil.h"
#import "AccountModuleProtocol.h"
#import "SCPinViewController.h"
#import "SCPinAppearance.h"

NS_ASSUME_NONNULL_BEGIN

@interface AccountController : UIViewController<AccountViewProtocol, UINavigationControllerDelegate,
UIImagePickerControllerDelegate,SCPinViewControllerCreateDelegate, SCPinViewControllerDataSource, SCPinViewControllerValidateDelegate>
@property UIImage* image;
@property (nonatomic, strong) id<AccountPresenterProtocol> accountPresenter;

@end

NS_ASSUME_NONNULL_END

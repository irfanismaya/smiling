//  AccountController.m
//  Smiling
//  Created by Irfan-Ismaya on 04/04/19.
//  Copyright © 2019 Merqurion Solution. All rights reserved.

#import "AccountController.h"
#import <MaterialComponents/MaterialAppBar.h>
#import "Constants.h"
#import "NSDate+TCUtils.h"
#import "UIColor+HexString.h"
#import "LSLDatePickerDialog.h"
#import "ActionSheetStringPicker.h"
#import "ActionSheetDatePicker.h"
#import "ISMessages.h"
#import "AHKActionSheet.h"
#import "JDStatusBarNotification.h"
#import "UIImage+ImageHelper.h"
#import "EditProfileController.h"
#import "ChangePassController.h"
#import "RulesController.h"
#import "ListPaymentController.h"
#import "CustomerSupportController.h"
#import "Smiling-Swift.h"

NSString * const kViewControllerPin = @"kViewControllerPin";
@interface AccountController ()
@property (nonatomic, strong) AbstractActionSheetPicker *actionSheetPicker;

@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *internetstatus;
@property (weak, nonatomic) IBOutlet CircleImageView *uiImageProfile;
@property (weak, nonatomic) IBOutlet UIScrollView *uiScrollView;
@property (weak, nonatomic) IBOutlet MDCCard *uiViewEditProfile;
@property (weak, nonatomic) IBOutlet MDCCard *uiViewChangePayment;
@property (weak, nonatomic) IBOutlet MDCCard *uiViewChangePass;
@property (weak, nonatomic) IBOutlet MDCCard *uiViewChangePin;
@property (weak, nonatomic) IBOutlet MDCCard *uiViewCS;
@property (weak, nonatomic) IBOutlet MDCCard *uiViewFAQ;
@property (weak, nonatomic) IBOutlet MDCCard *uiViewTermCondition;
@property (weak, nonatomic) IBOutlet MDCCard *uiViewPrivacyPolicy;
@property (weak, nonatomic) IBOutlet MDCCard *uiViewLogout;
@property (weak, nonatomic) IBOutlet MDCCard *cardSaldo;
@property (weak, nonatomic) IBOutlet GRView *uiviewSaldo;
@property (weak, nonatomic) IBOutlet UIImageView *arrowHistory;
@end

@implementation AccountController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callCheckInternet];
    [self initView];
    
    UITapGestureRecognizer *pickerImage =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(pickerImage:)];
    [self.uiImageProfile setUserInteractionEnabled:YES];
    [self.uiImageProfile addGestureRecognizer:pickerImage];
    
    UITapGestureRecognizer *goProfile =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(goProfile:)];
    
    UITapGestureRecognizer *goChangePass =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(goChangePass:)];
    
    UITapGestureRecognizer *goChangePayment =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(goChangePayment:)];
    
    UITapGestureRecognizer *goChangePin =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(goChangePin:)];
    
    UITapGestureRecognizer *goCS =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(goCS:)];
    
    UITapGestureRecognizer *goRuleFAQ =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(goRuleFAQ:)];
    
    UITapGestureRecognizer *goRuleTermCondition =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(goRuleTermCondition:)];
    
    UITapGestureRecognizer *goRulePrivacyPolice =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(goRulePrivacyPolice:)];
    
    UITapGestureRecognizer *goLogout =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(goLogout:)];
    
    [self.uiViewEditProfile setUserInteractionEnabled:YES];
    [self.uiViewEditProfile addGestureRecognizer:goProfile];
    
    [self.uiViewChangePass setUserInteractionEnabled:YES];
    [self.uiViewChangePass addGestureRecognizer:goChangePass];
    
    [self.uiViewChangePayment setUserInteractionEnabled:YES];
    [self.uiViewChangePayment addGestureRecognizer:goChangePayment];
    
    [self.uiViewChangePin setUserInteractionEnabled:YES];
    [self.uiViewChangePin addGestureRecognizer:goChangePin];
    
    [self.uiViewCS setUserInteractionEnabled:YES];
    [self.uiViewCS addGestureRecognizer:goCS];
    
    [self.uiViewFAQ setUserInteractionEnabled:YES];
    [self.uiViewFAQ addGestureRecognizer:goRuleFAQ];
    
    [self.uiViewTermCondition setUserInteractionEnabled:YES];
    [self.uiViewTermCondition addGestureRecognizer:goRuleTermCondition];
    
    [self.uiViewPrivacyPolicy setUserInteractionEnabled:YES];
    [self.uiViewPrivacyPolicy addGestureRecognizer:goRulePrivacyPolice];
    
    [self.uiViewLogout setUserInteractionEnabled:YES];
    [self.uiViewLogout addGestureRecognizer:goLogout];
}

- (void)goProfile:(UITapGestureRecognizer *)recognizer {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EditProfileController *editprofile = [storyboard instantiateViewControllerWithIdentifier:@"EditProfile"];
    [self.navigationController pushViewController:editprofile animated:YES];
}

- (void)goChangePass:(UITapGestureRecognizer *)recognizer {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChangePassController *changepassword = [storyboard instantiateViewControllerWithIdentifier:@"ChangePass"];
    [self.navigationController pushViewController:changepassword animated:YES];
}

- (void)goChangePayment:(UITapGestureRecognizer *)recognizer {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ListPaymentController *changepassword = [storyboard instantiateViewControllerWithIdentifier:@"ListPayment"];
    [self.navigationController pushViewController:changepassword animated:YES];
}

- (void)goChangePin:(UITapGestureRecognizer *)recognizer {
    SCPinViewController *vc;
    SCPinAppearance *appearance = [SCPinAppearance defaultAppearance];
    appearance.numberButtonstrokeEnabled = NO;
    appearance.titleText = @"Enter PIN";
    appearance.cancelButtonText = @"Close";
    
    [SCPinViewController setNewAppearance:appearance];
    vc = [[SCPinViewController alloc] initWithScope:SCPinViewControllerScopeValidate];
    
    vc.dataSource = self;
    vc.validateDelegate = self;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)goCS:(UITapGestureRecognizer *)recognizer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CustomerSupportController *cs = [storyboard instantiateViewControllerWithIdentifier:@"CustomerSupport"];
    [self.navigationController pushViewController:cs animated:YES];
}

- (void)goRuleFAQ:(UITapGestureRecognizer *)recognizer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RulesController *rules= [storyboard instantiateViewControllerWithIdentifier:@"RulesController"];
    rules.page_title = @"FAQ";
    [self.navigationController pushViewController:rules animated:YES];
}

- (void)goRuleTermCondition:(UITapGestureRecognizer *)recognizer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RulesController *rules= [storyboard instantiateViewControllerWithIdentifier:@"RulesController"];
    rules.page_title = @"Term Conditions";
    [self.navigationController pushViewController:rules animated:YES];
}

- (void)goRulePrivacyPolice:(UITapGestureRecognizer *)recognizer{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RulesController *rules= [storyboard instantiateViewControllerWithIdentifier:@"RulesController"];
    rules.page_title = @"Privacy Policy";
    [self.navigationController pushViewController:rules animated:YES];
}

- (void)goLogout:(UITapGestureRecognizer *)recognizer{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Logout" message:@"Are you sure you want to logout?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *logout = [UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //button click event
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:cancel];
    [alert addAction:logout];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)callCheckInternet{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    NSString *remoteHostName = @"https://www.google.com/";
    NSString *remoteHostLabelFormatString = NSLocalizedString(@"Remote Host: %@", @"Remote host label format string");
    
    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
    [self.hostReachability startNotifier];
    [self updateInterfaceWithReachability:self.hostReachability];
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [self updateInterfaceWithReachability:self.internetReachability];
}

-(void)initView{
    [self.uiScrollView setShowsVerticalScrollIndicator:NO];

    self.cardSaldo.layer.cornerRadius = 4;
    self.cardSaldo.layer.masksToBounds = true;
    
    self.uiviewSaldo.layer.cornerRadius = 4;
    self.uiviewSaldo.layer.masksToBounds = true;
    
    [self.arrowHistory setTintColor:[UIColor whiteColor]];
}

-(void)setBackgroundGradient:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha{
    
    [mainView setBackgroundColor:[UIColor clearColor]];
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = mainView.bounds;
    
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:34.0/255.0 green:211/255.0 blue:198/255.0 alpha:1.0] CGColor],(id)[[UIColor colorWithRed:145/255.0 green:72.0/255.0 blue:203/255.0 alpha:1.0] CGColor], nil];
    
    [mainView.layer insertSublayer:grad atIndex:0];
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pickerImage:(UITapGestureRecognizer *)recognizer {
    [self pickerFoto];
}

-(void)pickerFoto{
    AHKActionSheet *actionSheet = [[AHKActionSheet alloc] initWithTitle:NSLocalizedString(@"Ammbil Foto", nil)];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Kamera", nil)
                              image:[UIImage imageNamed:@"baseline_photo_camera_black_24pt"]
                               type:AHKActionSheetButtonTypeDefault
                            handler:^(AHKActionSheet *as) {
                                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"File Exist" message:@" No Camera found." preferredStyle: UIAlertControllerStyleAlert];
                                    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                    }];
                                    
                                    [controller addAction:action];
                                    [self presentViewController:controller animated:YES completion:nil];
                                    return;
                                }
                                
                                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                picker.delegate = self;
                                picker.allowsEditing = YES;
                                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                [self presentViewController:picker animated:true completion:nil];
                            }];
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Galleri", nil)
                              image:[UIImage imageNamed:@"baseline_photo_library_black_24pt"]
                               type:AHKActionSheetButtonTypeDefault
                            handler:^(AHKActionSheet *as) {
                                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                picker.delegate = self;
                                picker.allowsEditing = YES;
                                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                [self presentViewController:picker animated:YES completion:NULL];
                            }];
    
    [actionSheet show];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    self.image = [chosenImage scaleToFitWidth:345.0f];
    self.image = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(345, 245)];
    self.uiImageProfile.image = self.image;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)checkInternetConnection:(BOOL)connectionstatus{
    if(connectionstatus){
        _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_no_internet_satellite"]];
        _imageView.frame = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2, 200, 200);
        _imageView.center = CGPointMake(self.view.frame.size.width/2,
                                        self.view.frame.size.height/2);
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        
        CGRect frame = CGRectMake(0, 0, 245, 20);
        _internetstatus = [[UILabel alloc] initWithFrame:frame];
        _internetstatus.center = CGPointMake(self.view.frame.size.width/2, self.view.bounds.size.height/1.5f);
        _internetstatus.text = @"Oops! No Internet Connection";
        _internetstatus.textAlignment = NSTextAlignmentJustified;
        _internetstatus.textColor = [UIColor grayColor];
        [_internetstatus setFont:[UIFont boldSystemFontOfSize:17]];
        
        [self.view addSubview:_imageView];
        [self.view addSubview:_internetstatus];
    }else{
        [_imageView removeFromSuperview];
        [_internetstatus removeFromSuperview];
    }
}

- (void) reachabilityChanged:(NSNotification *)note{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}

- (void)updateInterfaceWithReachability:(Reachability *)reachability{
    if (reachability == self.hostReachability){
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        BOOL connectionRequired = [reachability connectionRequired];
        if (connectionRequired){
            
        }else{
            
        }
    }
    
    if (reachability == self.internetReachability){
        [self configureTextField:reachability];
    }
}

- (void)configureTextField:(Reachability *)reachability{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    BOOL connectionRequired = [reachability connectionRequired];
    NSString* statusString = @"";
    
    switch (netStatus){
        case NotReachable:{
            self.uiScrollView.hidden = YES;
            [self checkInternetConnection:YES];
            connectionRequired = NO;
            break;
        }case ReachableViaWWAN:{
            self.uiScrollView.hidden = NO;
            [self checkInternetConnection:NO];
            break;
        }case ReachableViaWiFi:{
            self.uiScrollView.hidden = NO;
            [self checkInternetConnection:NO];
            break;
        }
    }
    
    if (connectionRequired){
        NSString *connectionRequiredFormatString = NSLocalizedString(@"%@, Connection Required", @"Concatenation of status string with connection requirement");
        statusString= [NSString stringWithFormat:connectionRequiredFormatString, statusString];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

-(void)cancel {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)pinViewController:(SCPinViewController *)pinViewController didSetNewPin:(NSString *)pin {
    NSLog(@"pinViewController: %@",pinViewController);
    [[NSUserDefaults standardUserDefaults] setObject:pin forKey:kViewControllerPin];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)lengthForPin {
    return 6;
}

-(NSString *)codeForPinViewController:(SCPinViewController *)pinViewController {
    NSString *pin = [[NSUserDefaults standardUserDefaults] objectForKey:kViewControllerPin];
    return pin;
}

-(BOOL)hideTouchIDButtonIfFingersAreNotEnrolled {
    return YES;
}

-(BOOL)showTouchIDVerificationImmediately {
    return NO;
}

-(void)pinViewControllerDidSetWrongPin:(SCPinViewController *)pinViewController {
    
}

-(void)pinViewControllerDidSetСorrectPin:(SCPinViewController *)pinViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
    SCPinAppearance *appearance = [SCPinAppearance defaultAppearance];
    SCPinViewController *vc;

    appearance.titleText = @"Create PIN";
    [SCPinViewController setNewAppearance:appearance];
    vc = [[SCPinViewController alloc] initWithScope:SCPinViewControllerScopeCreate];

    vc.createDelegate = self;
    UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:vc];
    [navigation.navigationBar setTranslucent:NO];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    [vc.navigationItem setLeftBarButtonItems:@[item]];
    [self presentViewController:navigation animated:YES completion:nil];
    
}

- (void)pinViewControllerDidCancel:(SCPinViewController *)pinViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

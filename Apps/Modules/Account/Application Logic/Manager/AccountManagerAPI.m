//
//  AccountManagerAPI.m
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "AccountManagerAPI.h"
#import "NetworkManager+User.h"

@interface AccountManagerAPI ()
@end

@implementation AccountManagerAPI

- (void)getUserList:(NSDictionary *)params completion:(void (^)(NSArray *, NSError *))completion {
    [NetworkManager getUserList:params successBlock:^(NSArray *listUser) {
        completion(listUser, nil);
    } failureBlock:^(NSError *error) {
        completion(nil, error);
    }];
}

@end

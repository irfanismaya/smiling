//
//  AccountInteractor.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "AccountModuleProtocol.h"
@class AccountManagerAPI;

@interface AccountInteractor : NSObject <AccountInteractorInput>

- (instancetype)initWithAccountManager:(AccountManagerAPI *)accountManagerAPI;

@property (nonatomic, weak) id <AccountInteractorOutput> accountPresenter;

@end

//
//  AccountInteractor.m
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "AccountInteractor.h"
#import "AccountManagerAPI.h"

@interface AccountInteractor ()

@property (nonatomic, strong) AccountManagerAPI *accountManagerAPI;

@end

@implementation AccountInteractor

- (instancetype)initWithAccountManager:(AccountManagerAPI *)accountManagerAPI
{
    if ((self = [super init]))
    {
        _accountManagerAPI = accountManagerAPI;
    }
    
    return self;
}

#pragma mark - AccountInteractorInput
- (void)getUserList:(NSDictionary *)params {
    __weak typeof(self) weakSelf = self;
    
    [self.accountManagerAPI getUserList:params completion:^(NSArray *listUser, NSError *error) {
        [weakSelf.accountPresenter didGetUserList:listUser error:error];
    }];
}

@end


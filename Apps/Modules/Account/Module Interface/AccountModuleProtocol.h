//
//  AccountModuleInterface.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

//khusus buat presenter
@protocol AccountPresenterProtocol <NSObject>
- (void)doGetUserList:(NSInteger)page;
@end

//khusus buat interactor
@protocol AccountInteractorInput <NSObject>
- (void)getUserList:(NSDictionary *)params;
@end

@protocol AccountInteractorOutput <NSObject>
- (void)didGetUserList:(NSArray *)listUser error:(NSError *)error;
@end

@protocol AccountManagerAPIInput <NSObject>
- (void)getUserList:(NSDictionary *)params completion:(void (^)(NSArray *listUser, NSError *error))completion;
@end

//khusus buat view
@protocol AccountViewProtocol <NSObject>
- (void)showLoading;
- (void)hideLoading;
@end

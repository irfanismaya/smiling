//  CulinaryController.h
//  Smiling
//  Created by Irfan-Ismaya on 22/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
#import "KIImagePager.h"
NS_ASSUME_NONNULL_BEGIN

@interface CulinaryController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,KIImagePagerDelegate,KIImagePagerDataSource,KIImagePagerImageSource>

@end

NS_ASSUME_NONNULL_END

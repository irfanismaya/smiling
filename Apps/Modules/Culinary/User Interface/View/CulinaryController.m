//  CulinaryController.m
//  Smiling
//  Created by Irfan-Ismaya on 22/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "CulinaryController.h"
#import "CulinaryHeader.h"
#import "CulinaryCollectionCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DetailCulinaryController.h"

@interface CulinaryController ()
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *uiViewFilter;
@property (weak, nonatomic) IBOutlet UIImageView *imgBack;
@property (weak, nonatomic) IBOutlet UIImageView *imgFilter;
@property (weak, nonatomic) IBOutlet UICollectionView *culinaryCollection;
@end

NSArray * culinaryGallery;
NSArray * imgCul;
static CulinaryHeader *headerView;
@implementation CulinaryController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initViewHeader];
    
    UITapGestureRecognizer *actionBack =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionBack:)];
    [self.imgBack setUserInteractionEnabled:YES];
    [self.imgBack addGestureRecognizer:actionBack];
    
    culinaryGallery = [[NSArray alloc] initWithObjects:
                   [UIImage imageNamed:@"bg1@2x.png"],
                   [UIImage imageNamed:@"bg2@2x.png"],
                   [UIImage imageNamed:@"bg3@2x.png"],
                   [UIImage imageNamed:@"bg4@2x.png"],nil];
    
    imgCul = @[@"https://cdn.shopify.com/s/files/1/1228/3124/products/photo_porterhouse_steak_bdbaef5e-7c42-4893-ae30-ea77646eb48f_1024x1024.jpg?v=1463010050", @"https://img.okezone.com/content/2016/10/22/298/1521574/ayam-bakar-sambal-petis-sudah-lezat-pedasnya-menggoda-x2edZIIwHD.jpg", @"http://cdn2.tstatic.net/jatim/foto/bank/images/ilustrasi-sate-kambing-2_20170901_142551.jpg", @"https://cdn.idntimes.com/content-images/community/2018/04/resep-es-krim-rumahan-dbf7bd137b8c817483e299b60f989923_600x400.jpg"];
}

-(void)initViewHeader{
    for (UIView *subview in self.searchBar.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
            break;
        }
        for (UIView *subsubview in subview.subviews) {
            if ([subsubview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [subsubview removeFromSuperview];
                break;
            }
        }
    }
    
    self.imgBack.tintColor = [UIColor blackColor];
    self.imgFilter.tintColor = [UIColor grayColor];
    self.uiViewFilter.layer.cornerRadius = 10;
    self.searchBar.barTintColor = [UIColor grayColor];
    self.searchBar.layer.borderColor = [UIColor grayColor].CGColor;
    self.searchBar.backgroundColor = [UIColor whiteColor];
    
    UITextField *searchField = [self.searchBar valueForKey:@"searchField"];
    searchField.backgroundColor = [UIColor colorWithHexString:@"#F6F9FF"];
    searchField.textColor = [UIColor grayColor];
    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Cari Culinary"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor grayColor];
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return culinaryGallery.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CulinaryHeader" forIndexPath:indexPath];
        headerView.imageCulinary.delegate = self;
        headerView.imageCulinary.dataSource = self;
        headerView.imageCulinary.slideshowTimeInterval = 4.5f;
        headerView.imageCulinary.clipsToBounds = true;
        headerView.imageCulinary.layer.cornerRadius = 10;
        reusableview = headerView;
    }
    
    return reusableview;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DetailCulinaryController *detailCulinary = [storyboard instantiateViewControllerWithIdentifier:@"DetailCulinaryController"];
    [self.navigationController pushViewController:detailCulinary animated:YES];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CulinaryCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CulinaryCollectionCell" forIndexPath:indexPath];
    
    cell.uiView.clipsToBounds = true;
    cell.uiView.layer.cornerRadius = 10;
    cell.uiView.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMinXMinYCorner;
    
    [cell.imgCulinary sd_setImageWithURL:[NSURL URLWithString:imgCul[indexPath.row]]
                         placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
    cell.imgCulinary.clipsToBounds = true;
    cell.imgCulinary.layer.cornerRadius = 10;
    cell.imgCulinary.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMinXMinYCorner;
    
    cell.uiView.clipsToBounds = true;
    cell.uiView.layer.cornerRadius = 10;
    cell.uiView.layer.maskedCorners = kCALayerMaxXMinYCorner | kCALayerMaxXMaxYCorner;
    
    cell.uiViewDate.clipsToBounds = true;
    cell.uiViewDate.layer.cornerRadius = 10;
    cell.uiViewDate.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner;
    
    cell.btnPrice.clipsToBounds = true;
    cell.btnPrice.layer.cornerRadius = 15.f;
    
    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

#pragma mark - KIImagePager DataSource
- (NSArray *) arrayWithImages:(KIImagePager*)pager
{
    return @[
             @"http://m.jcodonuts.com/assets/img/slider/jcoph_201603_2.jpg",
@"https://imgix.bustle.com/rehost/2016/9/14/0821b285-1a49-4f26-900b-9b17fb76d763.JPG?w=970&h=546&fit=crop&crop=faces&auto=format&q=70",
    @"http://www.thegatenewcastle.co.uk/images/tenants/headers/pizza-hut.jpg?action=thumbnail&width=1366"
             ];
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager *)pager
{
    return UIViewContentModeScaleAspectFill;
}

#pragma mark - KIImagePager Delegate
- (void) imagePager:(KIImagePager *)imagePager didScrollToIndex:(NSUInteger)index{
    
}

- (void) imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index{
    
}

@end

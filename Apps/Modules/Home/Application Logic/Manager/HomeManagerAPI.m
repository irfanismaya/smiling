//  HomeManagerAPI.m
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "HomeManagerAPI.h"
#import "NetworkManager+User.h"
#import "NetworkManager+City.h"
#import "NetworkManager+Article.h"
#import "NetworkManager+Promotion.h"

@interface HomeManagerAPI ()
@end

@implementation HomeManagerAPI

- (void)getUserList:(NSInteger)page completion:(void (^)(NSArray *, NSError *))completion {
    [NetworkManager getUserList:page successBlock:^(NSArray *listUser) {
        completion(listUser, nil);
    } failureBlock:^(NSError *error) {
        completion(nil, error);
    }];
}

- (void)getCityList:(void (^)(NSArray *, NSError *))completion {
    [NetworkManager getCityList:^(NSArray *listCity) {
        completion(listCity, nil);
    } failureBlock:^(NSError *error) {
        completion(nil, error);
    }];
}

- (void)getArticleList:(NSInteger)page completion:(void (^)(NSArray *, NSError *))completion {
    [NetworkManager getArticleList:page successBlock:^(NSArray *listArticle) {
        completion(listArticle, nil);
    } failureBlock:^(NSError *error) {
        completion(nil, error);
    }];
}

- (void)getPromotionList:(NSInteger)page completion:(void (^)(NSArray *, NSError *))completion {
    [NetworkManager getPromotionList:page successBlock:^(NSArray *listPromotion) {
        completion(listPromotion, nil);
    } failureBlock:^(NSError *error) {
        completion(nil, error);
    }];
}

@end

//  HomeInteractor.h
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.


#import "HomeModuleProtocol.h"
@class HomeManagerAPI;

@interface HomeInteractor : NSObject <HomeInteractorInput>

- (instancetype)initWithHomeManager:(HomeManagerAPI *)homeManagerAPI;

@property (nonatomic, weak) id <HomeInteractorOutput> homePresenter;

@end

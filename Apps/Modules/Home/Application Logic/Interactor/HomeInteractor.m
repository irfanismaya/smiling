//  HomeInteractor.m
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "HomeInteractor.h"
#import "HomeManagerAPI.h"

@interface HomeInteractor ()
@property (nonatomic, strong) HomeManagerAPI *homeManagerAPI;
@end

@implementation HomeInteractor

- (instancetype)initWithHomeManager:(HomeManagerAPI *)homeManagerAPI
{
    if ((self = [super init]))
    {
        _homeManagerAPI = homeManagerAPI;
    }
    
    return self;
}

#pragma mark - HomeInteractorInput
- (void)getUserList:(NSInteger)page {
    __weak typeof(self) weakSelf = self;
    [self.homeManagerAPI getUserList:page completion:^(NSArray *listUser, NSError *error) {
        [weakSelf.homePresenter didGetUserList:listUser error:error];
    }];
}

- (void)getCityList {
    __weak typeof(self) weakSelf = self;
    [self.homeManagerAPI getCityList:^(NSArray *listCity, NSError *error) {
        [weakSelf.homePresenter didGetCityList:listCity error:error];
    }];
}

- (void)getArticleList:(NSInteger)page {
    __weak typeof(self) weakSelf = self;
    [self.homeManagerAPI getArticleList:page completion:^(NSArray *listArticle, NSError *error) {
        [weakSelf.homePresenter didGetArticleList:listArticle error:error];
    }];
}


@end

//
//  HomePresenter.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "HomeModuleProtocol.h"
#import "HomeWireframe.h"

@interface HomePresenter : NSObject <HomePresenterProtocol, HomeInteractorOutput>

@property (nonatomic, strong) HomeWireframe* homeWireframe;
@property (nonatomic, strong) UIViewController<HomeViewProtocol> *homeController;

@property (nonatomic, strong) id <HomeInteractorInput> homeInteractor;

@end

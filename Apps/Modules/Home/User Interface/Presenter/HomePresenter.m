//
//  HomePresenter.m
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "HomePresenter.h"

@implementation HomePresenter

#pragma mark - HomePresenterProtocol
- (void)doGetUserList:(NSInteger)page {
    [self.homeController showLoading];
    [self.homeInteractor getUserList:page];
}

- (void)doGetArticleList:(NSInteger)page {
    [self.homeController showLoadingArticle];
    [self.homeInteractor getArticleList:page];
}

- (void)doGetCityList {
    [self.homeController showLoading];
    [self.homeInteractor getCityList];
}

- (void)goToArticleView {
    [self.homeWireframe goToArticleView];
}

#pragma mark - HomeInteractorOutput
- (void)didGetUserList:(NSArray *)listUser error:(NSError *)error {
    [self.homeController hideLoading];
}

- (void)didGetArticleList:(NSArray *)listArticle error:(NSError *)error {
    [self.homeController setArticle:listArticle];
    [self.homeController hideLoadingArticle];
}

- (void)didGetCityList:(NSArray *)listCity error:(NSError *)error {
    [self.homeController setCity:listCity];
    [self.homeController hideLoading];
}

@end

//  HomeController.m
//  Smiling
//  Created by Irfan-Ismaya on 04/04/19.
//  Copyright © 2019 Merqurion Solution. All rights reserved.

#import "HomeController.h"
#import "Constants.h"
#import "UIColor+HexString.h"
#import "EventCollectionCell.h"
#import "AmenitiesCollectionCell.h"
#import "TourismAtrcCollectionCell.h"
#import "VideoCollectionCell.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
@import GooglePlaces;
@import GoogleMaps;

#import <GooglePlacePicker/GooglePlacePicker.h>
#import <GooglePlaces/GooglePlaces.h>
#import "SearchStringPickerViewController.h"
#import "CulinaryController.h"
#import "HotelController.h"
#import "FBShimmeringView.h"
#import "Somo.h"

#import "Article.h"

@interface HomeController ()
@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *internetstatus;

//Article
@property (nonatomic, strong) UIView *viewLoadingArticle;
@property (nonatomic, strong) MDCActivityIndicator *activityIndicatorArticle;
@property (weak, nonatomic) IBOutlet UIView *viewArticle;
@property (weak, nonatomic) IBOutlet UIImageView *imgOne;
@property (weak, nonatomic) IBOutlet UIImageView *imgTwo;
@property (weak, nonatomic) IBOutlet UIImageView *imgThree;
@property (weak, nonatomic) IBOutlet UILabel *lblOne;
@property (weak, nonatomic) IBOutlet UILabel *lblTwo;
@property (weak, nonatomic) IBOutlet UILabel *lblThree;


@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;

//Promo
@property (nonatomic, strong) UIView *viewLoadingPromo;
@property (nonatomic, strong) MDCActivityIndicator *activityIndicatorPromo;
@property (weak, nonatomic) IBOutlet UIView *viewPromo;
@property (weak, nonatomic) IBOutlet KIImagePager *imagePromo;

@end
NSArray *iconMenuEvent;
NSArray *nameMenuEvent;
NSArray *nameMenuTourism;
NSArray *colorMenuEvent;
NSArray *iconMenuAmenities;
NSArray *nameMenuAmenities;
NSArray *colorMenuAmenities;
NSArray *imgVideo;
NSArray *videoCode;

NSArray *imgTourism;
NSArray *imgStory;
NSArray *cityName;

@implementation HomeController{
    GMSPlacesClient *_placesClient;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self callCheckInternet];
    _placesClient = [GMSPlacesClient sharedClient];
    [self initData];
    [self initView];
    [self initLoading];
    
    [self.homePresenter doGetUserList:1];
    [self.homePresenter doGetCityList];
    [self.homePresenter doGetArticleList:1];
    
    UITapGestureRecognizer *chooseCity =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(chooseCity:)];
    [self.uiViewLocation setUserInteractionEnabled:YES];
    [self.uiViewLocation addGestureRecognizer:chooseCity];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.imagePromo.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    self.imagePromo.pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:primary_color_purple];
    self.imagePromo.slideshowTimeInterval = 5.5f;
    self.imagePromo.imageCounterDisabled = YES;
    self.imagePromo.slideshowShouldCallScrollToDelegate = YES;
}

- (NSArray *) arrayWithImages:(KIImagePager*)pager{
    return @[@"https://4.bp.blogspot.com/-nuERPyxzWLg/VxTLBpp7JVI/AAAAAAAABAQ/7oWRuO0z_CsZGvOOE7WByZj7G8oX-8FRACLcB/s1600/11.jpg", @"https://mmc.tirto.id/image/otf/500x0/2017/05/26/Sirup-marjan_ratio-16x9.JPG", @"https://jurnalmanajemen.com/wp-content/uploads/2019/02/33.-contoh-iklan-minuman.jpg", @"https://4.bp.blogspot.com/-RzgZ6YYegdc/WtwcyKOEkXI/AAAAAAAAPrg/bQM1nkQVjIAHdWk5hABF4rI3CaIWsANpQCLcBGAs/s1600/iklan_niaga.jpg",
             @"https://id-static.z-dn.net/files/de4/598e8a1c60362411b5b26009114ed8fa.jpg"];
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager*)pager{
    return UIViewContentModeScaleAspectFill;
}

#pragma mark - KIImagePager Delegate
- (void) imagePager:(KIImagePager *)imagePager didScrollToIndex:(NSUInteger)index{
    
}

- (void) imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index{
    
}

-(void)initData{
    imgVideo = [[NSArray alloc] initWithObjects:
                [UIImage imageNamed:@"bg1@2x.png"],
                [UIImage imageNamed:@"bg2@2x.png"],
                [UIImage imageNamed:@"bg3@2x.png"],
                [UIImage imageNamed:@"bg4@2x.png"],nil];
    
    imgTourism = @[@"https://tempatwisataseru.com/wp-content/uploads/2015/10/Curug-Cikaso.jpg", @"https://v-images2.antarafoto.com/festival-seni-budaya-jawa-barat-oyl651-prv.jpg", @"http://alienco.net/wp-content/uploads/2016/01/hiburan-rakyat-jawa-barat-kuda-lumping-640x402.jpg", @"https://media.suara.com/pictures/653x366/2018/12/14/37976-kemenpar.jpg"];
    
    imgStory = @[@"https://asset.kompas.com/crop/4x3:1000x667/750x500/data/photo/2018/08/02/2116057339.JPG", @"https://asset.kompas.com/crop/32x0:837x537/750x500/data/photo/2018/12/17/3024708800.jpeg", @"https://asset.kompas.com/crop/0x0:780x390/780x390/data/photo/2015/01/27/1354006Tanakita-5780x390.jpg",
    @"https://asset.kompas.com/crop/0x0:780x390/780x390/data/photo/2015/01/27/1354006Tanakita-5780x390.jpg"];
    
    iconMenuEvent  = [[NSArray alloc] initWithObjects:
                      [UIImage imageNamed:@"baseline_airplanemode_active_white_24pt"],
                      [UIImage imageNamed:@"baseline_queue_music_white_24pt"],
                      [UIImage imageNamed:@"baseline_directions_bike_white_24pt"],
                      [UIImage imageNamed:@"baseline_more_horiz_white_24pt"],nil];
    
    cityName  = @[@"BANDUNG", @"BOGOR", @"CIREBON", @"KUNINGAN", @"CIREBON", @"MAJALENGKA", @"PURWAKARTA", @"SUBANG", @"CIMAHI"];
    nameMenuEvent  = @[@"Seni", @"Musik", @"Olahraga", @"Lain-Lain"];
    nameMenuTourism  = @[@"Alam", @"Atraksi", @"Budaya", @"Religi"];
    colorMenuEvent = @[@"#D300EF", @"#F9931A", @"#FBD52B", @"#FC0071"];
    videoCode      = @[@"fKNdoxRld34", @"y12f7NxFgkw", @"oeGXqTI2PH8", @"AFXt7BLB0IM"];
    
    iconMenuAmenities  = [[NSArray alloc] initWithObjects:
                          [UIImage imageNamed:@"baseline_fastfood_white_24pt"],
                          [UIImage imageNamed:@"baseline_hotel_white_24pt"],
                          [UIImage imageNamed:@"baseline_airport_shuttle_white_24pt"],
                          [UIImage imageNamed:@"baseline_more_horiz_white_24pt"],nil];
    
    nameMenuAmenities  = @[@"Kuliner", @"Hotel", @"Transport", @"Lain-Lain"];
    colorMenuAmenities = @[@"#77CC29", @"#F56E63", @"#56ABFD", @"#71D9FC"];
    
    [self.imgOne sd_setImageWithURL:[NSURL URLWithString:@"http://anekatempatwisata.com/wp-content/uploads/2017/07/Bukit-Panembongan-Kuningan.jpg"]
                       placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
    
    [self.imgTwo sd_setImageWithURL:[NSURL URLWithString:@"http://anekatempatwisata.com/wp-content/uploads/2017/07/D%E2%80%99Jungle-Private-Camp-Puncak-Bogor.jpg"]
                   placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
    
    [self.imgThree sd_setImageWithURL:[NSURL URLWithString:@"http://anekatempatwisata.com/wp-content/uploads/2017/07/Kampung-Karuhun-Sumedang.jpg"]
                   placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
    
    [self.imgProfile sd_setImageWithURL:[NSURL URLWithString:@"https://cdn.soccerladuma.co.za/cms2/image_manager/uploads/News/653459/7/1556008610_cc033.jpg"]
                     placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
}

- (void)chooseCity:(UITapGestureRecognizer *)recognizer {
    [SearchStringPickerViewController showPickerWithTitle:@"Choose City"
                                                     rows:cityName
                                         initialSelection:[cityName indexOfObject:self.lblCity.text]
                                               sourceView:nil
                                                doneBlock:^(NSInteger selectedIndex, NSString *selectedValue) {
                                                    [self.lblCity setText:selectedValue];
                                                }
                                              cancelBlock:nil
                                presentFromViewController:self];
}

-(void)callCheckInternet{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    NSString *remoteHostName = @"https://www.google.com/";
    NSString *remoteHostLabelFormatString = NSLocalizedString(@"Remote Host: %@", @"Remote host label format string");
    
    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
    [self.hostReachability startNotifier];
    [self updateInterfaceWithReachability:self.hostReachability];
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [self updateInterfaceWithReachability:self.internetReachability];
}

-(void)initView{
    [self.uiScrollView setShowsVerticalScrollIndicator:NO];
    [self.eventCollection setShowsHorizontalScrollIndicator:NO];
    [self.amenitiesCollection setShowsHorizontalScrollIndicator:NO];
    [self.videoCollection setShowsHorizontalScrollIndicator:NO];
    [self.tourismCollection setShowsHorizontalScrollIndicator:NO];
    
    self.imagePromo.layer.cornerRadius = 10;
    self.imagePromo.layer.masksToBounds = true;
    
    self.tourismCollection.layer.cornerRadius = 8;
    self.tourismCollection.layer.masksToBounds = true;
    
    self.imageViewFirst.layer.cornerRadius = 4;
    self.imageViewFirst.layer.masksToBounds = true;
    
    self.imageViewTwo.layer.cornerRadius = 4;
    self.imageViewTwo.layer.masksToBounds = true;
    
    self.imageViewThree.layer.cornerRadius = 4;
    self.imageViewThree.layer.masksToBounds = true;
    
    self.imageProfile.layer.cornerRadius = 15;
    self.imageProfile.layer.masksToBounds = true;
    
    self.uiViewLocation.layer.cornerRadius = 17;
    self.uiViewLocation.layer.masksToBounds = true;
}

-(void)initLoading {
    _viewLoadingPromo = [UIUtils initLoadingView:_viewPromo];
    _activityIndicatorPromo = [UIUtils initLoadingIndicatorView:_viewLoadingPromo];
    
    _viewLoadingArticle = [UIUtils initLoadingView:_viewArticle];
    _activityIndicatorArticle = [UIUtils initLoadingIndicatorView:_viewLoadingArticle];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(collectionView == self.eventCollection){
      return iconMenuEvent.count;
    }else if(collectionView == self.amenitiesCollection){
      return iconMenuAmenities.count;
    }else if(collectionView == self.tourismCollection){
        return iconMenuAmenities.count;
    }else{
        return imgVideo.count;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if(collectionView == self.eventCollection){
        return 10;
    }else if(collectionView == self.amenitiesCollection){
        return 10;
    }else if(collectionView == self.tourismCollection){
        return -4;
    }else{
        return 15;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView == self.eventCollection){
        EventCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EventCollectionCell" forIndexPath:indexPath];
        cell.uiImageEvent.image = iconMenuEvent[indexPath.row];
        cell.uiViewEvent.layer.cornerRadius = 14;
        cell.uiViewEvent.backgroundColor = [UIColor colorWithHexString:colorMenuEvent[indexPath.row]];
        cell.uiLabelEvent.text = nameMenuEvent[indexPath.row];
        return cell;
    }else if(collectionView == self.amenitiesCollection){
        AmenitiesCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AmenitiesCollectionCell" forIndexPath:indexPath];
        cell.uiImageAmenities.image = iconMenuAmenities[indexPath.row];
        cell.uiViewAmenities.layer.cornerRadius = 14;
        cell.uiViewAmenities.backgroundColor = [UIColor colorWithHexString:colorMenuAmenities[indexPath.row]];
        cell.uiLabelAmenities.text = nameMenuAmenities[indexPath.row];
        return cell;
    }else if(collectionView == self.tourismCollection){
        TourismAtrcCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TourismAtrcCollectionCell" forIndexPath:indexPath];
        [cell.uiImageTourism sd_setImageWithURL:[NSURL URLWithString:imgTourism[indexPath.row]]
                           placeholderImage:[UIImage imageNamed:@"logo_smiling.png"]];
        cell.uiImageTourism.clipsToBounds = true;
        cell.uiImageTourism.layer.cornerRadius = 10;
        cell.uiImageTourism.layer.maskedCorners = kCALayerMinXMinYCorner | kCALayerMaxXMinYCorner;
        cell.imgIcon.tintColor = [UIColor whiteColor];
        cell.uiViewTourism.clipsToBounds = true;
        cell.uiViewTourism.layer.cornerRadius = 10;
        cell.uiViewTourism.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner;
        
        cell.uiViewTourism.backgroundColor = [UIColor whiteColor];
        [cell.uiViewTourism.layer setCornerRadius:10.0f];
        [cell.uiViewTourism.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [cell.uiViewTourism.layer setBorderWidth:0.2f];
        [cell.uiViewTourism.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
        [cell.uiViewTourism.layer setShadowOpacity:1.0];
        [cell.uiViewTourism.layer setShadowRadius:5.0];
        [cell.uiViewTourism.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
        
        cell.uiLabelTourism.text = nameMenuTourism[indexPath.row];
        cell.uiLabelTourism.textColor = [UIColor grayColor];
        return cell;
    }else{
        VideoCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VideoCollectionCell" forIndexPath:indexPath];
        [cell.imageVideo sd_setImageWithURL:[NSURL URLWithString:imgStory[indexPath.row]]
                           placeholderImage:[UIImage imageNamed:@"logo_smiling.png"]];
        cell.imageVideo.clipsToBounds = true;
        cell.imageVideo.layer.cornerRadius = 10;
        cell.imageVideo.layer.maskedCorners = kCALayerMinXMinYCorner | kCALayerMaxXMinYCorner;
        cell.imgIcon.tintColor = [UIColor grayColor];
        cell.uiViewContent.clipsToBounds = true;
        cell.uiViewContent.layer.cornerRadius = 10;
        cell.uiViewContent.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner;
        cell.uiViewContent.backgroundColor = [UIColor whiteColor];
        [cell.uiViewContent.layer setCornerRadius:10.0f];
        [cell.uiViewContent.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [cell.uiViewContent.layer setBorderWidth:0.2f];
        [cell.uiViewContent.layer setShadowColor:[UIColor colorWithRed:225.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0].CGColor];
        [cell.uiViewContent.layer setShadowOpacity:1.0];
        [cell.uiViewContent.layer setShadowRadius:5.0];
        [cell.uiViewContent.layer setShadowOffset:CGSizeMake(5.0f, 5.0f)];
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == self.eventCollection){
        
    }else if(collectionView == self.amenitiesCollection){
        if(indexPath.row == 0){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            CulinaryController *culinary = [storyboard instantiateViewControllerWithIdentifier:@"CulinaryController"];
            [self.navigationController pushViewController:culinary animated:YES];
        }else if(indexPath.row == 1){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            HotelController *hotel = [storyboard instantiateViewControllerWithIdentifier:@"HotelController"];
            [self.navigationController pushViewController:hotel animated:YES];
        }
    }else if(collectionView == self.tourismCollection){
        
    }else{
        [self playVideo:videoCode[indexPath.row]];
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if(collectionView == self.eventCollection){
        return 1;
    }else if(collectionView == self.amenitiesCollection){
        return 1;
    }else if(collectionView == self.tourismCollection){
        return 1;
    }else{
        return 1;
    }
}

- (void) playVideo:(NSString *)code
{
    XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:code];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewController.moviePlayer];
    [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
}

- (void) moviePlayerPlaybackDidFinish:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:notification.object];
    MPMovieFinishReason finishReason = [notification.userInfo[MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] integerValue];
    if (finishReason == MPMovieFinishReasonPlaybackError)
    {
        NSError *error = notification.userInfo[XCDMoviePlayerPlaybackDidFinishErrorUserInfoKey];
        // Handle error
    }
}
- (IBAction)getCurrentPlace:(UIButton *)sender {
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
    GMSPlacePickerViewController *placePicker =
    [[GMSPlacePickerViewController alloc] initWithConfig:config];
    placePicker.delegate = self;
    
    [self presentViewController:placePicker animated:YES completion:nil];
}

- (void)placePicker:(GMSPlacePickerViewController *)viewController didPickPlace:(GMSPlace *)place {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
}

- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"No place selected");
}

-(void)checkInternetConnection:(BOOL)connectionstatus{
    if(connectionstatus){
        _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_no_internet_satellite"]];
        _imageView.frame = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2, 200, 200);
        _imageView.center = CGPointMake(self.view.frame.size.width/2,
                                        self.view.frame.size.height/2);
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        
        CGRect frame = CGRectMake(0, 0, 245, 20);
        _internetstatus = [[UILabel alloc] initWithFrame:frame];
        _internetstatus.center = CGPointMake(self.view.frame.size.width/2, self.view.bounds.size.height/1.5f);
        _internetstatus.text = @"Oops! No Internet Connection";
        _internetstatus.textAlignment = NSTextAlignmentJustified;
        _internetstatus.textColor = [UIColor grayColor];
        [_internetstatus setFont:[UIFont boldSystemFontOfSize:17]];
        
        [self.view addSubview:_imageView];
        [self.view addSubview:_internetstatus];
    }else{
        [_imageView removeFromSuperview];
        [_internetstatus removeFromSuperview];
    }
}

- (void) reachabilityChanged:(NSNotification *)note{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}

- (void)updateInterfaceWithReachability:(Reachability *)reachability{
    if (reachability == self.hostReachability){
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        BOOL connectionRequired = [reachability connectionRequired];
        if (connectionRequired){
            
        }else{
            
        }
    }
    
    if (reachability == self.internetReachability){
        [self configureTextField:reachability];
    }
}

- (void)configureTextField:(Reachability *)reachability{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    BOOL connectionRequired = [reachability connectionRequired];
    NSString* statusString = @"";
    
    switch (netStatus){
        case NotReachable:{
            self.uiScrollView.hidden = YES;
            [self checkInternetConnection:YES];
            connectionRequired = NO;
            break;
        }case ReachableViaWWAN:{
            self.uiScrollView.hidden = NO;
            [self checkInternetConnection:NO];
            break;
        }case ReachableViaWiFi:{
            self.uiScrollView.hidden = NO;
            [self checkInternetConnection:NO];
            break;
        }
    }
    
    if (connectionRequired){
        NSString *connectionRequiredFormatString = NSLocalizedString(@"%@, Connection Required", @"Concatenation of status string with connection requirement");
        statusString= [NSString stringWithFormat:connectionRequiredFormatString, statusString];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

#pragma mark - BaseViewProtocol
- (void)connectionHideView {
    self.uiScrollView.hidden = YES;
}

- (void)connectionShowView {
    self.uiScrollView.hidden = NO;
}

#pragma mark - HomeViewProtocol
- (void)showLoading {
    _uiScrollView.hidden = YES;
    [super showLoading];
}

- (void)hideLoading {
    _uiScrollView.hidden = NO;
    [super hideLoading];
}

- (void)showLoadingPromo {
    _imagePromo.hidden = YES;
    
    [_viewLoadingPromo addSubview:_activityIndicatorPromo];
    _viewLoadingPromo.hidden = NO;
    [_activityIndicatorPromo startAnimating];
}

- (void)hideLoadingPromo {
    _imagePromo.hidden = NO;
    
    _viewLoadingPromo.hidden = YES;
    [_activityIndicatorPromo stopAnimating];
}

- (void)showLoadingArticle {
    _imgOne.hidden = YES;
    _imgTwo.hidden = YES;
    _imgThree.hidden = YES;
    
    _lblOne.hidden = YES;
    _lblTwo.hidden = YES;
    _lblThree.hidden = YES;
    
    [_viewLoadingArticle addSubview:_activityIndicatorArticle];
    _viewLoadingArticle.hidden = NO;
    [_activityIndicatorArticle startAnimating];
}

- (void)hideLoadingArticle {
    _imgOne.hidden = NO;
    _imgTwo.hidden = NO;
    _imgThree.hidden = NO;
    
    _lblOne.hidden = NO;
    _lblTwo.hidden = NO;
    _lblThree.hidden = NO;
    
    _viewLoadingArticle.hidden = YES;
    [_activityIndicatorArticle stopAnimating];
}

-(void)setArticle:(NSArray *)listArticle {
    NSInteger i = 0;
    for (Article *article in listArticle) {
        if (i == 0) {
            [_imgOne sd_setImageWithURL:[NSURL URLWithString:[StringUtils replaceSpaceFromUrl:article.coverImage]] placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
            _lblOne.text = article.title;
        } else if (i == 1) {
            [_imgTwo sd_setImageWithURL:[NSURL URLWithString:[StringUtils replaceSpaceFromUrl:article.coverImage]] placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
            _lblTwo.text = article.title;
        } else {
            [_imgThree sd_setImageWithURL:[NSURL URLWithString:[StringUtils replaceSpaceFromUrl:article.coverImage]] placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
            _lblThree.text = article.title;
        }
        i++;
    }
}

-(void)setCity:(NSArray *)listCity{
    
}

//action click
- (IBAction)lblArticleAllClick:(id)sender {
    [self.homePresenter goToArticleView];
}

@end

//
//  TourismAtrcCollectionCell.h
//  Smiling
//
//  Created by Irfan-Ismaya on 12/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TourismAtrcCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *uiViewTourism;
@property (weak, nonatomic) IBOutlet UIImageView *uiImageTourism;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelTourism;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

@end

NS_ASSUME_NONNULL_END

//  EventCollectionCell.h
//  Smiling
//  Created by Irfan-Ismaya on 12/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EventCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *uiViewEvent;
@property (weak, nonatomic) IBOutlet UIImageView *uiImageEvent;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelEvent;
@end

NS_ASSUME_NONNULL_END

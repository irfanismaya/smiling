//  VideoCollectionCell.h
//  Smiling
//  Created by Irfan-Ismaya on 12/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageVideo;
@property (weak, nonatomic) IBOutlet UIView *uiViewContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

@end

NS_ASSUME_NONNULL_END

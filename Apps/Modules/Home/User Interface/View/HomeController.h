//  HomeController.h
//  Smiling
//  Created by Irfan-Ismaya on 04/04/19.
//  Copyright © 2019 Merqurion Solution. All rights reserved.

#import <UIKit/UIKit.h>
#import "KIImagePager.h"
#import "HomeModuleProtocol.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeController : BaseViewController<HomeViewProtocol, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,KIImagePagerDelegate,KIImagePagerDataSource,KIImagePagerDataSource>
@property (weak, nonatomic) IBOutlet UIView *uiViewLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblNameLocation;
@property (weak, nonatomic) IBOutlet UIImageView *imageProfile;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewFirst;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewTwo;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewThree;
@property (weak, nonatomic) IBOutlet UICollectionView *eventCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *amenitiesCollection;
@property (weak, nonatomic) IBOutlet UIScrollView *uiScrollView;
@property (weak, nonatomic) IBOutlet UICollectionView *videoCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *tourismCollection;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (nonatomic, strong) id<HomePresenterProtocol> homePresenter;
@end

NS_ASSUME_NONNULL_END

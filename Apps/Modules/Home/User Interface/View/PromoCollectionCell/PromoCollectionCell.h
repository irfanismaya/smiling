//  PromoCollectionCell.h
//  Smiling
//  Created by Irfan-Ismaya on 18/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.


#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PromoCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imagePromo;
@end

NS_ASSUME_NONNULL_END

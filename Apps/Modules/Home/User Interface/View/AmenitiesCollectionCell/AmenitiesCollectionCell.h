//
//  AmenitiesCollectionCell.h
//  Smiling
//
//  Created by Irfan-Ismaya on 12/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AmenitiesCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *uiViewAmenities;
@property (weak, nonatomic) IBOutlet UIImageView *uiImageAmenities;
@property (weak, nonatomic) IBOutlet UILabel *uiLabelAmenities;
@end

NS_ASSUME_NONNULL_END

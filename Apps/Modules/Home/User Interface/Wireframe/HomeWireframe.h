//
//  HomeWireframe.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "TabBarModuleProtocol.h"
#import "RootWireframe.h"
@class HomePresenter;
@class TourismWireframe;

@interface HomeWireframe : RootWireframe <TabBarViewProtocol>

@property (nonatomic, strong) HomePresenter *homePresenter;
@property (nonatomic, strong) TourismWireframe *tourismWireframe;

- (void)presentHomeInterfaceFromViewController:(UIViewController *)viewController;
- (void)goToArticleView;

@end

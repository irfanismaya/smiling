//
//  HomeWireframe.m
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "HomeWireframe.h"
#import "HomeController.h"
#import "HomePresenter.h"
#import "RootWireframe.h"

#import "HomeManagerAPI.h"
#import "HomeInteractor.h"

#import "TourismWireframe.h"

static NSString *HomeControllerIdentifier = @"HomeController";

@interface HomeWireframe ()

@property (nonatomic, strong) HomeController *homeController;

@end

@implementation HomeWireframe

- (void)presentHomeInterfaceFromViewController:(UIViewController *)viewController
{
    [self initWireframe];
    [viewController presentViewController:self.homeController animated:YES completion:nil];
}

- (UIViewController *)configuredViewController {
    [self initWireframe];
    return self.homeController;
}

- (void) initWireframe {
    self.homeController = [self viewControllerFromStoryboard:HomeControllerIdentifier];
    
    HomeManagerAPI *homeManagerAPI = [[HomeManagerAPI alloc] init];
    HomeInteractor *homeInteractor = [[HomeInteractor alloc] initWithHomeManager:homeManagerAPI];
    
    self.homePresenter = [[HomePresenter alloc] init];
    self.homePresenter.homeWireframe = self;
    self.homePresenter.homeInteractor = homeInteractor;
    self.homePresenter.homeController = self.homeController;
    
    homeInteractor.homePresenter = self.homePresenter;
    self.homeController.homePresenter = self.homePresenter;
    
    self.tourismWireframe = [[TourismWireframe alloc] init];
}

- (UIImage *)tabIcon {
    return [UIImage imageNamed:TAB_HOME_ICON];
}

- (NSString *)tabTitle {
    return TAB_HOME;
}

- (void)goToArticleView{
    [self.tourismWireframe presentTourismInterfaceFromNavController:self.homeController.navigationController];
}

@end

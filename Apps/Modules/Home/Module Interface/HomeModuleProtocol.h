//  HomeModuleProtocol.h
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

//khusus buat presenter
@protocol HomePresenterProtocol <NSObject>
- (void)doGetUserList:(NSInteger)page;
- (void)doGetCityList;
- (void)doGetArticleList:(NSInteger)page;
- (void)goToArticleView;
@end

//khusus buat interactor
@protocol HomeInteractorInput <NSObject>
- (void)getUserList:(NSInteger)page;
- (void)getCityList;
- (void)getArticleList:(NSInteger)page;
@end

@protocol HomeInteractorOutput <NSObject>
- (void)didGetUserList:(NSArray *)listUser error:(NSError *)error;
- (void)didGetCityList:(NSArray *)listCity error:(NSError *)error;
- (void)didGetArticleList:(NSArray *)listArticle error:(NSError *)error;
@end

@protocol HomeManagerAPIInput <NSObject>
- (void)getUserList:(NSInteger)page completion:(void (^)(NSArray *listUser, NSError *error))completion;
- (void)getCityList:(void (^)(NSArray *listCity, NSError *error))completion;
- (void)getArticleList:(NSInteger)page completion:(void (^)(NSArray *listCity, NSError *error))completion;
- (void)getPromotionList:(NSInteger)page completion:(void (^)(NSArray *listPromotion, NSError *error))completion;
@end

//khusus buat view
@protocol HomeViewProtocol <NSObject>
- (void)showLoading;
- (void)hideLoading;

- (void)showLoadingPromo;
- (void)hideLoadingPromo;

- (void)showLoadingArticle;
- (void)hideLoadingArticle;
- (void)setArticle:(NSArray *)listArticle;
- (void)setCity:(NSArray *)setCity;
@end

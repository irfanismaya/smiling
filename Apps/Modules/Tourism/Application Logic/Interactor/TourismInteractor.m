//
//  TourismInteractor.m
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "TourismInteractor.h"
#import "TourismManagerAPI.h"

@interface TourismInteractor ()
@property (nonatomic, strong) TourismManagerAPI *tourismManagerAPI;
@end

@implementation TourismInteractor
- (instancetype)initWithTourismManager:(TourismManagerAPI *)tourismManagerAPI{
    if ((self = [super init]))
    {
        _tourismManagerAPI = tourismManagerAPI;
    }
    return self;
}

#pragma mark - TourismInteractorInput
- (void)getArticleList:(NSInteger)page {
    __weak typeof(self) weakSelf = self;
    [self.tourismManagerAPI getArticleList:page completion:^(NSArray *listArticle, NSError *error) {
        [weakSelf.tourismPresenter didGetArticleList:listArticle error:error];
    }];
}

@end

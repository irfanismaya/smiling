//
//  TourismInteractor.h
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "TourismModuleProtocol.h"
@class TourismManagerAPI;

@interface TourismInteractor : NSObject <TourismInteractorInput>
- (instancetype)initWithTourismManager:(TourismManagerAPI *)tourismManagerAPI;
@property (nonatomic, weak) id <TourismInteractorOutput> tourismPresenter;
@end

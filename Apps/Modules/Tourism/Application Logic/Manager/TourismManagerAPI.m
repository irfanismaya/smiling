//
//  TourismManagerAPI.m
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "TourismManagerAPI.h"
#import "NetworkManager+Article.h"

@interface TourismManagerAPI ()
@end

@implementation TourismManagerAPI
- (void)getArticleList:(NSInteger)page completion:(void (^)(NSArray *, NSError *))completion {
    [NetworkManager getArticleList:page successBlock:^(NSArray *listArticle) {
        completion(listArticle, nil);
    } failureBlock:^(NSError *error) {
        completion(nil, error);
    }];
}
@end

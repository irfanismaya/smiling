//
//  TourismManagerAPI.h
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "TourismModuleProtocol.h"
@interface TourismManagerAPI : NSObject<TourismManagerAPIInput>
@end

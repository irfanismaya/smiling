//
//  TourismModuleProtocol.h
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

//khusus buat presenter
@protocol TourismPresenterProtocol <NSObject>
- (void)doGetArticleList:(NSInteger)page;
- (void)goToArticleDetailView:(NSInteger)articleId;
@end

//khusus buat interactor
@protocol TourismInteractorInput <NSObject>
- (void)getArticleList:(NSInteger)page;
@end

@protocol TourismInteractorOutput <NSObject>
- (void)didGetArticleList:(NSArray *)listArticle error:(NSError *)error;
@end

@protocol TourismManagerAPIInput <NSObject>
- (void)getArticleList:(NSInteger)page completion:(void (^)(NSArray *listArticle, NSError *error))completion;
@end

//khusus buat view
@protocol TourismViewProtocol <NSObject>
- (void)showLoading;
- (void)hideLoading;
- (void)setArticle:(NSArray *)listArticle;
@end

//  TourismHeader.h
//  Smiling
//  Created by Irfan-Ismaya on 19/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
@import GoogleMaps;

NS_ASSUME_NONNULL_BEGIN

@interface TourismHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (weak, nonatomic) IBOutlet GMSMapView *mapLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@end

NS_ASSUME_NONNULL_END

//  TourismController.h
//  Smiling
//  Created by Irfan-Ismaya on 19/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
#import "TourismModuleProtocol.h"
#import "BaseViewProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface TourismController : BaseViewController<BaseViewProtocol, TourismViewProtocol, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionTourism;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, strong) id<TourismPresenterProtocol> tourismPresenter;

@end

NS_ASSUME_NONNULL_END

//  TourismCollectionCell.h
//  Smiling
//  Created by Irfan-Ismaya on 19/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TourismHomeCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UIButton *buttonPrice;
@property (weak, nonatomic) IBOutlet UIView *uiViewTourism;

@end

NS_ASSUME_NONNULL_END

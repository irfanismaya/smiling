//  TourismController.m
//  Smiling
//  Created by Irfan-Ismaya on 19/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "TourismController.h"
#import "Constants.h"
#import "UIColor+HexString.h"
#import "TourismHeader.h"
#import "TourismHomeCollectionCell.h"
#import "DetailTourismController.h"
#import <MaterialComponents/MaterialAppBar.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "Article.h"

@interface TourismController ()

@property(nonatomic, strong) MDCAppBar *appBar;

@property(nonatomic, strong) NSArray *listArticle;

@end

//NSArray *imgGallery;
//NSArray *imgTourismF;
@implementation TourismController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initSearchBar];
    [self initAppBar];
    
//    imgGallery = [[NSArray alloc] initWithObjects:
//                  [UIImage imageNamed:@"bg1@2x.png"],
//                  [UIImage imageNamed:@"bg2@2x.png"],
//                  [UIImage imageNamed:@"bg3@2x.png"],
//                  [UIImage imageNamed:@"bg4@2x.png"],nil];
//
//    imgTourismF = @[@"https://tempatwisataseru.com/wp-content/uploads/2015/10/Curug-Cikaso.jpg", @"https://v-images2.antarafoto.com/festival-seni-budaya-jawa-barat-oyl651-prv.jpg", @"http://alienco.net/wp-content/uploads/2016/01/hiburan-rakyat-jawa-barat-kuda-lumping-640x402.jpg", @"https://media.suara.com/pictures/653x366/2018/12/14/37976-kemenpar.jpg"];
    
    [_tourismPresenter doGetArticleList:1];
}

-(void)initAppBar{
    self.view.tintColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Maribaya";
    _appBar = [[MDCAppBar alloc] init];
    [self addChildViewController:_appBar.headerViewController];
    
    self.appBar.navigationBar.tintColor = UIColor.whiteColor;
    self.appBar.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName : UIColor.whiteColor};
    self.appBar.headerViewController.headerView.backgroundColor = [UIColor colorWithHexString:primary_color_purple];
    self.appBar.headerViewController.headerView.tintColor = UIColor.whiteColor;
    [self.appBar addSubviewsToParent];
    
    UIImage *menuItemImage = [UIImage imageNamed:@"baseline_arrow_back_ios_white_24pt"];
    UIImage *templatedMenuItemImage = [menuItemImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIBarButtonItem *menuItem =
    [[UIBarButtonItem alloc] initWithImage:templatedMenuItemImage
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(actionBack:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

-(void)initSearchBar{
    for (UIView *subview in self.searchBar.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
            break;
        }
        for (UIView *subsubview in subview.subviews) {
            if ([subsubview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [subsubview removeFromSuperview];
                break;
            }
        }
    }
    
    self.searchBar.barTintColor = [UIColor grayColor];
    self.searchBar.layer.borderColor = [UIColor grayColor].CGColor;
    self.searchBar.backgroundColor = [UIColor whiteColor];
    
    UITextField *searchField = [self.searchBar valueForKey:@"searchField"];
    searchField.backgroundColor = [UIColor colorWithHexString:@"#F6F9FF"];
    searchField.textColor = [UIColor grayColor];
    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Some Text"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor grayColor];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _listArticle.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    TourismHeader *header = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        header = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                    withReuseIdentifier:@"TourismHeader"
                                                           forIndexPath:indexPath];
        header.mapLocation.layer.cornerRadius = 12;
        [header.imageHeader sd_setImageWithURL:[NSURL URLWithString:@"https://i2.wp.com/www.urbandung.com/wp-content/uploads/2016/04/rumah-hobbit-bandung.jpg?fit=1500%2C845&ssl=1"]
                          placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
    }
    return header;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    DetailTourismController *detailtourism = [storyboard instantiateViewControllerWithIdentifier:@"DetailTourismController"];
//    [self.navigationController pushViewController:detailtourism animated:YES];
    
    Article *article = _listArticle[indexPath.row];
    [_tourismPresenter goToArticleDetailView:article.id];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Article *article = _listArticle[indexPath.row];
    
    TourismHomeCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TourismHomeCollectionCell" forIndexPath:indexPath];
    //cell.imageContent.image = imgGallery[indexPath.row];
    [cell.imageContent sd_setImageWithURL:[NSURL URLWithString:[StringUtils replaceSpaceFromUrl:article.coverImage]]
                         placeholderImage:[UIImage imageNamed:@"bg1@2x.png"]];
    
    cell.buttonPrice.layer.cornerRadius = 15;
    cell.uiViewTourism.layer.cornerRadius = 8;
    cell.imageContent.layer.cornerRadius = 8;
    
    cell.lblTitle.text = article.title;
    
    if(![article.desc isEqualToString:@""]) {
        NSMutableAttributedString *attributedString =
        [[NSMutableAttributedString alloc]initWithData: [article.desc dataUsingEncoding:NSUnicodeStringEncoding]
                                               options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                    documentAttributes: nil
                                                 error: nil];
        
        [cell.lblDesc setAttributedText:attributedString];
    } else {
        cell.lblDesc.text = @"";
    }
    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

#pragma mark - HomeViewProtocol
- (void)showLoading {
    _searchBar.hidden = YES;
    _collectionTourism.hidden = YES;
    [super showLoading];
}

- (void)hideLoading {
    _searchBar.hidden = NO;
    _collectionTourism.hidden = NO;
    [super hideLoading];
}

-(void)setArticle:(NSArray *)listArticle {
    _listArticle = listArticle;
    [_collectionTourism reloadData];
}

@end

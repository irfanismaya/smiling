//
//  TourismPresenter.m
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "TourismPresenter.h"
#import "TourismWireframe.h"

@interface TourismPresenter ()
@end

@implementation TourismPresenter

#pragma mark - TourismPresenterProtocol
- (void)doGetArticleList:(NSInteger)page {
    [self.tourismController showLoading];
    [self.tourismInteractor getArticleList:page];
}

- (void)goToArticleDetailView:(NSInteger)articleId {
    [self.tourismWireframe goToArticleDetailView:articleId];
}


#pragma mark - TourismInteractorOutput
- (void)didGetArticleList:(NSArray *)listArticle error:(NSError *)error {
    [self.tourismController setArticle:listArticle];
    [self.tourismController hideLoading];
}

@end

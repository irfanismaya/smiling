//
//  TourismPresenter.h
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "TourismModuleProtocol.h"
#import "TourismWireframe.h"

@interface TourismPresenter : NSObject <TourismPresenterProtocol, TourismInteractorOutput>
@property (nonatomic, strong) TourismWireframe* tourismWireframe;
@property (nonatomic, strong) UIViewController<TourismViewProtocol> *tourismController;
@property (nonatomic, strong) id <TourismInteractorInput> tourismInteractor;
@end

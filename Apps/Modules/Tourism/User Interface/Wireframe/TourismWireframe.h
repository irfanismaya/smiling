//
//  TourismWireframe.h
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "RootWireframe.h"
@class TourismPresenter;
@class DetailTourismWireframe;

@interface TourismWireframe : RootWireframe

@property (nonatomic, strong) TourismPresenter *tourismPresenter;
@property (nonatomic, strong) DetailTourismWireframe *detailTourismWireframe;

- (void)presentTourismInterfaceFromNavController:(UINavigationController *)navController;
- (void)goToArticleDetailView:(NSInteger)articleId;

@end

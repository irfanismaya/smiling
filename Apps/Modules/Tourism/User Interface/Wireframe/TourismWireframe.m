//
//  TourismWireframe.m
//  Smiling
//
//  Created by Rully Winata on 05/05/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "TourismWireframe.h"
#import "TourismController.h"
#import "TourismPresenter.h"
#import "RootWireframe.h"
#import "TourismManagerAPI.h"
#import "TourismInteractor.h"
#import "DetailTourismWireframe.h"

static NSString *TourismControllerIdentifier = @"TourismController";
@interface TourismWireframe ()
@property (nonatomic, strong) TourismController *tourismController;
@end

@implementation TourismWireframe
- (void)presentTourismInterfaceFromNavController:(UINavigationController *)navController {
    self.tourismController = [self viewControllerFromStoryboard:TourismControllerIdentifier];
    
    TourismManagerAPI *tourismManagerAPI = [[TourismManagerAPI alloc] init];
    TourismInteractor *tourismInteractor = [[TourismInteractor alloc] initWithTourismManager:tourismManagerAPI];
    
    self.tourismPresenter = [[TourismPresenter alloc] init];
    self.tourismPresenter.tourismWireframe = self;
    self.tourismPresenter.tourismInteractor = tourismInteractor;
    self.tourismPresenter.tourismController = self.tourismController;
    
    tourismInteractor.tourismPresenter = self.tourismPresenter;
    self.tourismController.tourismPresenter = self.tourismPresenter;

    self.detailTourismWireframe = [[DetailTourismWireframe alloc] init];
    
    //menampilkan halaman tourism
    [navController pushViewController:self.tourismController animated:YES];
}

- (void)goToArticleDetailView:(NSInteger)articleId {
    [self.detailTourismWireframe presentDetailTourismInterfaceFromNavController:self.tourismController.navigationController articleId:articleId];
}
@end

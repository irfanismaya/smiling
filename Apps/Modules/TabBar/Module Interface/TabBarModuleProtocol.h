//
//  TabBarModuleProtocol.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

@protocol TabBarPresenterProtocol <NSObject>
- (void)test;
@end

@protocol TabBarViewProtocol <NSObject>
- (NSString *)tabTitle;
- (UIImage *)tabIcon;
- (UIViewController *)configuredViewController;
@end

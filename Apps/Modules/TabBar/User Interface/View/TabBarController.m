//  TabBarController.m
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "TabBarController.h"
#import "KBTabbar.h"

@interface TabBarController ()

@end

@implementation TabBarController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCustomtabbar];
}

- (void)setCustomtabbar{
    KBTabbar *tabbar = [[KBTabbar alloc]init];
    [self setValue:tabbar forKeyPath:@"tabBar"];
    [tabbar.centerBtn addTarget:self action:@selector(centerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)centerBtnClick:(UIButton *)btn{
    NSLog(@"点击了中间");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"点击了中间按钮" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}
@end

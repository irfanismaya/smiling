//  TabBarController.h
//  Smiling
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "TabBarModuleProtocol.h"

@interface TabBarController : UITabBarController

@property (nonatomic, strong) id<TabBarPresenterProtocol> tabBarPresenter;

@end

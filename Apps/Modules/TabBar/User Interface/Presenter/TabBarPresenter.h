//
//  TabBarPresenter.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "TabBarModuleProtocol.h"
#import "TabBarWireframe.h"

@interface TabBarPresenter : NSObject <TabBarPresenterProtocol>

@property (nonatomic, strong) TabBarWireframe* tabBarWireframe;
@property (nonatomic, strong) UITabBarController *tabBarController;

//@property (nonatomic, strong) id <LoginInteractorInput> loginInteractor;

@end

//
//  TabBarWireframe.m
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "TabBarWireframe.h"
#import "TabBarController.h"
#import "TabBarPresenter.h"
#import "RootWireframe.h"

#import "HomeWireframe.h"
#import "NearbyWireframe.h"
#import "ReferralWireframe.h"
#import "AccountWireframe.h"

#import "TabBarModuleProtocol.h"

static NSString *TabBarControllerIdentifier = @"smilingTabVC";

@interface TabBarWireframe ()

@property (nonatomic, strong) TabBarController *tabBarController;

@property (nonatomic, strong) id <TabBarViewProtocol> homeWireframe;
@property (nonatomic, strong) id <TabBarViewProtocol> nearbyWireframe;
@property (nonatomic, strong) id <TabBarViewProtocol> referralWireframe;
@property (nonatomic, strong) id <TabBarViewProtocol> accountWireframe;

@end

@implementation TabBarWireframe

- (void)presentTabBarInterfaceFromNavController:(UINavigationController *)navController
{
    self.tabBarController = [self tabBarControllerFromStoryboard:TabBarControllerIdentifier];
    
    self.tabBarPresenter = [[TabBarPresenter alloc] init];
    self.tabBarPresenter.tabBarWireframe = self;
    self.tabBarPresenter.tabBarController = self.tabBarController;
    
    self.tabBarController.tabBarPresenter = self.tabBarPresenter;
    
    //init tab bar item
    self.homeWireframe = [[HomeWireframe alloc] init];
    self.nearbyWireframe = [[NearbyWireframe alloc] init];
    self.referralWireframe = [[ReferralWireframe alloc] init];
    self.accountWireframe = [[AccountWireframe alloc] init];
    
    NSMutableArray *listViewController = [[NSMutableArray alloc] init];
    [listViewController addObject:_homeWireframe];
    [listViewController addObject:_nearbyWireframe];
    [listViewController addObject:_referralWireframe];
    [listViewController addObject:_accountWireframe];
    
    NSMutableArray *listTabBar = [[NSMutableArray alloc] init];
    for(UIViewController<TabBarViewProtocol> *viewController in listViewController) {
        UITabBarItem *tabBarItem = [[UITabBarItem alloc] init];
        tabBarItem.title = viewController.tabTitle;
        tabBarItem.image = viewController.tabIcon;
        UIViewController *confViewController = viewController.configuredViewController;
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:confViewController];
        navController.tabBarItem = tabBarItem;
        navController.navigationBarHidden = true;
        
        [listTabBar addObject:navController];
    }
    
    self.tabBarController.viewControllers = listTabBar;
    [navController pushViewController:self.tabBarController animated:YES];
}

- (void)goToHomeView {
    //    [self.homeWireframe presentHomeInterfaceFromViewController:self.loginViewController];
}

@end

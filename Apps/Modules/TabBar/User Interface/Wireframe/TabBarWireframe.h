//
//  TabBarWireframe.h
//  Smiling
//
//  Created by Rully Winata on 20/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.
//

#import "RootWireframe.h"
@class TabBarPresenter;

@interface TabBarWireframe : RootWireframe

@property (nonatomic, strong) TabBarPresenter *tabBarPresenter;

- (void)presentTabBarInterfaceFromNavController:(UINavigationController *)navController;
//- (void)goToHomeView;

@end

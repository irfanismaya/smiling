//  HeaderController.m
//  Smiling
//  Created by Irfan-Ismaya on 11/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "HeaderController.h"
#import "SearchLocationController.h"

@interface HeaderController ()
@property (weak, nonatomic) IBOutlet UIView *uiViewSearch;
@end

@implementation HeaderController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.uiViewSearch.layer.borderColor = [UIColor grayColor].CGColor;
    self.uiViewSearch.layer.borderWidth = 1.0f;
    self.uiViewSearch.layer.masksToBounds = TRUE;
    self.uiViewSearch.layer.cornerRadius = 4.0f;
    
    UITapGestureRecognizer *goSearch =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(goSearch:)];
    
    [self.uiViewSearch setUserInteractionEnabled:YES];
    [self.uiViewSearch addGestureRecognizer:goSearch];
}

- (void)goSearch:(UITapGestureRecognizer *)recognizer {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchLocationController *searchlocation = [storyboard instantiateViewControllerWithIdentifier:@"SearchLocation"];
    [self.navigationController pushViewController:searchlocation animated:YES];
}

@end

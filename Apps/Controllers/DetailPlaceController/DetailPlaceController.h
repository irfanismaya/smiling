//  DetailPlaceController.h
//  Smiling
//  Created by Irfan-Ismaya on 11/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import <UIKit/UIKit.h>
#import <MaterialComponents/MaterialButtons.h>
#import <MaterialComponents/MaterialTextFields.h>
#import "MaterialButtons.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailPlaceController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *uiViewDate;
@property (weak, nonatomic) IBOutlet MDCButton *buttonNext;

@property (weak, nonatomic) IBOutlet UIButton *buttonPlus;
@property (weak, nonatomic) IBOutlet UIButton *buttonMinus;
@property (weak, nonatomic) IBOutlet UIView *uiViewContentVal;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;


@property (weak, nonatomic) IBOutlet UIButton *buttonPlusChild;
@property (weak, nonatomic) IBOutlet UIButton *buttonMinusChild;
@property (weak, nonatomic) IBOutlet UIView *uiViewContentValChild;
@property (weak, nonatomic) IBOutlet UILabel *lblValueChild;

@end

NS_ASSUME_NONNULL_END

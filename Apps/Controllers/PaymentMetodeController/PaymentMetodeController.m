//  PaymentMetodeController.m
//  Smiling
//  Created by Irfan-Ismaya on 13/04/19.
//  Copyright © 2019 Digi Asia. All rights reserved.

#import "PaymentMetodeController.h"
#import "Constants.h"
#import "UIColor+HexString.h"
#import <MaterialComponents/MaterialAppBar.h>

@interface PaymentMetodeController ()
@property(nonatomic, strong) MDCAppBar *appBar;
@end

@implementation PaymentMetodeController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initAppBar];
    
    self.tfIdTextFieldController =
    [[MDCTextInputControllerOutlined alloc] initWithTextInput:self.tfIdCashPro];
    self.tfIdTextFieldController.inlinePlaceholderColor = [UIColor colorWithHexString:primary_color_purple];
    self.tfIdTextFieldController.normalColor = [UIColor colorWithHexString:primary_color_purple];
    self.tfIdTextFieldController.activeColor = [UIColor colorWithHexString:primary_color_purple];
    self.tfIdTextFieldController.floatingPlaceholderActiveColor = [UIColor colorWithHexString:primary_color_purple];
    self.tfIdTextFieldController.placeholderText = @"3212 2144 9733 9263";
    
    self.buttonNext.layer.cornerRadius = 24;
    self.buttonNext.layer.masksToBounds = true;
    
    self.buttonMinus.clipsToBounds = true;
    self.buttonMinus.layer.cornerRadius = 12.5;
    self.buttonMinus.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMinXMinYCorner;
    self.buttonMinus.layer.borderWidth = 1;
    self.buttonMinus.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.buttonPlus.clipsToBounds = true;
    self.buttonPlus.layer.cornerRadius = 12.5;
    self.buttonPlus.layer.maskedCorners = kCALayerMaxXMinYCorner | kCALayerMaxXMaxYCorner;
    self.buttonPlus.layer.borderWidth = 1;
    self.buttonPlus.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.uiViewContentVal.layer.borderWidth = 1;
    self.uiViewContentVal.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.buttonMinusChild.clipsToBounds = true;
    self.buttonMinusChild.layer.cornerRadius = 12.5;
    self.buttonMinusChild.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMinXMinYCorner;
    self.buttonMinusChild.layer.borderWidth = 1;
    self.buttonMinusChild.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.buttonPlusChild.clipsToBounds = true;
    self.buttonPlusChild.layer.cornerRadius = 12.5;
    self.buttonPlusChild.layer.maskedCorners = kCALayerMaxXMinYCorner | kCALayerMaxXMaxYCorner;
    self.buttonPlusChild.layer.borderWidth = 1;
    self.buttonPlusChild.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.uiViewContentValChild.layer.borderWidth = 1;
    self.uiViewContentValChild.layer.borderColor = [UIColor grayColor].CGColor;
}

-(void)initAppBar{
    self.view.tintColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Maribaya";
    _appBar = [[MDCAppBar alloc] init];
    [self addChildViewController:_appBar.headerViewController];
    
    self.appBar.navigationBar.tintColor = UIColor.whiteColor;
    self.appBar.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName : UIColor.whiteColor};
    self.appBar.headerViewController.headerView.backgroundColor = [UIColor colorWithHexString:primary_color_purple];
    self.appBar.headerViewController.headerView.tintColor = UIColor.whiteColor;
    [self.appBar addSubviewsToParent];
    
    UIImage *menuItemImage = [UIImage imageNamed:@"baseline_arrow_back_ios_white_24pt"];
    UIImage *templatedMenuItemImage = [menuItemImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIBarButtonItem *menuItem =
    [[UIBarButtonItem alloc] initWithImage:templatedMenuItemImage
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(actionBack:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)actionBack:(id)selector{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
